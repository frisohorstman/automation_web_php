/* global settings */

$(function () {
    msg.init();
})

const msg = {
    init: function () {
        // create container
        const html = '<div class="alert-container"></div>';
        $('body').append(html);

        // check if there are already messages in js global settings var
        if (typeof settings.messages != 'undefined') {
            settings.messages.forEach(function (message) {
                msg.show(message.msg, message.type)
            });
        }

        // onscroll change top of the container
        msg.setContainerTop();
        $(document).on('scroll', function () {
            msg.setContainerTop()
        });
    },

    elementId: 0,
    countId: function () {
        return msg.elementId++;
    },

    slideUp: function (elm_id) {
        document[elm_id] = setTimeout(function (elm_id) {
            $('#' + elm_id).slideUp();
        }, 3000, elm_id);
    },

    create: function (message, type) {
        const countId = msg.countId();
        const elementId = 'msg-' + countId;

        const html = '<div class="alert alert-' + type + '" id="' + elementId + '" role="alert">' + message + '</div>';
        $('.alert-container').append(html);
        $('#' + elementId).hide();

        return countId;
    },

    setContainerTop: function () {
        const scrollTop = $(window).scrollTop();
        const height = $('.navbar').outerHeight(true);

        let top = 15 + (height - scrollTop);
        if (top <= 15) {
            top = 15;
        }

        $('.alert-container').css('top', top + 'px');
    },

    show: function (message = false, type = 'success') {
        // create an alert
        const countId = msg.create(message, type);
        const elementId = 'msg-' + countId;

        $('#' + elementId).slideDown(100);
        msg.slideUp(elementId);
    }
}