/* global settings */
/* global media */
/* global msg */

$(document).ready(function () {
    net.redirectToOtherWebserver();
    net.bindClicks();

    if ($('#network-devices').length > 0) {
        net.getNetworkDevices();
    }
});

var net = {
    devices: [],

    bindClicks: function () {
        $('#reboot-voice-pi').click(function () {
            net.rebootDevice({ reboot_device: 'voice_pi' })
        });

        $('#reboot-zwave-pi').click(function () {
            net.rebootDevice({ reboot_device: 'zwave_pi' })
        });

        $('#reboot-server').click(function () {
            net.rebootDevice({ reboot_device: 'server' })
        });

        $('#reboot-laptop').click(function () {
            net.rebootDevice({ reboot_device: 'laptop' })
        });

        $('#shutdown-server').click(function () {
            net.shutdownDevice({ shutdown_device: 'server' })
        });

        $('#shutdown-laptop').click(function () {
            net.shutdownDevice({ shutdown_device: 'laptop' })
        });

        $('#wake-server').click(function () {
            net.wakeDevice({ wake_device: 'server' })
        });
    },

    /**
     * we prefer to be on the server instead of the raspberry pi
     * check if the server is online...
     */
    redirectToOtherWebserver: function() {
        if (settings.WEBSERVER_ON_VOICE == false) {
            // were on server so no need to go anywhere
            return;
        }

        Promise.all([
            $.getJSON(settings.OTHER_WEBSERVER + '/net/json', { is_awake: 'get' }),
        ]).then(() => {
            // were on voice pi, but server is available so redirect to server unless no_redirect sits in URL.
            const url = new URL(window.location.href);

            if (url.searchParams.get('no_redirect') == null) {
                document.location = settings.OTHER_WEBSERVER + window.location.pathname;
            }
        });
    },

    rebootDevice: function (query) {
        $.getJSON('/net/json', query, function (json) {
            if (json.success == true) {
                msg.show('Het apparaat is aan het rebooten');
                net.waitReload();
            } else {
                msg.show('Reboot lukt niet', 'danger');
            }
        });
    },

    shutdownDevice: function (query) {
        $.getJSON('/net/json', query, function (json) {
            if (json.success == true) {
                msg.show('Het apparaat is aan het afsluiten');
                net.waitReload();
            } else {
                msg.show('Shutdown lukt niet', 'danger');
            }
        });
    },

    wakeDevice: function (query) {
        $.getJSON('/net/json', query, function (json) {
            if (json.success == true) {
                msg.show('Het apparaat is aan het opstarten');
                net.waitReload();
            } else {
                msg.show('Magic packet verzenden is niet gelukt', 'danger');
            }
        });
    },

    delayedReload: async function (milisec = 1000) {
        await media.sleep(milisec);
        location.reload();
    },

    findDevice: function (search_key = 'mac', search_value = false) {
        let foundIndex = false;

        for (const index in net.devices) {
            var device = net.devices[index];

            if (device[search_key] == search_value) {
                foundIndex = index;
            }
        }

        return foundIndex;
    },

    mergeDevices: function (new_devices) {
        const devices = net.devices;

        // loop trough new data and find matching macs
        for (const index in new_devices) {
            var new_device = new_devices[index];
            var localIndex = net.findDevice('mac', new_device['mac']);

            if (localIndex == false) {
                // the mac didnt exist among our devices so we can simply add the new device
                devices.push(new_device);
            } else {
                // we must transfer the new_device data to this device on this.device[localIndex]
                for (const new_key in new_device) {
                    var new_value = new_device[new_key];
                    devices[localIndex][new_key] = new_value;
                }
            }
        }

        return devices;
    },

    getNetworkDevices: async function () {
        Promise.all([
            net.getArp(),
            net.getDhcpLeases(),
            net.getWirelessClients(),
        ]).then(responses => {
            net.devices = responses[0];
            net.devices = net.mergeDevices(responses[1]);
            net.devices = net.parseWirelessClients(responses[2]);

            net.createHtml();

            // now get slow nmap where we get a little description for our TV, since it carries no host its nicer that way...
            net.getNmap();
        }).catch(errors => {
            console.error('Kon de netwerk devices niet ophalen', errors);
        });
    },

    /**
     * return promise
     * 
     * get arp
     */
    getArp: async function () {
        var url = '/net/json?router=getArp';
        return await $.getJSON(url);
    },

    /**
     * return promise
     * 
     * get dhcp leases
     */
    getDhcpLeases: async function () {
        var url = '/net/json?router=getDhcpLeases';
        return await $.getJSON(url);
    },

    /**
     * return promise
     * 
     * get wireless clients mac adresses
     */
    getWirelessClients: async function () {
        var url = '/net/json?router=getWirelessClients';
        return await $.getJSON(url);
    },


    // this one is slow, so lets place the info after other info is already shown
    getNmap: function () {
        var url = '/net/json?router=getNmap';

        Promise.all([
            $.getJSON(url),
        ]).then(responses => {
            net.parseNmap(responses[0]);
        });
    },

    // find html element and update html
    parseNmap: function (devices) {
        for (const index in devices) {
            var device = devices[index];
            var html_elm = $('[data-mac="' + device['mac'] + '"]');

            if (html_elm.length > 0) {
                if (html_elm.find('h3').text() == '*') {
                    html_elm.find('h3').text(device['description']);
                }
            }
        }
    },

    parseWirelessClients: function (macs) {
        const devices = net.devices;

        for (const index in macs) {
            var mac = macs[index];
            var localIndex = net.findDevice('mac', mac);

            if (localIndex == false) {
                const mac_device = {
                    mac: mac,
                    wireless: true,
                }

                devices.push(mac_device);
            } else {
                devices[localIndex]['wireless'] = true;
            }
        }

        return devices;
    },

    ///////
    // loop trough all devices in array and create html network devices (icons, title, etc..)
    createHtml: function () {
        for (const index in net.devices) {
            const device = net.devices[index];

            if (device['device'] == 'vlan2') {
                net.addHtmlDevice(device, '#internet_iface');
                net.addHtmlDevice({
                    host: 'router.home',
                    mac: '38:2C:4A:65:23:30',
                    ip: '10.0.0.1',
                }, '#internet_iface');
            } else if (device['wireless']) {
                net.addHtmlDevice(device, '#wireless');
            } else {
                net.addHtmlDevice(device, '#ethernet');
            }
        }
    },


    addHtmlDevice(dev, append_container = '#ethernet') {
        $('.device-container .loading').hide();

        // element inladen vanuit de webpage en vullen met device info
        var block = $($('.device-container .block')[0].outerHTML);
        $('.device-container ' + append_container).append(block);

        var url = 'javascript://';
        if (dev.host == 'zwavepi') {
            url = 'http://zwavepi.home:8083';
        }
        if (dev.host == 'automation') {
            url = 'http://automation.home';
        }
        if (dev.host == 'AppleTV') {
            url = 'http://appletv.home';
        }
        if (dev.ip == '10.0.0.1') {
            url = 'http://router.home';
        }

        var host = dev.host;
        if (!host) {
            host = dev.ip;
        }

        var device_name = host;
        if (dev.device == 'vlan2') {
            device_name = 'Internet';
            url = 'http://' + dev.ip;
            $(block).find('svg').html('<use xlink:href="#globe"/>');
        } else if (dev.host == 'router.home') {
            device_name = 'Router';
            $(block).find('svg').html('<use xlink:href="#router"/>');
            $(block).addClass('router');
        } else if (dev.wireless) {
            $(block).find('svg').html('<use xlink:href="#wifi"/>');
            $(block).addClass('medium');
        } else {
            $(block).find('svg').html('<use xlink:href="#hdd-network"/>');

            if (append_container == '#ethernet_small') {
                $(block).addClass('small');
            } else {
                $(block).addClass('medium');
            }
        }

        $(block).removeClass('hidden');
        $(block).attr('data-mac', dev.mac);

        $(block).find('a').attr('href', url);
        $(block).find('h3').text(device_name);
        $(block).find('p').text(dev.mac);
        $(block).find('div').text(dev.ip);
    },
}
