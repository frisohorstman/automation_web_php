/* global settings */
/* global websocket */
/* global msg */

$(function () {
    // remote click bind, for table tr and buttons
    $("[data-remote]").on("click", function () {
        const remote = $(this).data('remote');
        const key = $(this).data('key');

        infrared.send(remote, key);
    });
});


const infrared = {
    send: function (remote, key) {
        if (websocket.connected == false) {
            infrared.sendJSON(remote, key);
        } else {
            infrared.sendWebsocket(remote, key);
        }
    },

    /**
     * transmit infrared signal piped trough websocket
     */
    sendWebsocket: function (remote, key) {
        const obj = {
            'infrared': {
                'function': 'send',
                'args': [remote, key]
            }
        }

        console.log('send ir', obj);

        const json = JSON.stringify(obj);
        websocket.send(json);
    },

    /**
     * old method and unused method, preferrable use the websocket infrared.send() instead
     * since that wat the led also blinks when sending the ir.
     * 
     * leaving this option inhere since it still works and may come in handy some other thime
     */
    sendJSON: function (remote, key) {
        const url = settings.VOICE_HTTP + '/infrared/json/send';
        $.getJSON(url, { remote: remote, key: key }, function (data) {
            if (typeof data.send_remote != 'undefined' && data.send_remote == true) {
                msg.show('Infrarood ' + key + ' signaal verzonden.');
            } else {
                msg.show('Kan infrarood signaal niet verzenden.', 'danger');
            }
        });
    }
}