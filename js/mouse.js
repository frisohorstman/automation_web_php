/* global websocket */

// user-scalable=no should be added to html for phone double taps to work
// user-scalable=no is now added to all pages.. @todo, only add mouse_pad page(s)
//
// !alert, although my laptop has a touchpad the events are interpeted as mousemove NOT touchmove so touch only represents touch screens.

$(function () {
    if($('.remote-mouse').length > 0) {
        mouse.init();
    }
})

const mouse = {
    remote_screen_resolution: null,
    start_x: false,
    start_y: false,

    touch_start: false,
    touch_fingers: 1,

    init: function() {
        mouse.bindEvents();
        mouse.fullScreenMousePad();
    },

    bindEvents: function() {
        // mouse; send absolute mouse positions over websocket
        $('#mouse_pad').on('mousemove', function (e) {
            if(mouse.touch_start !== false) {
                // mousemove also gets triggered on touch events, ignore it.
                return;
            }

            if(websocket.connected == false || mouse.remote_screen_resolution == null) {
                $('#mouse_feedback').html("ERROR: Websocket is down.");
                return;
            }

            const pos = mouse.getAbsolutePosition(e);
            mouse.showPosition(pos);
            mouse.sendMouseMovement(pos, true);
        });

        // touch; save start position so we can calculate the movement
        $('#mouse_pad').on('touchstart', function(e) {
            mouse.touch_start = Date.now();
            mouse.touch_fingers = e.originalEvent.touches.length;
            
            const touch = e.originalEvent.touches[0];
            const pageX = touch.pageX;
            const pageY = touch.pageY;
            
            // calc absolute pos on mouse pad
            const offset = $('#mouse_pad').offset(); 
            mouse.start_x = pageX - offset.left;
            mouse.start_y = pageY - offset.top;
        });
        
        // touch; send relative mouse positions over websocket
        $('#mouse_pad').on('touchmove', function (e) {
            if(websocket.connected == false || mouse.remote_screen_resolution == null) {
                $('#mouse_feedback').html("ERROR: Websocket is down.");
                return;
            }

            const pos = mouse.getRelativePosition(e);
            mouse.showPosition(pos);

            if(mouse.touch_fingers == 1) {
                mouse.sendMouseMovement(pos, false);
            }

            if(mouse.touch_fingers == 2) {
                mouse.sendMouseScroll(pos);
            }
        });

        // touch;
        $('#mouse_pad').on('touchend', function() {
            if(mouse.touch_fingers == 2) {
                // short touch only. long touch is cursor movement handled in touchmove
                if((Date.now() - mouse.touch_start) < 500) {
                    mouse.sendClick('right', 'mousedownup');
                }
            }

            mouse.touch_fingers = 1;
        });

        // mouse; scroll wheel
        // !note, DOMMouseScroll advises wheel event https://developer.mozilla.org/en-US/docs/Web/API/Element/wheel_event
        $('#mouse_pad').bind('DOMMouseScroll mousewheel', function(e) {
            if (e.originalEvent.wheelDelta > 0 || e.originalEvent.detail < 0) {
                // up
                mouse.sendMouseScroll({ y: 1 });
            } else {
                mouse.sendMouseScroll({ y: -1 });
            }
        });

        // mouse; dont open menu on right click in browser
        $('#mouse_pad').contextmenu(function() { return false; });

        // mouse; what click has been done? left, middle or right click?
        $('#mouse_pad').mousedown(function(e) {
            if(e.which == 1) { mouse.sendClick('left', 'mousedown'); }
            if(e.which == 2) { mouse.sendClick('middle', 'mousedown'); }
            if(e.which == 3) { mouse.sendClick('right', 'mousedown'); }
        });
        $('#mouse_pad').mouseup(function(e) {
            if(e.which == 1) { mouse.sendClick('left', 'mouseup'); }
            if(e.which == 2) { mouse.sendClick('middle', 'mouseup'); }
            if(e.which == 3) { mouse.sendClick('right', 'mouseup'); }
        });

        // mouse/touch; on mobile when topmenu icon is pressed, hide/show the mouse pad
        $('.navbar-collapse').on('show.bs.collapse', function () {
            $('#mouse_pad').fadeOut();
        });
        $('.navbar-collapse').on('hide.bs.collapse', function () {
            $('#mouse_pad').fadeIn();
        });
    },

    // @todo, cant this be moved to stylesheet instead?
    fullScreenMousePad: function() {
        const pad = $('#mouse_pad');
        const nav = $('.navbar-dark');

        pad.css("position", "fixed");
        pad.css("left", "0px");
        pad.css("top", nav.outerHeight() + "px");
        pad.css("width", "100%");
        const height = "calc(100% - " + nav.outerHeight() + "px)";
        pad.css("height", height);

        // disable scrolling (meant for phone)
        $('html, body').css({
            overflow: 'hidden',
            height: '100%'
        });
    },

    /**
     * every touch click resets to zero.
     * meaning, touchClick = 0, touchMove ++, touchRelease = 0
     * this way you'll get a x/y position minus last stored x/y position
     * which only gives an increment or decrement of mouse or touch position.
     * 
     * so something like x: 1, y: 2
     */
    getRelativePosition: function(e) {
        const touch = e.originalEvent.touches[0];
        const pageX = touch.pageX;
        const pageY = touch.pageY;

        // calc absolute pos on mouse pad
        const offset = $('#mouse_pad').offset(); 
        let x = pageX - offset.left;
        let y = pageY - offset.top;

        // subtract last pos with current pos
        const sendX = x - mouse.start_x;
        const sendY = y - mouse.start_y;

        const factorX = mouse.remote_screen_resolution.width / $('#mouse_pad').width();
        const factorY = mouse.remote_screen_resolution.height / $('#mouse_pad').height();

        // save positions for next getPosition calc
        mouse.start_x = x;
        mouse.start_y = y;

        return {
            x: Math.round(sendX * factorX),
            y: Math.round(sendY * factorY),
        }
    },

    /**
     * get aboslute position on mouse pad. meaning x/y on #mouse_pad will 
     * directly translate to x/y mouse movements on the remote screen.
     * 
     * so something like 1121, 500.
     * 
     * !note, python must also accept these absolute positions
     */
    getAbsolutePosition: function(e) {
        let pageX = e.pageX;
        let pageY = e.pageY;

        // calc pos on mouse pad
        const offset = $('#mouse_pad').offset(); 
        let x = pageX - offset.left;
        let y = pageY - offset.top;

        const factorX = mouse.remote_screen_resolution.width / $('#mouse_pad').width();
        const factorY = mouse.remote_screen_resolution.height / $('#mouse_pad').height();
        
        return {
            x: Math.round(x * factorX),
            y: Math.round(y * factorY),
        }
    },

    showPosition: function(pos) {
        let txt = 'x: ' + pos.x + ', y: ' + pos.y;
        $('#mouse_feedback').html(txt);
    },

    sendMouseMovement: function(pos, absolute = true) {
        // send to websocket
        const obj = {
            'mouse': {
                'absolute': absolute,
                'x': pos.x,
                'y': pos.y,
            }
        }

        const json = JSON.stringify(obj);
        websocket.send(json);
    },

    sendMouseScroll: function(pos) {
        if(pos.y == 0) {
            return;
        }

        // pos.y has big bumps/values
        let scroll_y = 1;
        if(pos.y < 0) {
            scroll_y = -1
        } 

        const obj = {
            'mouse': {
                'scroll': scroll_y,
            }
        }

        const json = JSON.stringify(obj);
        websocket.send(json);
    },

    sendClick: function(btn_name, action) {
        const obj = {
            'mouse': {
                'click': btn_name,
                'action': action,
            }
        }

        const json = JSON.stringify(obj);
        websocket.send(json);
    }
}