$(function () {
    voice_command.init()
})

const voice_command = {
    init: function () {
        // tag input enter key didnt work.. so fixing it
        $('.bootstrap-tagsinput input').on("keydown", function (event) {
            if (event.key != "Enter") {
                return;
            }

            // enter was pressed, turn into tag
            const keyword = $(this).val();
            $('#modal_voice_commands').find('#keywords').tagsinput('add', keyword);
            $(this).val('');
        });

        $('.voice_command_table_click').click(function () {
            voice_command.table_item_click($(this));
        });

        $('#voice_command_create').click(function () {
            voice_command.create_item();
        });
    },

    // load data from table into modal form
    table_item_click: function (elm) {
        $('#modal_voice_commands .btn-delete').show();

        const function_id = elm.find('.function').data('id');
        const comment = elm.find('.function').data('comment');
        const mod = elm.find('.function').data('module');
        const klass = elm.find('.function').data('class');
        const func = elm.find('.function').data('function');
        const arg1 = elm.find('.function').data('arg1');
        const arg2 = elm.find('.function').data('arg2');
        const keywords = elm.find('.keywords').data('keywords');

        $('#modal_voice_commands').find('#function_id').val(function_id);
        $('#modal_voice_commands').find('#comment').val(comment);
        $('#modal_voice_commands').find('#module').val(mod);
        $('#modal_voice_commands').find('#class').val(klass);
        $('#modal_voice_commands').find('#function').val(func);
        $('#modal_voice_commands').find('#arg1').val(arg1);
        $('#modal_voice_commands').find('#arg2').val(arg2);

        $('#modal_voice_commands').find('#keywords').tagsinput('removeAll');

        const keyword_arr = keywords.split(',');
        keyword_arr.forEach(function (keyword) {
            $('#modal_voice_commands').find('#keywords').tagsinput('add', keyword);
        })

        $('#modal_voice_commands').modal('show');
    },

    // create button is hit, prepare modal for create action
    create_item: function () {
        $('#modal_voice_commands .btn-delete').hide();
        $('#modal_voice_commands').find('#function_id').val('create');

        $('#modal_voice_commands').find('#comment').val('');
        $('#modal_voice_commands').find('#module').val('');
        $('#modal_voice_commands').find('#class').val('');
        $('#modal_voice_commands').find('#function').val('');
        $('#modal_voice_commands').find('#arg1').val('');
        $('#modal_voice_commands').find('#arg2').val('');

        $('#modal_voice_commands').find('#keywords').tagsinput('removeAll');

        $('#modal_voice_commands').modal('show');
    }
}