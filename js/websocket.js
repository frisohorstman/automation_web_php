/* global settings */
/* global mouse */
/* global voice */
/* global msg */

$(document).ready(function () {
    websocket.init();
});

const websocket = {
    socket: false,
    connected: false,
    keep_connected: true,

    init: function () {
        websocket.connect();

        $(window).bind('beforeunload', function () {
            websocket.keep_connected = false;

            if (websocket.socket != false) {
                websocket.socket.close();
            }
        });
    },

    connect: function () {
        let url = settings.VOICE_HTTP.replace('https', 'wss');
        url = settings.VOICE_HTTP.replace('http', 'ws');
        websocket.socket = new WebSocket(url + '/websocket');

        websocket.socket.onmessage = function (e) {
            websocket.recieve(e.data);
        }

        websocket.socket.onopen = function () {
            voice.updateStatusHTML('yellow', 'Socket opened');
            websocket.connected = true;
        }

        websocket.socket.onclose = function (e) {
            voice.updateStatusHTML('red', 'Socket closed');
            websocket.connected = false;

            // reconnect
            if (websocket.keep_connected) {
                console.log('websocket closed, reconnecting...', e)
                websocket.connect();
            }
        }

        websocket.socket.onerror = function () {
            websocket.connected = false;

            // socket doesnt supply listening status, so fetch json to update status anyway
            voice.checkIfListening();
        }
    },

    showIrMsg: function (args) {
        const title = 'IR ' + args[1] + ' verzonden naar ' + args[0];
        msg.show(title);
    },

    ucFirst: function(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    },

      
    recieve: function (txt) {
        // console.log('websocket txt: ' + txt.toString());
        const json = websocket.decodeJSON(txt);

        if (json !== false) {
            if (json.infrared) {
                if (typeof json.infrared.ran_func != 'undefined') {
                    websocket.showIrMsg(json.infrared.args);
                }
            }

            if (typeof json.mouse != 'undefined' && typeof json.mouse.screen != 'undefined') {
                mouse.remote_screen_resolution = json.mouse.screen;
            }

            if (json.voice) {
                if (typeof json.voice.status != 'undefined') {
                    voice.updateStatus(json.voice.status);
                }

                if (typeof json.voice.detected != 'undefined') {
                    if (json.voice.detected == 'porcupine') {
                        if (typeof json.voice.keyword != 'undefined') {
                            msg.show(websocket.ucFirst(json.voice.keyword) + ' hotword gehoord');
                        }

                        msg.show('Google luistert nu naar zinnen/trefwoorden', 'warning');
                        voice.updateStatus(json.voice);
                    }

                    if (json.voice.detected == 'google') {
                        msg.show('Spraakherkenning hoorde "' + json.voice.talk + '"');
                        voice.updateStatus({ is_listening: true });
                    }

                    if (json.voice.detected == false) {
                        let message = 'Spraakherkenning is mislukt';
                        if (json.voice.error) {
                            message = json.voice.error;
                        }

                        msg.show(message, 'danger');
                        voice.updateStatus({ is_listening: true });
                    }
                }

                if (typeof json.voice.func_called != 'undefined' && json.voice.func_called == true) {
                    if (json.voice.module == 'infrared') {
                        // voice heeft ir verstuurt
                        const remote_key = json.voice.args[0]
                        websocket.showIrMsg(['de tv', remote_key]);
                    }
                }
            }
        }
    },

    decodeJSON: function (data) {
        try {
            return JSON.parse(data);
        } catch (e) {
            return false
        }
    },

    // see if there are actions in json
    interpetJSON: function (json) {
        if (json.msg) {
            // show message in browser
            let type = 'warning';

            if (typeof json.msg.type != 'undefined') {
                type = json.msg.type;
            }

            msg.show(json.msg.content, type);
        }
    },

    send: function (txt = false) {
        if (websocket.socket === false) {
            console.error('ERR: Websocket is not initialized');
            return false;
        }

        websocket.socket.send(txt);
    }
}