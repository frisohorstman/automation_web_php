/* global settings */
/* global msg */

$(function () {
    voice.init()
})

const voice = {
    timers: [],

    init: function () {
        voice.bindClicks();
    },

    bindClicks: function () {
        $('.voice-recognition-stop').click(function() {
            $.getJSON(settings.VOICE_HTTP + "/voice/json", { kill_voice: true }, function (json) {
                if (json.success == true) {
                    msg.show('Spraakherkenning is gestopt');
                    voice.updateStatus(json); // websocket should set this state
                } else {
                    msg.show('Het stoppen is mislukt', 'danger');
                }
            });
        });
        
        $('.voice-recognition-start').click(function() {
            $.getJSON(settings.VOICE_HTTP + "/voice/json", { start_voice: true }, function (json) {
                if (json.success == true) {
                    msg.show('Een moment geduld, spraakherkenning word opgestart...');
                } else {
                    msg.show('Het starten is mislukt', 'danger');
                }
            });
        });
    },

    // ideally the websocket server supplies this info upon connect
    checkIfListening: function() {
        $.getJSON(settings.VOICE_HTTP + "/voice/json", { is_listening: 'get' }, function (json) {
            if (json.success == true) {
                voice.updateStatus(json);
            }
        });
    },

    // change color of listening status
    updateStatus: function (json) {
        // green
        if (typeof json.is_listening != 'undefined' && json.is_listening == true) {
            voice.updateStatusHTML('green', 'Aan het luisteren');

            $('.table-listening-status').removeClass('hidden');
            $('.table-not-listening-status').addClass('hidden');
        }

        // yellow
        if (typeof json.detected != 'undefined') {
            voice.updateStatusHTML('yellow', 'Spraakherkenning');

            $('.table-listening-status').removeClass('hidden');
            $('.table-not-listening-status').addClass('hidden');
        }

        // red
        if (typeof json.is_listening != 'undefined' && json.is_listening == false) {
            voice.updateStatusHTML('red', 'Luistert niet');
            
            $('.table-not-listening-status').removeClass('hidden');
            $('.table-listening-status').addClass('hidden');
        }
    },

    updateStatusHTML: function(color = 'green', text = false) {
        $('.listen-status').removeClass('transparant');
        $('.listen-status').removeClass('red');
        $('.listen-status').removeClass('yellow');
        $('.listen-status').removeClass('green');

        $('.listen-status').addClass(color);
        $('.listen-status').html(text);
    }
}