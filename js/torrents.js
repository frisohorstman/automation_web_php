/* global settings */
/* global msg */

$(function () {
    torrents.init();
})

const torrents = {
    timers: [],

    init: function () {
        torrents.bindClicks();
        torrents.bindDownloadClicks();
        torrents.bindPaginationClick();

        // downloads pagina
        if ($('.torrent-downloads-table').length > 0) {
            torrents.getDownloadsJson();
            torrents.startPolling('downloadsTable');
        }

        // details pagina
        if ($('.torrent-details-table').length > 0) {
            torrents.startPolling('detailsTable');
        }
    },

    getDownloadsJson: function () {
        $.getJSON("/torrent/json", { downloads_overview: true }, function (json) {
            if (json.result == 'success') {
                const ids = [];
                for (const i in json.arguments.torrents) {
                    const torrent = json.arguments.torrents[i];
                    ids.push(torrent.id);

                    // get html element
                    const tr = $('.torrent-downloads-table tbody').find("tr[data-torrent-id='" + torrent.id + "']");

                    if (tr.length == 0) {
                        torrents.downloadsTableAddElement(torrent);
                        continue;
                    }

                    torrents.downloadsTableSetHtml(tr, torrent);
                }

                // run trough html elements and remove ones that arent in our data anymore.
                $('.torrent-downloads-table tbody tr').each(function () {
                    const elm_id = $(this).data('torrent-id');

                    if (!ids.includes(elm_id)) {
                        $(this).remove();
                    }
                })
            } else {
                msg.show('Ophalen torrent download is mislukt.', 'danger');
            }
        });
    },

    downloadsTableSetHtml: function (tr, torrent) {
        // set torrent id as html data
        $(tr).data('torrent-id', torrent.id);
        $(tr).find('.mass-checkbox').val(torrent.id);

        // set torrent id as html data
        $(tr).find('.title').html('<a href="/torrent/details?id=' + torrent.id + '">' + torrent.name + '</a>');

        // set progress bar width
        const percentDone = parseFloat(torrent.percentDone * 100).toFixed(2);
        $(tr).find('.torrent-remove').data('percent-done', percentDone);
        const progress_bar = $(tr).find('.progress-bar');
        progress_bar.width(percentDone + '%');

        // set text inside progress bar
        const bytesDownload = torrent.sizeWhenDone - torrent.leftUntilDone;
        const text = percentDone + '%' + ' (' + torrents.formatBytes(bytesDownload) + '/' + torrents.formatBytes(torrent.sizeWhenDone) + ')';
        $(tr).find('.progress-text').html(text);

        // set eta
        let eta = '-'
        if (torrent.eta > 0) {
            eta = torrents.etaHuman(torrent.eta);
        }

        $(tr).find('.eta').html(eta);

        // downspeed en upspeed
        $(tr).find('.downspeed').html(torrents.formatBytes(torrent.rateDownload) + '/s');
        $(tr).find('.upspeed').html(torrents.formatBytes(torrent.rateUpload) + '/s');

        // buttons
        $(tr).find('.torrent-remove').attr('href', '/torrent/json?remove=' + torrent.id);
        $(tr).find('.torrent-start').attr('href', '/torrent/json?start=' + torrent.id);
        $(tr).find('.torrent-stop').attr('href', '/torrent/json?stop=' + torrent.id);

        if (torrent.status == 4 || torrent.status == 6) {
            $(tr).find('.torrent-start').hide();
        } else {
            $(tr).find('.torrent-start').show();
        }

        if (torrent.status != 4) {
            $(tr).find('.torrent-stop').hide();
        } else {
            $(tr).find('.torrent-stop').show();
        }
    },

    downloadsTableAddElement: function (torrent) {
        const tr = $('.torrent-downloads-table tbody tr:nth-child(1)').clone();
        $(tr).attr('data-torrent-id', torrent.id);
        torrents.downloadsTableSetHtml(tr, torrent);
        tr.appendTo('.torrent-downloads-table tbody');
    },

    bindDownloadClicks: function (elm = '#torrent-downloads-page') {
        $(elm).on('click', '.torrent-remove', function (e) {
            e.preventDefault();

            torrents.remove($(this));
            torrents.getDownloadsJson();
        });

        $(elm).on('click', '.torrent-start', function (e) {
            e.preventDefault();

            torrents.start($(this));
            torrents.getDownloadsJson();
        });

        $(elm).on('click', '.torrent-stop', function (e) {
            e.preventDefault();

            torrents.stop($(this));
            torrents.getDownloadsJson();
        });
    },

    bindClicks: function () {
        $('#torrent-downloads-page').on('click', '#torrent-add-file-btn', function (e) {
            e.preventDefault();
            $('#modal_add_torrent').modal('show');
        });

        $('#torrent-downloads-page').on('click', 'input.check-all', function () {
            const check_state = $(this).prop('checked');

            const all_checkboxes = $(this).closest('#torrent-downloads-page').find('input[type=checkbox]');
            all_checkboxes.prop('checked', check_state);
        });

        $('.mass-action-container li a').click(function () {
            $('#mass_action').val($(this).data('action'));
            $('#torrent-downloads-form').submit();
        });

        $('#torrent-details-page').on('mousedown', '.torrent-priority', function () {
            clearInterval(torrents.timers['details']);
        });

        $('#torrent-details-page').on('change', '.torrent-priority', function () {
            torrents.setPriority($(this));
            torrents.startPolling('detailsTable');
        });

        $('#modal_add_torrent .btn-save').click(function (e) {
            torrents.modalSave(e);
        });

        $('#modal_add_torrent #torrent').change(function () {
            $('#modal_add_torrent form').submit();
        });
    },

    modalSave: function (e) {
        if ($('#modal_add_torrent #magnet').val() == '') {
            return;
        }

        // when a magnet link is added
        e.preventDefault();
        $.getJSON("/torrent/json", { addMagnet: $('#modal_add_torrent #magnet').val() }, function (json) {
            if (json.result == 'success') {
                msg.show('Torrent is toegevoegd');
            } else {
                msg.show('Torrent toevoegen is mislukt. Wellicht staat de Transmission app uit.', 'danger');
            }
        });

        $('#modal_add_torrent #magnet').val('')
        $('#modal_add_torrent').modal('hide');
    },

    bindPaginationClick: function () {
        $('.torrent-search-table-container .pagination').on('click', 'a', function (e) {
            e.preventDefault();

            const url = '/torrent/search/table?search_url=' + $(this).prop('href');
            torrents.getHtmlTable(url, '.torrent-search-table-container');
        });

        $('#torrent-search-page .torrent-magnet').click(function (e) {
            e.preventDefault();
            torrents.addMagnet($(this));
        });
    },

    // refresh html table every second
    startPolling: function (what = 'downloadsTable') {
        const interval = 1500;

        if (what == 'downloadsTable') {
            // torrents.timers['downloads'] = setInterval(function () {
            //     torrents.getHtmlTable('/torrent/downloads/table', '.torrent-downloads-table', 'downloads');
            // }, interval);
            torrents.timers['downloads'] = setInterval(function () {
                torrents.getDownloadsJson();
            }, interval);
        }

        if (what == 'detailsTable') {
            torrents.timers['details'] = setInterval(function () {
                torrents.getHtmlTable('/torrent/details/table', '.torrent-details-table', 'details');
            }, interval);
        }
    },

    getHtmlTable: function (url, clss, timer = false) {
        if ($(clss).length == 0 && timer != false) {
            console.log('table on page dissapeared... url', url, 'ending timer', timer, 'clss', clss, 'elm', $(clss))
            clearInterval(torrents.timers[timer]);
            return false;
        }

        let args = {}
        if (typeof settings.torrent_id != 'undefined') {
            args = { id: settings.torrent_id };
        }

        $.ajax({
            url: url,
            type: 'GET',
            data: args,
            dataType: 'html',
            success: function (data) {
                $(clss)[0].outerHTML = data;

                if (clss == '.torrent-search-table-container') {
                    torrents.bindPaginationClick();
                }
            },
            error: function (request, error) {
                msg.show('ERROR, kan html table niet updaten. ' + error, 'danger');
            }
        });
    },

    addMagnet: function (elm) {
        // extract magnet link from url via php
        const externalUrl = elm.prop('href');

        $.getJSON("/torrent/json", { getMagnet: externalUrl }, function (json) {
            $.getJSON("/torrent/json", { addMagnet: json.magnet }, function (json) {
                if (json.result == 'success') {
                    msg.show('Torrent is toegevoegd');
                } else {
                    msg.show('Torrent toevoegen is mislukt. Wellicht staat de Transmission app uit.', 'danger');
                }
            });
        });
    },

    start: function (elm) {
        $.getJSON(elm.prop('href'), function (json) {
            if (json.result == 'success') {
                msg.show('Torrent is gestart');
            } else {
                msg.show('Torrent starten is mislukt', 'danger');
            }
        });
    },

    stop: function (elm) {
        $.getJSON(elm.prop('href'), function (json) {
            if (json.result == 'success') {
                msg.show('Torrent is gepauzeerd');
            } else {
                msg.show('Torrent pauzeren is mislukt', 'danger');
            }
        });
    },

    remove: function (elm) {
        let delete_data = false;
        if (elm.data('percent-done') != 100) {
            delete_data = true;
        }

        $.getJSON(elm.prop('href'), { delete_data: delete_data }, function (json) {
            if (json.result == 'success') {
                msg.show('Torrent is verwijderd');
            } else {
                msg.show('Torrent verwijderen is mislukt', 'danger');
            }
        });
    },

    setPriority: function (elm) {
        const args = {
            set_priority: elm.val(),
            id: elm.data('torrent-id'),
            file_id: elm.data('file-id')
        };

        $.getJSON('/torrent/json', args, function (json) {
            if (json.result == 'success') {
                msg.show('Prioriteit is gewijzigd');
            } else {
                msg.show('Prioriteit wijzigen is mislukt', 'danger');
            }
        });
    },

    formatBytes: function (bytes, decimals = 2) {
        if (bytes === 0) return '0 B';

        const k = 1024;
        const dm = decimals < 0 ? 0 : decimals;
        const sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

        const i = Math.floor(Math.log(bytes) / Math.log(k));

        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    },

    etaHuman: function (seconds) {
        // const seconds = Math.floor((new Date() - date) / 1000);
        let interval = seconds / 31536000;

        if (interval > 1) {
            return Math.floor(interval) + " jaar";
        }
        interval = seconds / 2592000;
        if (interval > 1) {
            return Math.floor(interval) + " maanden";
        }
        interval = seconds / 86400;
        if (interval > 1) {
            const days = Math.floor(interval);
            
            if (days == 1) {
                return "1 dag";
            }
            return days + " dagen";
        }
        interval = seconds / 3600;
        if (interval > 1) {
            return Math.floor(interval) + " uur";
        }
        interval = seconds / 60;
        if (interval > 1) {
            return Math.floor(interval) + " minuten";
        }

        return Math.floor(seconds) + " seconden";
    }
}