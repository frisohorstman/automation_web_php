/* global settings */
/* global dashjs */
/* global media */
/* global msg */

$(document).ready(function () {
    vcr.init();
});

const vcr = {
    timers: [],
    video_player: false,
    stream_manifest: '/stream/dash/manifest.mpd',

    init: function () {
        vcr.bindClicks();

        // when a #vcr_stream id exists on a page, start stream
        if ($('#vcr_stream').length > 0) {
            if (settings.vcr_status.is_streaming) {
                vcr.loadVideoPlayerHTML();
            } else {
                vcr.hideShowStreamElements(false);
            }
        }

        // when on #vcr-record-page
        if ($('#vcr-record-page').length > 0) {
            vcr.hideShowRecordElements(settings.vcr_status.is_recording);

            if (settings.vcr_status.is_recording) {
                vcr.startPollingVideoRecordProgress();
            }
        }
    },

    bindClicks: function () {
        $('.stop-vcr-stream').click(function () {
            // stop ffmpeg
            vcr.stopVideoStream($(this));
        });

        $('.load-vcr-stream-in-browser').click(function () {
            // load html video tag
            vcr.loadVideoPlayerHTML();
        });

        $('.start-vcr-ffmpeg-stream-on-server').click(function () {
            // start ffmpeg
            vcr.startVideoStream($(this));
        });

        $('.start-vcr-record').click(function () {
            vcr.startVideoRecord();
        });

        $('.stop-vcr-record').click(function () {
            vcr.stopVideoRecord($(this));
        });

        $('.start-vlc-vcr-on-server').click(function () {
            vcr.startVlc();
        });

        $('.stop-vlc-vcr-on-server').click(function () {
            vcr.stopVlc();
        });
    },

    startVlc: function () {
        $.getJSON("/media/json", { vlc: 'play_vcr' }, function (json) {
            if (json.success == true) {
                msg.show('VLC is opgestart');
                location.reload();
            } else {
                msg.show('Het starten van VLC is mislukt', 'danger');
            }
        });
    },

    stopVlc: function () {
        $.getJSON("/media/json", { vlc: 'stop_vcr' }, function (json) {
            if (json.success == true) {
                msg.show('VLC is beëindigt');
                location.reload();
            } else {
                msg.show('Het stoppen van VLC is mislukt', 'danger');
            }
        });
    },

    stopPollingVideoRecordProgress: function () {
        clearInterval(vcr.timers['get_json']);
    },

    startPollingVideoRecordProgress: function () {
        vcr.stopPollingVideoRecordProgress();

        vcr.timers['get_json'] = setInterval(function () {
            vcr.getVideoRecordProgress()
        }, 1000);
    },

    getVideoRecordProgress: function () {
        $.getJSON("/media/json", { get_recording_progress: true }, function (json) {
            if (json.success == true) {
                $('.convert_progress').show();

                let title = 'Voortgang';
                if (json.progress.details.is_conversion) {
                    title = "Voortgang conversie";
                }

                $('.voortgang-title').html(title);
                $('#record_size').html(json.progress.details.size);
                $('#record_frames').html(json.progress.details.frames);
                $('#record_time').html(json.progress.details.time);
                $('#record_total_length').html(json.length ? json.length : json.progress.details.time); // als length false is dan draait ffmpeg niet meer
                const filename_html = '<a href="/media/file?filepath=' + json.filepath + '">' + json.progress.details.filename + '</a>';
                $('#record_filename').html(filename_html);
                $('.progress-bar').css('width', json.progress.percentage + '%');
                $('.progress-text').text(json.progress.percentage + '%');

                if (json.progress.details.is_conversion) {
                    $('#record_time_name').html('Geconverteerde tijd');
                    $('.stop-vcr-record span').html('Stop conversie');
                } else {
                    $('#record_time_name').html('Opgenomen tijd');
                    $('.stop-vcr-record span').html('Stop opname');
                }

                if (json.progress.percentage == 100) {
                    if ($('#convert_checkbox').prop('checked') && json.progress.details.is_conversion) {
                        vcr.stopPollingVideoRecordProgress();
                        vcr.hideShowRecordElements(false);
                    }
                    if (!$('#convert_checkbox').prop('checked') && !json.progress.details.is_conversion) {
                        vcr.stopPollingVideoRecordProgress();
                        vcr.hideShowRecordElements(false);
                    }
                }
            } else {
                msg.show('Het ophalen van de opname status is mislukt', 'danger');
            }
        });
    },

    /**
     * gets user defined video record length
     */
    getVideoRecordLength: function () {
        let hh = $('#record_hh').val();
        if (hh == '') {
            hh = '00';
        }

        let mm = $('#record_mm').val();
        if (mm == '') {
            mm = '00';
        }

        let ss = $('#record_ss').val();
        if (ss == '') {
            ss = '00';
        }

        return hh + ':' + mm + ':' + ss;
    },

    startVideoRecord: function () {
        const duration = vcr.getVideoRecordLength();

        if (duration == '00:00:00') {
            alert('Definieer opname length');
            return false;
        }

        let convert = 'no';
        if ($('#convert_checkbox').prop('checked')) {
            convert = 'yes';
        }

        let infrared = 'no';
        if ($('#playstop_checkbox').prop('checked')) {
            infrared = 'yes';
        }

        $.getJSON("/media/json", { start_recording: 'vcr', duration: duration, convert: convert, infrared: infrared }, function (json) {
            if (json.success == true) {
                msg.show('De opname is gestart');
                vcr.hideShowRecordElements(true);
                vcr.startPollingVideoRecordProgress();
            } else {
                msg.show('Het opstarten van de opname is mislukt', 'danger');
            }
        });
    },

    stopVideoRecord: function (btn) {
        $.getJSON("/media/json", { stop_recording: 'vcr' }, function (json) {
            if (json.success == true) {
                msg.show('De opname is gestopt');
                vcr.hideShowRecordElements(false);

                if (btn.hasClass('reload')) {
                    location.reload();
                }
            } else {
                msg.show('Het stoppen van de opname is mislukt', 'danger');
            }
        });
    },

    /**
     * hide and show certain buttons when on the stream page/
     */
    hideShowStreamElements: function (is_streaming = false) {
        if (is_streaming) {
            $('#vcr_stream').show();
            $('.start-vcr-ffmpeg-stream-on-server').hide();
            $('.stop-vcr-stream').show();
            $('.load-vcr-stream-in-browser').show();
        } else {
            $('#vcr_stream').hide();
            $('.start-vcr-ffmpeg-stream-on-server').show();
            $('.stop-vcr-stream').hide();
            $('.load-vcr-stream-in-browser').hide();
        }
    },

    /**
     * hide and show certain buttons when on recording page
     */
    hideShowRecordElements: function (is_recording = false) {
        if (is_recording) {
            $('.stop-vcr-record').show();
            $('.start-vcr-record').hide();

            $('#playstop_checkbox').prop('disabled', true);
            $('#convert_checkbox').prop('disabled', true);
            $('#record_hh').prop('disabled', true);
            $('#record_mm').prop('disabled', true);
            $('#record_ss').prop('disabled', true);
        } else {
            $('.stop-vcr-record').hide();
            $('.start-vcr-record').show();

            $('#playstop_checkbox').prop('disabled', false);
            $('#convert_checkbox').prop('disabled', false);
            $('#record_hh').prop('disabled', false);
            $('#record_mm').prop('disabled', false);
            $('#record_ss').prop('disabled', false);
        }
    },

    /**
     * fetch stream manifest
     */
    getVideoStreamManifest: function () {
        $.ajax({
            url: vcr.stream_manifest,
            statusCode: {
                404: function () {
                    msg.show('Geen manifest. FFMPEG VCR stream opstarten.', 'warning');
                }
            }
        }).done(function () {
            // manifest exists, load video in browser
            msg.show('Er is een manifest');
        });
    },

    startVideoStream: function (btn) {
        $.getJSON("/media/json", { start_stream: 'vcr' }, function (json) {
            if (json.success == true) {
                vcr.waitForManifest();

                if (btn.hasClass('redirect')) {
                    window.location.replace('/media/vcr/stream');
                }
            } else {
                msg.show('Het opstarten van de VCR stream is mislukt', 'danger');
            }
        });
    },

    stopVideoStream: function (btn) {
        if (vcr.video_player != false) {
            // vcr.video_player.pause();
            vcr.video_player.destroy();
        }

        $.getJSON("/media/json", { stop_stream: 'vcr' }, function (json) {
            if (json.success == true) {
                msg.show('De stream is gestopt.');
                vcr.hideShowStreamElements(false);

                if (btn.hasClass('reload')) {
                    location.reload();
                }
            } else {
                msg.show('Het stoppen van de stream is mislukt', 'danger');
            }
        });
    },

    waitForManifest: async function (attempt = 0) {
        await media.sleep(1000);
        msg.show('Waiting for manifest...');

        $.ajax({
            url: vcr.stream_manifest,
            statusCode: {
                404: function () {
                    vcr.waitForManifest(attempt++);
                }
            }
        }).done(function () {
            vcr.loadVideoPlayerHTML();
        });
    },

    loadVideoPlayerHTML: async function () {
        msg.show('Media player voor VCR dash stream laden');
        vcr.hideShowStreamElements(true);

        // actually load video_player
        if (vcr.video_player !== false) {
            vcr.video_player.reset();
        }
        vcr.video_player = dashjs.MediaPlayer().create();
        vcr.video_player.updateSettings({ 'debug': { 'logLevel': dashjs.Debug.LOG_LEVEL_NONE } });
        vcr.video_player.initialize(document.getElementById("vcr_stream"), vcr.stream_manifest, true);
    }
}