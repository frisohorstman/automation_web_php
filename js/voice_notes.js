$(function () {
    voice_notes.init()
})

const voice_notes = {
    init: function () {
        $('#modal_voice_notes .btn-hide').click(function () {
            voice_notes.hide_btn_click();
        });

        $('#voice_note_create').click(function () {
            voice_notes.create_btn_click();
        });

        $('.voice_notes_table_click').click(function () {
            voice_notes.table_item_click($(this));
        });
    },

    // load data from table into modal form
    table_item_click: function (elm) {
        $('#modal_voice_notes .btn-delete').show();
        $('#modal_voice_notes .btn-hide').show();

        const note_id = elm.data('id');
        const note = elm.data('note');
        const hidden_in_dashboard = elm.data('hidden-in-dashboard');

        $('#modal_voice_notes').find('#note_id').val(note_id);
        $('#modal_voice_notes').find('#note').val(note);
        $('#modal_voice_notes').find('.hidden-in-dashboard-checkbox input').prop('checked', hidden_in_dashboard);
        if (hidden_in_dashboard) {
            $('#modal_voice_notes').find('.hidden-in-dashboard-checkbox').show();
            $('#modal_voice_notes').find('.btn-hide').hide();
        } else {
            $('#modal_voice_notes').find('.hidden-in-dashboard-checkbox').hide();
            $('#modal_voice_notes').find('.btn-hide').show();
        }

        $('#modal_voice_notes').modal('show');
    },

    // create button is hit, prepare modal for create action
    create_btn_click: function () {
        $('#modal_voice_notes .btn-delete').hide();
        $('#modal_voice_notes .btn-hide').hide();
        $('#modal_voice_notes').find('#note_id').val('create');

        $('#modal_voice_notes').find('#note').val('');

        $('#modal_voice_notes').modal('show');
    },

    hide_btn_click: function () {
        const note_id = $('#modal_voice_notes').find('#note_id').val()
        document.location = '?hide_in_dashboard&note_id=' + note_id;
    }
}