/*global msg*/

$(document).ready(function () {
    vlc.buttonsBindClicks();
    
    if ($('.media-controls.vlc').length > 0) {
        vlc.init();
    }
});

const vlc = {
    timers: [],
    video_info: [],

    init: function () {
        vlc.sliderBindEvents();

        vlc.startPolling();
        $(window).bind('beforeunload', function () {
            vlc.stopPolling();
        });
    },

    buttonsBindClicks: function () {
        $('.play-in-vlc').click(function () {
            $.getJSON("/vlc/json", { vlc: 'open_filepath', filepath: $(this).data('filepath') }, function (json) {
                if (json.success == true) {
                    msg.show('Bestand is geopend in VLC');
                    vlc.startPolling();
                } else {
                    msg.show('Het opstarten van VLC is mislukt...', 'danger');
                }
            });
        });

        $('.media-controls.vlc .goto-file').click(function () {
            document.location = '/media/file?filepath=' + vlc.video_info.video_filepath;
        });

        $('.stop-vlc-filepath').mousedown(function () {
            $(this).data('time',  Date.now());
        });
        $('.stop-vlc-filepath').mouseup(function () {
            const press_time =  Date.now() - $(this).data('time');
            console.log('press_time', press_time)
            const me = $(this);

            let query = { vlc: 'stop' }
            if(press_time > 1000) {
                query.stop_any = true;
            }
            $.getJSON("/vlc/json", query, function (json) {
                if (json.success == true) {
                    msg.show('VLC is gestopt');

                    if (me.hasClass('reload')) {
                        location.reload();
                    }
                } else {
                    msg.show('Het stoppen van VLC is mislukt...', 'danger');
                }
            });
        });

        $('.media-controls.vlc .keystroke').click(function () {
            if ($(this).hasClass('pause')) {
                $(this).hide();
                $('.media-controls.vlc .play').show();
            }

            if ($(this).hasClass('play')) {
                $(this).hide();
                $('.media-controls.vlc .pause').show();
            }

            $.getJSON("/vlc/json", { keystroke: $(this).data('keystroke') }, function (json) {
                if (!json.success) {
                    msg.show('Keystroke uitvoeren is mislukt...', 'danger');
                }
            });
        });
    },

    sliderBindEvents: function () {
        $('.media-controls.vlc .bar').mousedown(function () {
            vlc.stopPolling();
        });

        $(document).on('input', '.media-controls.vlc .bar', function () {
            const seek_seconds = vlc.sliderGetSeconds();
            const video_time = new Date(seek_seconds * 1000).toISOString().substr(11, 8);

            $('.media-controls.vlc .time').text(video_time);
        });

        $('.media-controls.vlc .bar').mouseup(function () {
            vlc.startPolling();

            const video_seek = vlc.sliderGetSeconds();
            vlc.seekTime(video_seek);
        });
    },

    sliderGetSeconds: function () {
        const slider_percentage = $('.media-controls .bar').val() / 100; // 10000 / 100 = 1 tm 100%
        const video_seek = (vlc.video_info.video_length / 100) * slider_percentage;

        return Math.round(video_seek);
    },

    seekTime: function (seek_time) {
        $.getJSON("/vlc/json", { vlc: 'seek', 'time': seek_time }, function (json) {
            if (!json.success) {
                msg.show('Seek <time> in VLC is mislukt.', 'danger');
            }
        });
    },

    stopPolling: function () {
        clearInterval(vlc.timers['get_json']);
    },

    startPolling: function () {
        vlc.stopPolling();

        vlc.timers['get_json'] = setInterval(function () {
            vlc.getDetails()
        }, 1000);
    },

    getDetails: function () {
        $.getJSON("/vlc/json", { vlc: 'get_time' }, function (json) {
            if (json.success == true) {
                vlc.video_info = json;
                $('.media-controls.vlc').show();

                // set text values
                const video_time = new Date(json.video_time * 1000).toISOString().substr(11, 8)
                const video_length = new Date(json.video_length * 1000).toISOString().substr(11, 8)

                $('.media-controls.vlc .time').text(video_time);
                $('.media-controls.vlc .length').text(video_length);

                // set position of slider in control progress bar
                let percentage = (json.video_time / json.video_length) * 100;
                percentage = Math.round(percentage * 100); // .bar loopt tot 10000 for fijnere beweging
                $('.media-controls.vlc .bar').val(percentage);

                // show/hide play button
                if (json.video_is_playing) {
                    $('.media-controls.vlc .play').hide();
                    $('.media-controls.vlc .pause').show();
                } else {
                    $('.media-controls.vlc .play').show();
                    $('.media-controls.vlc .pause').hide();
                }
            } else {
                vlc.stopPolling();
                $('.media-controls.vlc').hide();
            }
        });
    }
}