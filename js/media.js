/* global videojs */
/* global vlc */
/* global msg */

$(document).ready(function () {
    media.bindClicks();
});

const media = {
    title_clicks: 0,
    audio_recorder: false,
    audio_chunks: [],

    bindClicks: function () {
        $('#record_audio').on('mousedown touchstart', function () {
            if(media.audio_recorder == false) {
                media.audioRecordAskPermission();
                return;
            }

            media.audioRecord($(this));
        });

        $('#record_audio').on('mouseup touchend', function () {
            if(media.audio_recorder == false) {
                return;
            }

            media.audioRecordStop($(this));
        });

        $('.generate-thumbnails').click(function () {
            const filepath = $(this).data('filepath');

            $.getJSON("/media/json", { generate_thumbnails: true, filepath: filepath }, function (json) {
                if (json.success == true) {
                    msg.show('De thumbnails worden gegenereert.');
                    $('.generate-thumbnails').hide();
                    $('.view-thumbnails').show();
                } else {
                    msg.show('Het genereren is mislukt', 'danger');
                }
            });
        });

        $('.start-stream-in-browser').click(function () {
            $(this).hide();
            const filepath = $(this).data('filepath');
            media.startFilepathVideoStream(filepath);
        });

        $('.stop-filepath-stream').click(function () {
            media.stopFilepathVideoStream($(this));
        });

        $('.open-in-browser').click(function () {
            $('#video_file').show();
            document.getElementById('video_file').play();

            // hide vlc controls (if shown) to not confuse these buttons
            // with the buttons of the html <video> tag player
            $('.media-controls.vlc').hide();
            vlc.stopPolling();
        });

        $('#media-folder-title').click(function () {
            media.title_clicks++;

            if (media.title_clicks == 3) {
                document.location = '/media/folder?filepath=/mnt/sdc1/tmp/2/92/11';
            }
        })
    },

    /**
     * record audio
     */
    audioRecord: function (btn) {
        $(btn).find('span').html('Luistert')
        media.audio_chunks = [];
        media.audio_recorder.start();
    },

    /**
     * stop recording
     */
    audioRecordStop: function (btn) {
        $(btn).find('span').html('Opnemen')

        media.audio_recorder.stop();
    },

    /**
     * the recording was stopped so weve got a recording, act upon it
     */
    audioRecordingPost: function (e) {
        var formData = new FormData();
        formData.append('recorded_audio', e.data);

        $.ajax('/voice/json?recorded_audio', {
            method: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function (response) {
                const json = JSON.parse(response);

                if(json.success) {
                    msg.show('Gehoord "' + json.talk + '"');
                } else {
                    msg.show('Kon spraak in audio niet herkennen', 'danger');
                }
            },
            error: function () {
                console.error('media ajax error')
            }
        });
    },

    /**
     * browser will ask user if it allows audio to be recorded.
     */
    audioRecordAskPermission: function () {
        navigator.mediaDevices.getUserMedia({
            audio: true
        })
            .then(function (stream) {
                media.audio_recorder = new MediaRecorder(stream);

                // when a recording has been made
                media.audio_recorder.addEventListener('dataavailable', media.audioRecordingPost);
                media.audioRecord($('#record_audio'));
            });
    },


    /**
     * input int; miliseconds
     * 
     * wait for x miliseconds
     */
    sleep: function (ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    },

    /**
     * rtmp stream for files
     * 
     */
    startFilepathVideoStream: function (filepath) {
        const unique_id = Math.round(new Date().getTime() + (Math.random() * 100));

        $.getJSON("/media/json", { start_stream: 'filepath', filepath: filepath, video_id: unique_id }, function (json) {
            if (json.success == true) {
                msg.show('Een moment geduld...');
                media.waitForM3u8(unique_id);
            } else {
                msg.show('Het opstarten van de stream is mislukt', 'danger');
            }
        });
    },

    stopFilepathVideoStream: function (btn) {
        $.getJSON("/media/json", { stop_stream: 'filepath' }, function (json) {
            if (json.success == true) {
                msg.show('De stream is gestopt');

                if (btn.hasClass('reload')) {
                    location.reload();
                }
            } else {
                msg.show('Het stoppen van de stream is mislukt', 'danger');
            }
        });
    },

    waitForM3u8: async function (video_id, attempt = 0) {
        await media.sleep(1000);

        // when visiting via internet, server_host has a port behind it
        const server_host = window.location.hostname;

        $.ajax({
            url: 'https://' + server_host + ":8081/hls/" + video_id + ".m3u8",
            statusCode: {
                404: function () {
                    media.waitForM3u8(video_id, attempt++);
                }
            }
        }).done(function () {
            media.loadM3u8VideoStreamHTML(video_id);
        });
    },

    loadM3u8VideoStreamHTML: async function (video_id) {
        await media.sleep(1000);
        msg.show('Stream zou nu moeten beginnen...');

        // when visiting via internet, server_host has a port behind it
        const server_host = window.location.hostname;

        let html = '<video id="stream_player" class="video-js vjs-default-skin" style="width: 100%;" controls>';
        html += ' <source src="https://' + server_host + ':8081/hls/' + video_id + '.m3u8" type="application/x-mpegURL" />';
        html += '</video>'

        $('#stream_wrapper').html(html);
        var player = videojs('stream_player');
        player.fluid(true);
        // player.aspectRatio('16:9');
        player.play();
    },
}