/* global msg */

$(document).ready(function () {
    if ($("[data-zway-device]").length > 0) {
        zway.getDevices();
        zway.startPolling();
    }

    $(window).bind('beforeunload', function () {
        zway.stopPolling();
    });
});


var zway = {
    // internal vars
    timer: false,
    timeout: false,
    boundHtmlElements: false,

    urlConstruct: function (url = false) {
        if (url == false) {
            url = 'ZAutomation/api/v1/devices';
        }

        return '/zway/json?url=' + url;
    },

    // poll light switch state every second so the checkbox(es) on this
    //  page will match even when the actual light switch has been hit.
    startPolling: function () {
        zway.timer = setInterval(function () {
            zway.getDevices();
        }, 1000);
    },

    stopPolling: function () {
        clearInterval(zway.timer);
    },

    getDevices: async function () {
        const url = this.urlConstruct();

        Promise.all([
            $.getJSON(url),
        ]).then(responses => {
            zway.parseDevices(responses[0]);
        }).catch(() => {
            console.error("Couldn't fetch zway JSON so we're calling zway.stopPolling();")
            zway.stopPolling();
        });
    },

    sendCommand: function (device, state = 'on') {
        let url = this.urlConstruct();
        url = url + '/' + device + '/command/' + state;

        Promise.all([
            $.getJSON(url),
        ]).then(responses => {
            zway.parseCommand(responses[0], state);
        }).catch(() => {
            console.error("Couldn't fetch zway JSON so we're calling zway.stopPolling();")
            zway.stopPolling();
        });
    },

    parseCommand: function (json, state) {
        if (json && json.code == 200) {
            let nlword = 'uitgezet';
            if (state == 'on') {
                nlword = 'aangezet';
            }

            msg.show('Licht is ' + nlword);
        } else {
            msg.show('Licht actie is mislukt', 'danger');
        }
    },

    /*
     * recieves json from getJSON, parse this json object set the right state for 
     * checkboxes and button and find html devices on page then bind clicks on elements.
     */
    parseDevices(json) {
        // loop trough json data to find devices and transfer data to html
        Object.values(json.data.devices).forEach(device => {
            if (device.visibility == false) {
                return;
            }

            if (device.deviceType != 'switchBinary') {
                return;
            }

            // check checkbox if lamp is turned on according to zway api
            zway.htmlElementsSetState(device.id, device.metrics.level);

            // bind clicks and checkbox change events for devices, but only if this hasnt been done already
            if (zway.boundHtmlElements == false) {
                zway.boundHtmlElements = true;
                zway.findCheckbox(device.id);
                zway.findButtons(device.id);
            }
        })
    },

    // finds button or checkboxes and set checkbox checked/uncheked or button name to on / off.
    // this according to the state given to this function
    htmlElementsSetState(zway_dev_id = false, state = 'on') {
        $("[data-zway-device='" + zway_dev_id + "']").each(function (i, element) {
            // button
            if ($(element).is('button')) {
                zway.updateButtonHTML(element, state);
            }

            // checkbox
            if ($(element).is('input')) {
                let checked = true;

                if (state == 'off') {
                    checked = false;
                }

                $(element).prop('checked', checked);
            }

            // table td
            if($(element).is('td')) {
                zway.updateTableTdHTML(element, state);
            }
        })
    },
    
    updateButtonHTML: function(element, state) {
        $(element).text('Uit');
        if ($(element).hasClass('btn-outline-success')) {
            $(element).removeClass('btn-outline-success');
            $(element).addClass('btn-outline-danger');
        } else {
            $(element).addClass('btn-outline-primary');
            $(element).removeClass('btn-primary');
        }

        if (state == 'off') {
            // seems reversed, its now off so you want a label for turning it on.
            $(element).text('Aan');
            if ($(element).hasClass('btn-outline-danger')) {
                $(element).removeClass('btn-outline-danger');
                $(element).addClass('btn-outline-success');
            } else {
                $(element).removeClass('btn-outline-primary');
                $(element).addClass('btn-primary');
            }
        }
    },

    // for /status page
    updateTableTdHTML: function(element, state) {
        if(state == 'on') {
            $(element).find('.dot').removeClass('danger');
            $(element).find('span').html('De ganglamp staat aan');
        } else {
            $(element).find('.dot').addClass('danger');
            $(element).find('span').html('De ganglamp staat uit');
        }
    },

    /*
     * Finds button(s) and make clickable.
     * Hide one button depending on the state of the light, wether it is on or off.
     */
    findButtons(zway_dev_id) {
        const button_elm = $("button[data-zway-device='" + zway_dev_id + "']");

        if (button_elm.length == 0) {
            // console.log('WARNING: Could not find zway device "' + zway_dev_id + '" on page.');
            return;
        }

        // what to do when the checkbox is pressed
        button_elm.click(function () {
            // determine the current state
            let state = 'on';

            if ($(this).text() == 'Uit') {
                state = 'off';
            }

            zway.htmlElementsSetState($(this).data('zway-device'), state);
            zway.btnClick($(this), state);
        });
    },

    // gets fired upon btn click on button or checkbox
    btnClick(elm, state = 'on') {
        // clear the timer so the checkbox doesnt get checked/unchecked instantly
        // because it is still reading the zway json
        clearTimeout(zway.timeout);
        clearInterval(zway.timer);

        const device = $(elm).data('zway-device');
        zway.sendCommand(device, state);

        zway.timeout = setTimeout(function () {
            zway.startPolling();
        }, 1000);
    },

    /*
     * Finds a checkbox toggle switch on the webpage.
     * Make it checked depending on the light is on or off.
     */
    findCheckbox(zway_dev_id) {
        const checkbox_elm = $("input[data-zway-device='" + zway_dev_id + "']");

        if (checkbox_elm.length == 0) {
            // console.log('WARNING: Could not find zway device "' + zway_dev_id + '" on page.');
            return;
        }

        // what to do when the checkbox is pressed
        checkbox_elm.click(function () {
            let state = 'off';

            if ($(this).prop('checked')) {
                state = 'on';
            }

            zway.htmlElementsSetState($(this).data('zway-device'), state);
            zway.btnClick($(this), state);
        });
    },
}
