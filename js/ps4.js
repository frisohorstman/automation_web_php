/* global msg */

$(function() {
    // ps4 power button was pressed, replace button with loader gif until answered
    $('#ps4-power-on, #ps4-power-off').click(function() {
        ps.btnClick($(this));
    })
})


const ps = {
    getJSON(power = 'on', btn_id = false) {
        $.get('/playstation/json', { ps4Power: power }, function (data) {
            $('#ps4_preloader').remove();
            const nlpower = (power == 'on') ? 'aan' : 'uit';

            // check if there is any data
            if( !data || data === ""){
                msg.show('A fetching error occurred, turning Playstation ' + power + ' failed.');
                $('#' + btn_id).show();
                return;
            }
    
            // check if json
            let json;
            try {
                json = jQuery.parseJSON(data);
            } catch (e) {
                $('#' + btn_id).show();
                msg.show('A JSON error occurred, turning Playstation ' + power + ' failed.');
                return;
            }
            
            console.log('de json', json);
            if(json.success == false) {
                msg.show('De Playstation ' + nlpower + 'zetten is mislukt', 'danger');
                $('#' + btn_id).show();
                return;
            }
            
            // success, show opposite button
            let show = 'ps4-power-on';
            if(power == 'on') {
                show = 'ps4-power-off';
            }
            $('#' + show).removeClass('hidden');
            $('#' + show).show();

            // use json here
            console.log('de ps4 json', json);
            msg.show('De Playstation staat nu ' + nlpower);
        }, "text");
    },

    showPreloader(elm) {
        const btn_width = $(elm).outerWidth() + parseFloat($(elm).css('margin-right'));
    
        let loader_img  = '';
        loader_img += '<div id="ps4_preloader" class="ajax-loader" style="display: inline-block; width: ' + btn_width +'px;">';
        loader_img += '  <img src="/img/ajax-loader.gif" /></div>';
        loader_img += '</div>';
    
        $(elm).parent().append(loader_img);
    },

    /**
     * a click has been done
     */
    btnClick(elm) {
        const btn_id = $(elm).prop('id');
        $(elm).hide();

        // show preloader
        ps.showPreloader(elm);

        // ask php to turn on ps4
        let power = 'off';
        if(btn_id == 'ps4-power-on') {
            power = 'on';
        }

        ps.getJSON(power, btn_id);
    }
}
