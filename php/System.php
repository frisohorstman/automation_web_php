<?php

class System {
    static function exec($cmd) {
        $output = null;
        exec($cmd, $output, $retval);

        if($retval !== 0) {
            return false;
        }

        return $output;
    }

    static function cpuInfo() {
        $txt = file_get_contents('/proc/cpuinfo');
        
        $info = array();
        $separator = "\r\n";
        $line = strtok($txt, $separator);
        
        while ($line !== false) {
            $line = strtok( $separator );
            if(empty($line)) {
                continue;
            }
            
            list($key, $value) = explode(':', $line);
            $info[trim($key)] = trim($value);
        }

        return $info;
    }
}