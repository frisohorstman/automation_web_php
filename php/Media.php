<?php

class Media extends Boot {
    private $order = 'filename';
    private $by = 'ASC';
    private $search = '';

    /* folder page */
    function folderPage() {
        $smarty = $this->smarty();
        
        // get files in folder 
        if(isset($_GET['filepath'])) {
            $folder = $_GET['filepath'];
        } else {
            $folder = '/mnt/sda1/Downloads';
            $this->order = 'created';
            $this->by = 'DESC';
        }
        
        $media = $this->globFolder($folder);        
        $smarty->assign('media', $media);
        
        // filters
        $rev_by = 'ASC';
        if($this->by == 'ASC') {
            $rev_by = 'DESC';
        }
        
        $smarty->assign('filters', array(
            'order' => $this->order,
            'by' => $this->by,
            'rev_by' => $rev_by,
            'search' => $this->search
        ));
        
        // free disk space
        $parts = explode('/', $folder);
        $disk_space = $this->diskSpaceFree($parts[2]);
        // print_r($disk_space);die();
        $smarty->assign('disk_space', $disk_space);

        $this->smartyDisplay($smarty, 'pages/media/folder_page.tpl');
    }

    /* file page */
    function filePage() {
        $smarty = $this->smarty();
        
        $is_video = false;
        list($type, $ext) = explode('/', mime_content_type($_GET['filepath']));

        if($type == 'video') {
            $is_video = true;
        }

        $filepath = $_GET['filepath'];
        $parent_folder = dirname($filepath);
        $filename = basename($filepath);
        
        $next_filepath = false;
        $parent = $this->globFolder($parent_folder);
        foreach($parent['files'] as $i => $file) {
            if($filepath == $file['filepath']) {
                if(isset($parent['files'][$i + 1])) {
                    $next_filepath = $parent['files'][$i + 1]['filepath'];
                    break;
                }
            }
        }

        $video_id3 = false;
        if($is_video) {
            include_once('php/lib/getid3/getid3.php');
            $id3 = new getID3();
            $file = $id3->analyze($filepath);

            $video_id3 = array(
                'video' => $file['video'],
                'playtime' => $file['playtime_string'],
                'bitrate' => $file['bitrate']
            );
        }

        $has_thumbnails = false;
        $thumb_folder = $parent_folder.'/screens';
        if(file_exists($thumb_folder)) {
            $has_thumbnails = true;
        }

        $smarty->assign('media', array(
            'parent_folder' => $parent_folder,
            'thumbnails' => $has_thumbnails,
            'filepath' => urlencode($filepath),
            'next_filepath' => urlencode($next_filepath),
            'filename' => $filename,
            'filesize' => $this->formatBytes(filesize($filepath)),
            'content_type' => mime_content_type($filepath),
            'is_video' => $is_video,
            'video_id3' => $video_id3,
            'created' => filemtime($filepath),
        ));

        $this->smartyDisplay($smarty, 'pages/media/file_page.tpl');
    }

    /* thumbnails page */
    function thumbnailsPage() {
        $smarty = $this->smarty();

        $thumbnails = $this->globFolder($_GET['folder']);
        $smarty->assign('thumbnails', $thumbnails);
        $smarty->assign('folder', $_GET['folder']);

        $this->smartyDisplay($smarty, 'pages/media/thumbnails_page.tpl');
    }

    /* json page @todo, vervang met switch */
    function jsonPage() {
        if(isset($_GET['start_stream'])) {
            if($_GET['start_stream'] == 'vcr') {
                return Vcr::startStream();
            }
    
            if($_GET['start_stream'] == 'filepath') {
                return $this->startFilepathVideoStream();
            }
        }

        if(isset($_GET['stop_stream'])) {
            if($_GET['stop_stream'] == 'vcr') {
                return Vcr::stopStream();
            }

            if($_GET['stop_stream'] == 'filepath') {
                return Media::stopFilepathVideoStream();
            }
        }

        if(isset($_GET['generate_thumbnails'])) {
            return Media::generateThumbnails();
        }

        if(isset($_GET['start_recording'])) {
            return Vcr::startRecording();
        }

        if(isset($_GET['stop_recording'])) {
            return Vcr::stopRecording();
        }

        if(isset($_GET['get_recording_progress'])) {
            return Vcr::recordingProgress();
        }

        return json_encode(array(
            'success' => false,
        ));
    }

    static function generateThumbnails() {
        $filepath = urldecode($_GET['filepath']);
        $folder = dirname($filepath);

        $thumb_folder = $folder.'/screens';
        if(!file_exists($thumb_folder)) {
            mkdir($thumb_folder);
        }

        $cmd = 'ffmpeg -i "'.$filepath.'" -vf fps=1/60 "'.$thumb_folder.'/thumb%03d.jpg" >/dev/null 2>/dev/null &';
        $success = System::exec($cmd);

        if($success !== false) {
            $success = true;
        }

        echo json_encode(array(
            'cmd' => $cmd,
            'success' => $success,
        ));
    }

    static function isFilepathVideoStreamRunning($return_output = false) {
        $cmd = 'ps ax | grep ffmpeg | grep rtmp | grep -v "grep"';
        $output = shell_exec($cmd);

        if(empty($output)) {
            return false;
        }

        if($return_output) {
            return $output;
        }

        return true;
    }

    static function stopFilepathVideoStream() {
        $output = Media::isFilepathVideoStreamRunning(true);

        $pid = strtok($output, ' ');
        $retval = null;
        exec('kill '.$pid, $output, $retval);

        if($retval == 0) {
            return json_encode(array(
                'success' => true,
            ));
        }

        return json_encode(array(
            'success' => false,
        ));
    }

    function startFilepathVideoStream() {
        if(!isset($_GET['filepath'])) {
            echo json_encode(array(
                'success' => false,
                'error' => 'no filepath given',
            ));

            return false;
        }

        shell_exec('killall ffmpeg');
        shell_exec('rm -f /tmp/hls/*');

        $filepath = urldecode($_GET['filepath']);
        $rtmp_url = 'rtmp://localhost:1935/hls/'.$_GET['video_id'];
        $cmd = 'ffmpeg -nostdin -loglevel verbose -re -i "'.$filepath.'" -vf format=yuv420p -c:v libx264 -b:v 1500k -r 25 -s  1280x720 -profile:v baseline -shortest -c:a aac -ar 44100 -ac 2 -b:a 128k -movflags faststart -f flv '.$rtmp_url.' >/dev/null 2>/dev/null &';
        shell_exec($cmd);

        echo json_encode(array(
            'cmd' => $cmd,
            'success' => true,
        ));
    }

    /**
     * some characters need to be prepended with a backslash, aka escaped.
     */
    static function shellAddSlashes($filepath) {
        $filepath = str_replace(" ", "\ ", $filepath);
        $filepath = str_replace("'", "\'", $filepath);
        $filepath = str_replace('(', '\(', $filepath);
        $filepath = str_replace(')', '\)', $filepath);
        $filepath = str_replace('&', '\&', $filepath);
        // $filepath = str_replace('[', '\[', $filepath);
        // $filepath = str_replace(']', '\]', $filepath);

        return $filepath;
    }


    /**
     * insert string; $folder
     * return array; $folders_and_files
     * 
     * insert a folder and get back all files and directories with some 
     * extra intel about their properties.
     */
    function globFolder($folder = '/mnt/sda1/Downloads', $return_video = false) {
        if(!file_exists($folder)) {
            return false;
        }

        $files = scandir($folder);

        $media = [];
        foreach($files as $file) {
            if($file == '.' || $file == '..') {
                continue;
            }

            $filepath = $folder.'/'.$file;

            $is_video = false;
            $video_filepath = false;
            if(is_file($filepath)) {
                list($type, $ext) = explode('/', mime_content_type($filepath));
                
                if($type == 'video') {
                    $is_video = true;
                    $video_filepath = $filepath;
                }
            } else {
                if(!isset($_GET['find_video']) || (isset($_GET['find_video']) && $_GET['find_video'] == 1)) {
                    // $video_filepath = $this->globFolder($filepath, true);
                }
            }

            $m = array(
                'filepath' => $filepath,
                'filepath_url' => urlencode($filepath),
                'video_filepath_url' => $video_filepath,
                'filename' => $file,
                'filesize' => $this->formatBytes(filesize($filepath)),
                'is_file' => is_file($filepath),
                'is_video' => $is_video,
                'created' => filemtime($filepath),
            );

            if($return_video && $is_video) {
                return $filepath;
            }

            $media[] = $m;
        }

        if($return_video) {
            return false;
        }

        $media = $this->orderFolder($media);

        return array(
            'folder' => $folder,
            'parent_folder' => dirname($folder),
            'files' => $media,
        );
    }
    
    /**
     * insert array; $files
     * 
     * apply html _get filters for ordering the files and folders inside a folder.
     * want to order by date? order by filename?
     */
    function orderFolder($media) {
        if(isset($_GET['order'])) {
            $this->order = $_GET['order'];
        }

        if(isset($_GET['by'])) {
            $this->by = $_GET['by'];
        }

        if(isset($_GET['search'])) {
            $this->search = $_GET['search'];
        }

        if($this->order == 'created') {
            usort($media, function($a, $b) {
                if($this->by == 'ASC') {
                    return $a['created'] - $b['created'];
                }
                
                return $b['created'] - $a['created'];
            });
        }
        
        if($this->order == 'filename') {
            usort($media, function($a, $b) {
                if($this->by == 'ASC') {
                    return strcmp($a["filename"], $b["filename"]);
                }

                return strcmp($b["filename"], $a["filename"]);
            });
        }

        if(!empty($this->search)) {
            $found = [];
            foreach($media as $m) {
                if(strpos(strtolower($m['filename']), strtolower($this->search)) !== false) {
                    $found[] = $m;
                }
            }

            return $found;
        }

        return $media;
    }

    /**
     * input int; bytes
     * return string; 1mb/1gb/etc..
     */
    static function formatBytes($bytes, $precision = 2) { 
        $units = array('B', 'KB', 'MB', 'GB', 'TB'); 
    
        $bytes = max($bytes, 0); 
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
        $pow = min($pow, count($units) - 1); 
    
        // Uncomment one of the following alternatives
        $bytes /= pow(1024, $pow);
        // $bytes /= (1 << (10 * $pow)); 
    
        return round($bytes, $precision) . ' ' . $units[$pow]; 
    }
}