<?php

class Vcr extends Boot {
    /* status page */
    function statusPage() {
        $this->_GET();
        $smarty = $this->smarty();
        $smarty->assign('vcr_status', Vcr::status());
        
        $this->smartyDisplay($smarty, 'pages/media/vcr/status_page.tpl');
    }
    
    /* stream page */
    function streamPage() {
        $this->_GET();
        $smarty = $this->smarty();
        $smarty->assign('vcr_status', Vcr::status());
        Js::addSetting('vcr_status', Vcr::status());
        $this->smartyDisplay($smarty, 'pages/media/vcr/stream_page.tpl');
    }
    
    /* record page */
    function recordPage() {
        $this->_GET();
        $smarty = $this->smarty();
        Js::addSetting('vcr_status', Vcr::status());
        $smarty->assign('vcr_status', Vcr::status());
        $smarty->assign('record_status', $this->recordingProgress('array'));
        
        $this->smartyDisplay($smarty, 'pages/media/vcr/record_page.tpl');
    }

    function _GET() {
        if(isset($_GET['stop_stream']) && $_GET['stop_stream'] == 'vcr') {
            $stopped = $this->stopStream('bool');
            
            if($stopped) {
                Js::addMsg('De VCR stream is gestopt');
            } else {
                Js::addMsg('Het stoppen van de VCR stream is mislukt', 'danger');
            }
        }
    }

    /**
     * start stream
     */
    static function startStream() {
        $cmd = 'cd /home/friso/video_capture_scripts/stream && ./start_vcr_stream.sh';
        $output = shell_exec($cmd);

        echo json_encode(array(
            'output' => $output,
            'cmd' => $cmd,
            'success' => true,
        ));
    }

    /**
     * stop stream
     */
    static function stopStream($return_type = 'json') {
        $cmd = 'cd /home/friso/video_capture_scripts/stream && ./kill.sh';
        $output = shell_exec($cmd);

        $success = false;
        if(strpos($output, 'Killing pid') !== false) {
            $success = true;
        }

        if($return_type == 'bool') {
            return $success;
        }

        return json_encode(array(
            'output' => $output,
            'cmd' => $cmd,
            'success' => $success,
        ));
    }

    /**
     * start recording
     */
    static function startRecording() {
        if(Vcr::isRecording()) {
            return json_encode(array(
                'success' => false,
                'error' => 'FFMPEG is already recording',
            ));
        }

        $cmd  = 'cd /home/friso/video_capture_scripts/capture && ';
        $cmd .= './start.sh --duration '.$_GET['duration'].' --log yes --convert '.$_GET['convert'].' --infrared '.$_GET['infrared'].' > /dev/null 2>&1 &';
        $output = shell_exec($cmd);

        sleep(1);

        return json_encode(array(
            'cmd' => $cmd,
            'success' => Vcr::isRecording(),
        ));
    }

    /**
     * stop recording
     */
    static function stopRecording() {
        $cmd = 'cd /home/friso/video_capture_scripts/capture && ./kill.sh --all';
        $output = shell_exec($cmd);

        $success = false;
        if(strpos($output, 'Killing pid') !== false) {
            $success = true;
        }

        return json_encode(array(
            'cmd' => $cmd,
            'success' => $success,
        ));
    }

    /**
     * get record progress details from ffmpeg.log
     */
    static function recordingProgress($return_type = 'json') {
        // get details from logfile and ps ax
        $details = Vcr::getRecordingLogDetails();

        if($details == false) {
            if($return_type == 'json') {
                return json_encode(array(
                    'success' => false
                ));
            } else {
                return false;
            }
        }

        $length = Vcr::isRecordingGetLength();

        // calculate percentage of completion
        $length_sec = Vcr::getSeconds($length);
        $time_sec = Vcr::getSeconds($details['time']);
        
        if($length === false) {
            $percent = 100;
        } else {
            $percent = ($time_sec / $length_sec) * 100;
        }

        // filepath to recorded filepath
        $filepath = '/mnt/sda1/Capture/'.$details['filename'];

        $arr = array(
            'success' => true,
            'progress' => array(
                'percentage' => round($percent, 2),
                'details' => $details,
            ),
            'filepath' => $filepath,
            'length' => $length,
        );

        if($return_type == 'array') {
            return $arr;    
        }

        return json_encode($arr);
    }

    /**
     * input string; '00:00:15' HH:MM:SS
     * return int; $seconds
     * 
     * give back seconds
     */
    static function getSeconds($time = '00:00:15') {
        $parsed = date_parse($time);
        $seconds = $parsed['hour'] * 3600 + $parsed['minute'] * 60 + $parsed['second'];

        return $seconds;
    }

    static function getRecordingLogDetails() {
        $filepath = '/home/friso/video_capture_scripts/capture/ffmpeg.log';

        if(!file_exists($filepath)) {
            return false;
        }

        $log = file_get_contents($filepath);

        // find last frame= line in log
        $output_file_line = false;
        $last_line = false;
        $line = strtok($log, "\r\n");
        while ($line !== false) {
            $line = strtok("\r\n");
            
            if(substr($line, 0, 9) == 'Output #0') {
                $output_file_line = $line;
            }

            if(substr($line, 0, 6) == 'frame=') {
                $last_line = $line;
            }
        }

        // devide into parts
        $parts = explode('=', $last_line);

        $tmp = explode(' ', trim($parts[1]));
        $frames = $tmp[0];
        
        $tmp = explode(' ', trim($parts[4]));
        $size = str_replace('kB', '', $tmp[0]);
        $size = $size * 1024;
        $size = Media::formatBytes($size);
        
        $tmp = explode(' ', $parts[5]);
        $time = $tmp[0];

        preg_match("/'(.*?)':/", $output_file_line, $matches);
        $filename = str_replace('./files/', '', $matches[1]);

        $is_conversion = false;
        if(strpos($filename, 'converted.mkv') !== false) {
            $is_conversion = true;
        }

        return array(
            'frames' => $frames,
            'size' => $size,
            'time' => $time,
            'filename' => $filename,
            'is_conversion' => $is_conversion,
        );
    }

    static function status() {
        return array(
            'is_streaming' => Vcr::isStreaming(),
            'is_recording' => Vcr::isRecording(),
            'is_vlc_running' => Vlc::isRunning('/dev/video0'),
            'last_recorded' => Vcr::lastRecordedFile(),
        );
    }

    static function isStreaming() {
        $cmd = 'ps ax | grep ffmpeg | grep vcr | grep -v "grep"';
        $output = shell_exec($cmd);

        if(empty($output)) {
            return false;
        }

        return true;
    }

    static function isRecording() {
        $cmd = 'ps ax | grep ffmpeg | grep capture.mkv | grep -v "grep"';
        $output = shell_exec($cmd);

        if(empty($output)) {
            return false;
        }

        return true;
    }

    static function isRecordingGetLength() {
        $cmd = 'ps ax | grep "start.sh --duration" | grep -v "grep"';
        $output = shell_exec($cmd);

        if(empty($output)) {
            return false;
        }

        $regex = '/--duration (.*?) /';
        preg_match($regex, $output, $matches);
        
        return $matches[1];
    }

    static function lastRecordedFile() {
        $media = new Media();
        $folder = $media->globFolder('/mnt/sda1/Capture');

        if(!$folder) {
            return false;
        }

        $count = count($folder['files']);
        
        if($count == 0) {
            return false;
        }

        $last_index = $count - 1;
        return $folder['files'][$last_index];
    }
}