<?php

class Tv {
    function isPoweredOn() {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, Net::deviceGetIp('tv'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);

        $found = strpos($output, '<h1>404 - Not Found</h1>');
        
        if($found !== false) {
            return true;
        }

        return false;
    }
}