<?php

class Net extends Boot {
    /**
     * page /json
     */
    function jsonPage() {
        $auth = new Auth();

        // login not needed
        if(isset($_GET['is_awake'])) {
            return $this->isAwake();
        }

        // login needed
        if(!$auth->needsLogin()) {
            if(isset($_GET['reboot_device'])) {
                return $this->rebootDevice($_GET['reboot_device']);
            }

            if(isset($_GET['shutdown_device'])) {
                return $this->shutdownDevice($_GET['shutdown_device']);
            }

            if(isset($_GET['wake_device'])) {
                return $this->wakeDevice($_GET['wake_device']);
            }

            // these get router details, for now just fetch the json from router webserver
            if(isset($_GET['router'])) {
                return $this->getRouterInfo($_GET['router']);
            }
        }

        return json_encode(array(
            'success' => false,
        ));
    }

    /**
     * return json
     */
    function getRouterInfo($get) {
        return file_get_contents('http://10.0.0.1:808?'.$get);
    }

    /**
     * if were on http we need to goto https, but https can be on port 8443
     * 
     * !note, raspberry pi connection gets upgraded via nginx
     */
    function upgradeToHttps() {
        $https_ports = Net::getAllHttpsPorts();
        
        if(in_array($_SERVER['SERVER_PORT'], $https_ports)) {
            return false;
        }

        // if a route specifically does not want to be upgraded to https, then dont.
        $route = $this->getRoute();

        if(isset($route['httpsUpgrade']) && $route['httpsUpgrade'] == false) {
            return false;
        }

        // where do we need to goto?
        $device = Net::findWebServer($_SERVER['HTTP_HOST']);
        $redirect_url = Net::getHTTP($device, false, true);

        header('Location: '.$redirect_url);
    }

    static function getAllHttpsPorts() {
        $ports = array();
        $ports[] = '443';
        
        foreach(NETWORK_DEVICES as $device_name => $device) {
            if(isset($device['webserver']['https_port']))  {
                $ports[] = $device['webserver']['https_port'];
            }
        }

        return $ports;

    }

    /**
     * actually pings device to check if it's online
     * 
     * @note zwavepi needed sudo otherwise it would always say its unreachable...
     */
    static function ping($ip = false) {
        $output = shell_exec('sudo fping -t 1 '.$ip);

        if(strpos($output, 'is alive') !== false) {
            return true;
        }

        return false;
    }


    /**
     * return boolean;
     * 
     * check if webserver is hosted on $device_name from config constants.
     */
    static function isWebserverOn($device_name = false) {
        if($_SERVER['SERVER_ADDR'] == Net::deviceGetIp($device_name)) {
            return true;
        }

        return false;
    }

    /**
     * get ip from const array
     */
    static function deviceGetIp($device_name = false) {
        $device = NETWORK_DEVICES[$device_name];
        
        if(!isset($device['ip'])) {
            echo "ip for device name ".$device_name." is not set...";
            die();
        }

        return $device['ip'];
    }

    static function deviceGetHost($device_name = false) {
        $device = NETWORK_DEVICES[$device_name];
        return $device['host'];
    }

    /**
     * insert array, bool;
     * 
     * returns http<s>://<domain><:<port>>
     */
    static function getHTTP($device = false, $force_return_network_http = false, $force_https = null) {
        if($device == false) {
            return false;
        }

        // according to current connection, so keep https if its that way right now 
        $schema = 'http';
        if($_SERVER['SERVER_PORT'] == '443' || $_SERVER['SERVER_PORT'] == '8443') {
            $schema = 'https';
        }

        if($force_https === true) { $schema = 'https'; }
        if($force_https === false) { $schema = 'http'; }

        if(Net::isConnectedViaNetwork() || $force_return_network_http == true) {
            // via network
            return $schema.'://'.$device['host'];
        } else {
            // via internet
            $url = $schema.'://'.$device['webserver']['host'];

            if($schema == 'http' && isset($device['webserver']['port'])) {
                $url .= ':'.$device['webserver']['port'];
            }

            if($schema == 'https' && isset($device['webserver']['https_port'])) {
                $url .= ':'.$device['webserver']['https_port'];
            }

            return $url;
        }
    }

    /**
     * return string;
     * 
     * gives back other webserver url
     */
    static function otherWebServer($give_https = null) {
        $device = Net::findWebServer($_SERVER['HTTP_HOST'], true);
        
        if($device == false) {
            Js::addMsg('Dit domein is niet herkend in de config.', 'danger');
            return false;
        }

        return Net::getHTTP($device, false, $give_https);
    }

    /**
     * insert string;
     * 
     * pass defined name in config.php const NETWORK_DEVICES[name]
     */
    static function getDevice($net_device_name) {
        return NETWORK_DEVICES[$net_device_name];
    }

    /**
     * insert $give_primary (bool), $host_not (string/false)
     * 
     * if host_not string is given then given search string will be skipped. 
     */
    static function findWebServer($find_host = false, $skip_found = false) {
        // append default port if there is none
        if(strpos($find_host, ':') === false) {
            $find_host .= ':'.$_SERVER['SERVER_PORT'];
        }

        // store all webserver device names in array
        $device_names = [];
        foreach(NETWORK_DEVICES as $device_name => $device) {
            if(!isset($device['webserver']))  {
                continue;
            }

            $device_names[] = $device_name;
        }

        // now find the current webserver by hostname
        foreach(NETWORK_DEVICES as $device_name => $device) {
            if(!isset($device['webserver']))  {
                continue;
            }

            $netw_host = $device['host'];
            $inet_host = $device['webserver']['host'];
            $inet_port = isset($device['webserver']['port']) ? $device['webserver']['port']  : '80';
            $inet_https_port = isset($device['webserver']['https_port']) ? $device['webserver']['https_port'] : '443';

            if($skip_found == true &&
            (  $find_host == $netw_host.':80'
            || $find_host == $netw_host.':443'
            || $find_host == $inet_host.':'.$inet_port
            || $find_host == $inet_host.':'.$inet_https_port
            )) {
                $device_index = array_search($device_name, $device_names);
                unset($device_names[$device_index]);
                $next_name = reset($device_names);

                return NETWORK_DEVICES[$next_name];
            }
            
            if($skip_found == false &&
            (  $find_host == $netw_host.':80'
            || $find_host == $netw_host.':443'
            || $find_host == $inet_host.':'.$inet_port
            || $find_host == $inet_host.':'.$inet_https_port
            )) {
                return $device;
            }
        }

        return false;
    }

    static function voiceHTTP() {
        $device = Net::getDevice('voice_pi');
        return Net::getHTTP($device);
    }

    static function serverHost() {
        $device = Net::getDevice('server');
        return Net::getHTTP($device);
    }

    static function sameWebServerOnNetwork() {
        $device = Net::findWebServer($_SERVER['HTTP_HOST']);
        return Net::getHTTP($device, true);
    }

    /**
     * fetch json from other webserver to see if its on
     */
    static function isOtherWebServerAwake() {
        $http_domain = Net::otherWebServer(false);
        
        if($http_domain == false) {
            return false;
        }

        $url = $http_domain.'/net/json?is_awake';
        $txt = @file_get_contents($url);
        
        if($txt === false || empty($txt)) {
            return false;
        }

        $json = json_decode($txt);
        if(isset($json->success) && $json->success) {
            return true;
        }

        return false;
    }

    /**
     * used by javascript to check if this server is still alive
     */
    function isAwake() {
        header("Access-Control-Allow-Origin: *");

        return json_encode(array(
            'success' => true,
        ));
    }


    /**
     * reboot device op netwerk
     */
    function rebootDevice($device) {
        $output = false;

        if($device == 'zwave_pi') {
            $device = Net::getDevice('zwave_pi');
            $output = Net::sshExec($device['ip'], 'sudo reboot');
        } else if($device == 'voice_pi') {
            if(Net::isWebserverOn('voice_pi')) {
                $output = System::exec('sudo reboot');
            } else {
                $device = Net::getDevice('voice_pi');
                $output = Net::sshExec($device['ip'], 'sudo reboot');
            }
        } else if($device == 'server') {
            if(Net::isWebserverOn('server')) {
                $output = System::exec('sudo reboot');
            } else {
                $device = Net::getDevice('server');
                $output = Net::sshExec($device['ip'], 'sudo reboot');
            }
        } else if($device == 'laptop') {
            $device = Net::getDevice('laptop');
            $output = Net::sshExec($device['ip'], 'shutdown /r /t 3 /c "Home automation wilt rebooten"');
        }

        $success = true;
        if($output === false) {
            $success = false;
        }

        return json_encode(array(
            'success' => $success,
            'output' => $output,
        ));
    }

    /**
     * reboot device op netwerk
     */
    function shutdownDevice($device) {
        $output = false;

        if($device == 'server') {
            if(Net::isWebserverOn('voice_pi')) {
                $device = Net::getDevice('server');
                $output = Net::sshExec($device['ip'], 'sudo shutdown now');
            } else {
                $output = System::exec('sudo shutdown now');
            }
        } else if($device == 'laptop') {
            $device = Net::getDevice('laptop');
            $output = Net::sshExec($device['ip'], 'shutdown /s /t 3 /c "Home automation wilt deze computer afsluiten"');
        }

        $success = true;
        if($output === false) {
            $success = false;
        }

        return json_encode(array(
            'success' => $success,
            'output' => $output,
        ));
    }

    /**
     * wake on lan
     */
    function wakeDevice($device_name) {
        $device = Net::getDevice($device_name);

        if(!isset($device['mac'])) {
            return json_encode(array(
                'success' => false,
            ));
        }

        $cmd = 'wakeonlan '.$device['mac'];
        $output = System::exec($cmd);

        $success = false;
        if($output[0] == 'Sending magic packet to 255.255.255.255:9 with '.$device['mac']) {
            $success = true;
        }

        return json_encode(array(
            'cmd' => $cmd,
            'output' => $output,
            'success' => $success,
        ));
    }


    /**
     * execute commando on remote ssh server
     */
    static function sshExec($ip, $cmd) {
        $cmd = "ssh friso@".$ip." '".$cmd."'";
        return System::exec($cmd);
    }


    /**
     * return boolean;
     * 
     * is the website user connected via internet or network?
     * 
     * !alert
     * dont use const WEB_VIA_INTERNET in Auth because then you can fool
     * the constant by visiting via another domain then frisohorstman.nl 
     */
    // static function isOnNetwork() {
    static function isConnectedViaNetwork() {
        $remote_ip = $_SERVER['REMOTE_ADDR'];
        if(isset($_SERVER['HTTP_X_REAL_IP'])) {
            $remote_ip = $_SERVER['HTTP_X_REAL_IP'];
        }

        $ip = ip2long($remote_ip);
        $low_ip = ip2long('10.0.0.1');
        $high_ip = ip2long('10.0.0.255');

        if ($ip <= $high_ip && $low_ip <= $ip) {
            // ip is in range
            return true;
        }

        return false;
    }
}