<?php

class Voice extends Boot {
    function __construct() {
        $this->_GET();
    }

    /* notes page */
    function pageNotes() {
        $smarty = $this->smarty();
        
        $this->_POST();

        $smarty->assign('db_voice_notes', $this->getNotes());
        $this->smartyDisplay($smarty, 'pages/voice/notes/page.tpl');
    }

    /* commands page */
    function pageCommands() {
        $smarty = $this->smarty();
        $this->_POST();

        if(isset($_GET['search'])) {
            $commands = $this->searchCommands($_GET['search']);
        } else {
            $commands = $this->getCommands();
        }

        $smarty->assign('db_voice_commands', $commands);
        $this->smartyDisplay($smarty, 'pages/voice/commands/page.tpl');
    }
    
    /* log page */
    function pageLog() {
        $smarty = $this->smarty();
        
        if(Net::isWebserverOn('voice_pi')) {
            $log = $this->getLog();
        } else {
            $log = Json::get('Voice', 'getLog');
        }

        $smarty->assign('voice_log', $log);
        $this->smartyDisplay($smarty, 'pages/voice/log_page.tpl');
    }

    /* json page */
    function pageJSON() {
        if(isset($_GET['recorded_audio'])) {
            return $this->saveRecordedAudio();
        }

        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: *");

        if(isset($_GET['is_listening'])) {
            return json_encode(array(
                'success' => true,
                'is_listening' => $this->isListening(),
            ));
        }
        
        if(isset($_GET['start_voice'])) {
            if(Net::isWebserverOn('voice_pi')) {
                $started = $this->startPython();
            } else {
                $started = Json::get('Voice', 'startPython');
            }

            return json_encode(array(
                'success' => $started,
                'is_listening' => true,
            ));
        }
        
        if(isset($_GET['kill_voice'])) {
            if(Net::isWebserverOn('voice_pi')) {
                $stopped = $this->killPython();
            } else {
                $stopped = Json::get('Voice', 'killPython');
            }
            
            return json_encode(array(
                'success' => $stopped,
                'is_listening' => false,
            ));
        }

        return json_encode(array(
            'success' => false,
        ));
    }

    /**
     * when the browser records audio it gets posted here,
     * save the file and process the recognition part
     */
    function saveRecordedAudio() {
        // copy file to tmp dir..
        $oggfilepath = '/tmp/recognizedaudio.ogg';
        $wavfilepath = '/mnt/sdb1/www/recorded_audio/'.time().'.wav';
        move_uploaded_file($_FILES['recorded_audio']['tmp_name'], $oggfilepath); 

        // convert ogg file to wav
        $cmd = '/usr/bin/ffmpeg -i '.$oggfilepath.' '.$wavfilepath;
        $output = shell_exec($cmd);

        if(!file_exists($wavfilepath)) {
            // unlink($oggfilepath);

            return json_encode(array(
                'success' => false,
                'error' => 'audio conversion failed'
            ));
        }

        // recognize via python
        $cmd = 'python3 /home/friso/frisos_automation/python/voice_recognition.py -file '.$wavfilepath. ' 2>&1';
        $output = shell_exec($cmd);

        // remove wav file
        unlink($oggfilepath);
        // unlink($wavfilepath);
        
        // get interpeted speech
        $json = json_decode($output);

        $talk = false;
        $success = false;
        if(!empty($json)) {
            $success = true;
            $talk = $json->talk;
        }

        // response
        return json_encode(array(
            'success' => $success,
            'talk' => $talk,
            'output' => $output,
            'cmd' => $cmd,
        ));
    }

    /**
     * returns file contents of python voice recognizer
     */
    function getLog() {
        return file_get_contents('/tmp/voice.log');
    }

    // check if voice listener is running
    // !unused? is being ported to websocket 
    static function isListening() {
        if(Net::isWebserverOn('voice_pi')) {
            $output = shell_exec('ps ax | grep "frisos_automation/python/start.py" | grep -v "grep"');

            $running = false;
            if(!empty($output)) {
                $running = true;
            }
        } else {
            $running = Json::get('Voice', 'isListening');
        }

        return $running;
    }

    /**
     * start voice listener
     */
    function _GET() {
        if(isset($_GET['hide_in_dashboard'])) {
            $update = array(
                'hidden_in_dashboard' => '1',
            );

            $this->updateNote($_GET['note_id'], $update);
            header('Location: '.uri());
        }
    }

    /**
     * test for _POST parameters when form is submitted
     */
    function _POST() {
        if(empty($_POST) || !isset($_POST['form_name'])) {
            return;
        }
    
        if($_POST['form_name'] == 'voice_command') {
            if(isset($_POST['delete'])) {
                $this->deleteCommand($_POST['function_id']);
                return;
            } else {
                $this->saveCommand();
            }
        }

        if($_POST['form_name'] == 'voice_note') {
            if(isset($_POST['delete'])) {
                $this->deleteNote($_POST['note_id']);
                return;
            } else {
                $this->saveNote();
            }
        }
    }
    
    function killPython() {
        $output = shell_exec('ps ax | grep python3 | grep start.py | grep -v "sudo" | grep -v "grep"');

        $pid = strtok($output, ' ');
        $retval = null;
        $cmd = 'sudo kill '.$pid;
        exec($cmd, $output, $retval);

        if($retval == 0) {
            return true;
        }

        return false;
    }

    function startPython() {
        $cmd = 'sudo /home/friso/frisos_automation/start.sh > /dev/null &';
        $output = false;
        exec($cmd, $output, $return_var);

        if($return_var !== 0) {
            return false;
        }

        return true;
    }

    /**
     * insert int; $function_id
     * return array;
     */
    function getKeywords($function_id = false) {
        $db = new Mysql();
    
        $sql = "SELECT
                    *
                FROM
                    voice_keywords
                WHERE
                    function_id = ?";
    
        $query = $db->prepare($sql);
        $query->execute([$function_id]);
    
        return $query->fetchAll();
    }
    
    /**
     * return array;
     * 
     * combination of functions with keywords in one array
     */
    function getCommands() {
        $db = new Mysql();
    
        $sql = "SELECT
                    func.id,
                    func.module,
                    func.class,
                    func.function,
                    func.arg1,
                    func.arg2,
                    func.comment,
                    keyw.created,
                    (SELECT GROUP_CONCAT(keyw2.keyword) FROM voice_keywords keyw2 WHERE keyw2.function_id = func.id) AS keywords
                FROM
                    voice_functions func
                LEFT JOIN
                    voice_keywords keyw ON keyw.function_id = func.id
                ORDER BY
                    keyw.created
                DESC";
        
        // werkte niet in mysql 8
        // GROUP BY
        // func.id
    
        $query = $db->query($sql);
        return $query->fetchAll();
    }

    /**
     * return array;
     * 
     * combination of functions with keywords in one array
     */
    function searchCommands($search) {
        $db = new Mysql();

        $search = explode(' ', $search);
        $in  = str_repeat('?, ', count($search) - 1) . '?';

        $sql = 'SELECT
                    functions.id,
                    functions.comment,
                    functions.module,
                    functions.class,
                    functions.function,
                    functions.arg1,
                    functions.arg2,
                    keywords.created,
                    COUNT(keywords.function_id) AS occurrences,
                    (SELECT GROUP_CONCAT(keyw2.keyword) FROM voice_keywords keyw2 WHERE keyw2.function_id = functions.id) AS keywords,
                    (SELECT
                        GROUP_CONCAT(keyword)
                    FROM
                        voice_keywords
                    WHERE
                        voice_keywords.function_id = keywords.function_id
                    AND
                        voice_keywords.keyword IN ('.$in.')
                    GROUP BY
                        voice_keywords.function_id) AS matched_keywords
                FROM
                    voice_keywords keywords
                LEFT JOIN
                    voice_functions functions
                ON
                    functions.id = keywords.function_id
                WHERE
                    keywords.keyword IN ('.$in.')
                GROUP BY
                    keywords.function_id
                ORDER BY
                    occurrences
                DESC';
        
        $query = $db->prepare($sql);

        $query->execute(array_merge($search, $search));
        $results = $query->fetchAll();

        return $results;
    }

    /**
     * insert int; $function_id
     */
    function deleteCommand($function_id = false) {
        $db = new Mysql();

        // delete function
        $sql = "DELETE FROM voice_functions WHERE id = ?";

        $query = $db->prepare($sql);
        $query->execute([$function_id]);

        // delete keywords
        $sql = 'DELETE FROM voice_keywords WHERE function_id = ?';

        $query = $db->prepare($sql);
        $query->execute([$function_id]);
    }

    /**
     * save form is called, saves function in voice_functions table and
     * saves keywords in voice_keywords table.
     */
    function saveCommand() {
        $db = new Mysql();

        if($_POST['function_id'] != 'create') {
            $this->deleteCommand($_POST['function_id']);
        }

        // insert function
        $sql = 'INSERT INTO
                    voice_functions (id,module,class,function,arg1,arg2,comment)
                VALUES
                    (NULL, ?, ?, ?, ?, ?, ?)';

        $query = $db->prepare($sql);
        $query->execute([$_POST['module'], $_POST['class'], $_POST['function'], $_POST['arg1'], $_POST['arg2'], $_POST['comment']]);

        $new_function_id = $db->lastInsertId();

        // insert keywords
        $keywords = explode(',', $_POST['keywords']);

        foreach($keywords as $keyword) {
            $sql = 'INSERT INTO
                        voice_keywords (id,function_id,keyword,created)
                    VALUES
                        (NULL, ?, ?, ?)';

            $query = $db->prepare($sql);
            $query->execute([$new_function_id, $keyword, date('Y-m-d H:i:s')]);
        }
    }

    /**
     * insert array; $args (optional)
     * return array;
     * 
     * optionally you could add hardcoded arguments to alter the query.
     * add $args['hidden_in_dashboard'] = 1 and youll only get back the hidden ones
     */
    function getNotes($args = array()) {
        $db = new Mysql();
    
        $sql = 'SELECT
                    *
                FROM
                    voice_notes ';

        if(isset($args['hidden_in_dashboard'])) {
            $sql .= 'WHERE
                        hidden_in_dashboard = ? ';
        }

        $sql .= 'ORDER BY
                    created
                DESC ';

        if(isset($args['limit'])) {
            $sql .= 'LIMIT
                        0, ? ';
        }

        $query = $db->prepare($sql);
        $query->execute(array_values($args));
    
        return $query->fetchAll();
    }

    function saveNote() {
        if($_POST['note_id'] == 'create') {
            return $this->insertNote($_POST);
        } else {
            return $this->updateNote($_POST['note_id'], array(
                'note' => $_POST['note'],
                'hidden_in_dashboard' => (!isset($_POST['hidden_in_dashboard'])) ? 0 : 1
            ));
        }
    }

    function updateNote($note_id, $update) {
        $db = new Mysql();
       
        $sql = 'UPDATE
                    voice_notes
                SET ';

        foreach($update as $field => $value) {
            $sql .= $field.' = ?, ';
        }

        $sql .= 'updated = ? ';

        $sql .= 'WHERE
                    id = ?';


        $values = array_values($update);
        $values[] = date('Y-m-d H:i:s');
        $values[] = $note_id;

        $query = $db->prepare($sql);
        return $query->execute($values);
    }

    function insertNote() {
        $db = new Mysql();
       
        $sql = 'INSERT INTO
                    voice_notes (id,note,created)
                VALUES
                    (NULL, ?, ?)';

        $query = $db->prepare($sql);
        $query->execute([$_POST['note'], date('Y-m-d H:i:s')]);

        $new_note_id = $db->lastInsertId();

        return $new_note_id;
    }

    function deleteNote($note_id = false) {
        $db = new Mysql();

        $sql = "DELETE FROM voice_notes WHERE id = ?";

        $query = $db->prepare($sql);
        $query->execute([$note_id]);
    }
}