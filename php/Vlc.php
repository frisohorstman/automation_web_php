<?php

include_once('lib/telnet/Telnet.php');
use TelnetClient\TelnetClient;

class Vlc {
    /* json page */
    function jsonPage() {
        if(isset($_GET['vlc'])) {
            if($_GET['vlc'] == 'play_vcr') {
                return Vlc::playVCR();
            }

            if($_GET['vlc'] == 'stop_vcr') {
                return Vlc::stopVCR();
            }

            if($_GET['vlc'] == 'open_filepath') {
                return Vlc::openFile();
            }

            if($_GET['vlc'] == 'get_time') {
                return Vlc::getInfo();
            }

            if($_GET['vlc'] == 'seek') {
                return Vlc::seek();
            }

            if($_GET['vlc'] == 'stop') {
                $stop_any = false;
                if(isset($_GET['stop_any'])) {
                    $stop_any = true;
                }
                
                return Vlc::stop($stop_any);
            }
        }

        if(isset($_GET['keystroke'])) {
            return $this->execKeystroke();
        }

        return json_encode(array(
            'success' => false,
        ));
    }

    /**
     * simulate a keystroke from keyboard.
     * used for example to control vlc via shortcut keys like the letter f for full screen.
     */
    function execKeystroke() {
        $cmd = 'export DISPLAY=":0" && xdotool key '.$_GET['keystroke'];
        $output = shell_exec($cmd);

        return json_encode(array(
            'output' => $output,
            'cmd' => $cmd,
            'success' => true,
        ));
    }

    /**
     * input string; optional
     * return boolean
     * 
     * kills a vlc instance
     * !warning without giving in an additional $grep_search any vlc window will be killed
     */
    static function kill($grep_search = false) {
        $output = Vlc::isRunning($grep_search, true);
        
        if(empty($output)) {
            // vlc isnt running, nothing to do
            return false;
        }

        $pid = strtok($output[0], ' ');
        $cmd = 'kill '.$pid;
        $output = System::exec($cmd);

        if($output === false) {
            return false;
        }

        return true;
    }

    /**
     * input ...
     * return boolean/string; defaults to boolean
     * 
     * !warning without giving in an additional $grep_search any vlc window will be killed
     */
    static function isRunning($grep_search = false, $return_output = false) {
        if($grep_search != false) {
            $grep_search = '| grep '.$grep_search;
        }

        $cmd = 'ps ax | grep vlc '.$grep_search.' | grep -v "grep"';
        $output = System::exec($cmd);

        if(empty($output)) {
            return false;
        }

        if($return_output) {
            return $output;
        }

        return true;
    }

    /**
     * and start vlc stream server thats listens, for now viewable via laptop vlc.
     * !alert: has to go via ssh to gain proper access to screen and audio device for vlc to start.
     */
    static function playVCR() {
        if(Vlc::isRunning('/dev/video0')) {
            return json_encode(array(
                'success' => false,
            ));
        }

        $cmd = "cd /home/friso/video_capture_scripts && ./vlc_vcr.sh";
        $output = Net::sshExec('127.0.0.1', $cmd);

        $success = true;
        if($output === false) {
            $success = false;
        }

        return json_encode(array(
            'cmd' => $cmd,
            'output' => $output,
            'success' => $success,
        ));
    }

    /**
     * return string; json
     * 
     * kills vlc window with VCR stream 
     */
    static function stopVCR() {
        return json_encode(array(
            'success' => Vlc::kill('/dev/video0'),
        ));
    }

    /**
     * return json;
     * 
     * opens _GET[filepath] in vlc.
     */
    static function openFile() {
        $filepath = urldecode($_GET['filepath']);

        try {
            return Vlc::openFileTelnet($filepath);
        } catch (\Exception $e) {
            return Vlc::openFileShell($filepath);
        }
    }

    /**
     * opens a file in vlc via shell command.
     */
    static function openFileShell($filepath) {
        // $filepath = Media::shellAddSlashes($filepath);
        $filepath = addslashes($filepath);
        Vlc::stop();
        
        // $cmd = 'python3 /home/friso/frisos_automation/remote.py -vlc '.$filepath;
        $cmd = 'export DISPLAY=":0" && vlc "'.$filepath.'" --fullscreen --extraintf telnet --telnet-password bierentieten > /dev/null 2>&1 &';
        $output = Net::sshExec('127.0.0.1', $cmd);

        $success = false;
        if($output !== false) {
            $success = true;
        }

        return json_encode(array(
            'output' => $output,
            'cmd' => $cmd,
            'success' => $success,
        ));
    }

    /**
     * opens a file in vlc via telnet directly to vlc
     * (but vlc might not be running with telnet...)
     */
    static function openFileTelnet($filepath) {
        $vlc_cmd = 'add '.$filepath;
        $output = Vlc::telnetExec($vlc_cmd);

        return json_encode(array(
            'output' => $output,
            'vlc_cmd' => $vlc_cmd,
            'success' => true,
        ));
    }

    /**
     * return json;
     * 
     * stops VLC thats opened via browser, either via telnet or cli kill.
     * only tries to kill vlc thats running with telnet unless $stop_any = true;
     */
    static function stop($stop_any = false) {
        $stopped = Vlc::stopTelnet();

        if($stopped) {
            return json_encode(array(
                'success' => true
            ));
        } else {
            if($stop_any == true) {
                // beware, any vlc opened will be killed
                $killed = Vlc::kill();
            } else {
                $killed = Vlc::kill('telnet');
            }

            if($killed) {
                return json_encode(array(
                    'success' => true
                ));
            }
        }

        return json_encode(array(
            'success' => false
        ));
    }
    
    /**
     * return json;
     * 
     * shuts down vlc thats opened via browser
     */
    static function stopTelnet() {
        try {
            Vlc::telnetExec('shutdown');
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * return json;
     * 
     * gets info via telnet about how far along a videofile has been played
     */    
    static function getInfo() {
        try {
            $info = Vlc::telnetGetInfo();
            $info['success'] = true;
        } catch (\Exception $e) {
            $info = ['success' => false];
        }

        return json_encode($info);
    }

    /**
     * return json;
     * 
     * seeks vlc to _GET[time]
     */    
    static function seek() {
        try {
            $cmd = 'seek '.$_GET['time'];
            $info = Vlc::telnetExec($cmd);
            $info['success'] = true;
        } catch (\Exception $e) {
            $info = ['success' => false];
        }

        return json_encode($info);
    }


    /**
     * return object;
     * 
     * connect via telnet to running VLC instance.
     * dont forget to ->disconnect() when finished using this telnet session..
     * !sneak but disconnect is probably not needed... its prob killed when php sess ends
     */
    static function telnetConnect() {
        $telnet = new TelnetClient('127.0.0.1', 4212);
        $telnet->connect();
        $telnet->setPrompt('Password:');
        $telnet->exec('bierentieten');
        $telnet->setPrompt('>');

        return $telnet;
    }

    /**
     * return array; $video_info
     * 
     * returns video time, length and if its playing.
     */
    static function telnetGetInfo() {
        $telnet = Vlc::telnetConnect();

        $video_title = $telnet->exec('get_title');
        $video_time = $telnet->exec('get_time');
        $video_length = $telnet->exec('get_length');
        $video_status = $telnet->exec('status');
        $telnet->disconnect();
        
        $is_playing = false;
        if($video_status[2] == '( state playing )') {
            $is_playing = true;
        }

        preg_match('/\( new input: file:\/\/(.*) \)/', $video_status[0], $matches);

        return array(
            'video_filepath' => $matches[1],
            'video_time' => (int) filter_var($video_time[0], FILTER_SANITIZE_NUMBER_INT),
            'video_length' => (int) filter_var($video_length[0], FILTER_SANITIZE_NUMBER_INT),
            'video_is_playing' => $is_playing,
        );
    }

    /**
     * input string; $vlc_cmd
     * return string; $cmd_output
     */
    static function telnetExec($cmd) {
        $telnet = Vlc::telnetConnect();

        $output = $telnet->exec($cmd);
        $telnet->disconnect();
        
        return array(
            'output' => $output[0]
        );
    }
}