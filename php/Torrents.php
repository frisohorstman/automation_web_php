<?php

class Torrents extends Boot {
    /* json page */
    function jsonPage() {
        $transmission = new Transmission();

        if(isset($_GET['getMagnet'])) {
            return $this->getMagnet($_GET['getMagnet']);
        }

        if(isset($_GET['addMagnet'])) {
            return $this->addMagnet($_GET['addMagnet']);
        }

        if(isset($_GET['start'])) {
            return $transmission->start($_GET['start']);
        }

        if(isset($_GET['stop'])) {
            return $transmission->stop($_GET['stop']);
        }

        if(isset($_GET['remove'])) {
            $delete_data = false;
            if(isset($_GET['delete_data']) && $_GET['delete_data'] == 'true') {
                $delete_data = true;
            }

            return $transmission->remove($_GET['remove'], $delete_data);
        }

        if(isset($_GET['set_priority'])) {
            return $transmission->setPriority($_GET['id'], $_GET['file_id'], $_GET['set_priority']);
        }

        if(isset($_GET['downloads_overview'])) {
            return $transmission->getDownloads(false);
        }

        return json_encode(array(
            'success' => false,
            'error' => 'unhandled json request',
        ));
    }

    /* search page */
    function searchPage($display_page = true) {
        $smarty = $this->smarty();

        $torrent_search = false;
        $torrents = [];
        $pagination = [];
        if(isset($_GET['search']) || isset($_GET['search_url'])) {
            if(isset($_GET['search'])) {
                $torrent_search = $_GET['search'];
            }

            $search_url = false;
            if(isset($_GET['search_url'])) {
                $search_url = $_GET['search_url'];
            }

            $site = new Torrent1337();
            $page = $site->search($torrent_search, $search_url);

            if(!empty($page)) {
                extract($page);
            }
        }

        $smarty->assign('torrent_search', $torrent_search);
        $smarty->assign('torrents', $torrents);
        $smarty->assign('pagination', $pagination);
        
        if($display_page) {
            $this->smartyDisplay($smarty, 'pages/torrent/search/page.tpl');
        } else {
            $this->smartyDisplay($smarty, 'pages/torrent/search/table.inc.tpl', 'empty.tpl');
        }
    }

    function searchHtmlTable() {
        $this->searchPage(false);
    }

    /* detail page */
    function detailPage($display_page = true) {
        $smarty = $this->smarty();

        // add torrent id to javascript vars
        Js::addSetting('torrent_id', $_GET['id']);

        $t = new Transmission();
        $info = $t->getDetails($_GET['id']);

        if(empty($info) || empty($info->arguments->torrents)) {
            echo "redirect here";
            die();
            header('Location: /torrent/downloads');
        }

        $details = $info->arguments->torrents[0];

        $files = array();
        foreach($details->files as $i => $file) {
            $percentDone = ($details->fileStats[$i]->bytesCompleted / $file->length) * 100;
            $file->percentDone = round($percentDone, 2);
            $file->sizeCompletedHuman = Media::formatBytes($details->fileStats[$i]->bytesCompleted);
            $file->sizeWhenDoneHuman = Media::formatBytes($file->length);
            $file->priority = $details->fileStats[$i]->priority;
            $file->name = str_replace($details->name.'/', '', $file->name);

            $files[] = $file;
        }
        $details->files = $files;
        
        $smarty->assign('details', $details);

        if($display_page) {
            $this->smartyDisplay($smarty, 'pages/torrent/details/page.tpl');
        } else {
            $this->smartyDisplay($smarty, 'pages/torrent/details/table.inc.tpl', 'empty.tpl');
        }
    }
    
    function detailHtmlTable() {
        $this->detailPage(false);
    }

    /* downloads page */
    function downloadsPage($display_page = true) {
        $smarty = $this->smarty();
        $this->downloadsPage_POST();

        $t = new Transmission();
        $info = $t->getDownloads();

        if(empty($info)) {
            $smarty->assign('torrent_error', 'Error contacting Transmission torrent application.<br>Staat die aan?');
            $this->smartyDisplay($smarty, 'pages/torrent/downloads/page.tpl');
            return false;
        }
        
        // wat beter leesbare stuff toevoegen
        $downloads = [];
        foreach($info->arguments->torrents as $download) {
            $bytesDownload = $download->sizeWhenDone - $download->leftUntilDone;
            
            $download->rateDownloadHuman = Media::formatBytes($download->rateDownload);
            $download->rateUploadHuman = Media::formatBytes($download->rateUpload);
            $download->sizeDownloadedHuman = Media::formatBytes($bytesDownload);
            $download->sizeWhenDoneHuman = Media::formatBytes($download->sizeWhenDone);
            $download->percentDone = $download->percentDone * 100;
            $download->etaHuman = $this->etaHuman($download->eta);
            $downloads[] = $download;
        }

        $smarty->assign('downloads', $downloads);

        if($display_page) {
            $this->smartyDisplay($smarty, 'pages/torrent/downloads/page.tpl');
        } else {
            $this->smartyDisplay($smarty, 'pages/torrent/downloads/table.inc.tpl', 'empty.tpl');
        }
    }

    function downloadsHtmlTable() {
        $this->downloadsPage(false);
    }

    function downloadsPage_POST() {
        if(empty($_POST)) {
            return false;
        }

        if(isset($_POST['form_name']) && $_POST['form_name'] == 'add_torrent') {
            $this->downloadsPage_addTorrent_POST();
        }

        if(isset($_POST['mass_action'])) {
            $this->downloadsPage_massAction_POST();
        }
    }

    function downloadsPage_massAction_POST() {
        $remove_data = false;
        if($_POST['mass_action'] == 'delete_remove_data') {
            $remove_data = true;
        }

        $t = new Transmission();
        $t->remove(
            array_values($_POST['torrent_ids']),
            $remove_data
        );
        
        Js::addMsg('De geselecteerde torrents zijn verwijderd uit de lijst.');
    }

    function downloadsPage_addTorrent_POST() {
        $content = file_get_contents($_FILES['torrent']['tmp_name']);
        $b64_content = base64_encode($content);

        $t = new Transmission();
        $txt = $t->addTorrent($b64_content);
        $json = json_decode($txt);

        $js = new JS();
        if($json->result == 'success') {
            Js::addMsg('De torrent is toegevoegd');
            return true;
        }
        
        Js::addMsg('Het toevoegen van de torrent is mislukt', 'danger');
    }

    function getMagnet($url) {
        $site = new Torrent1337();
        $magnet = $site->getMagnetUrl($url);
        return json_encode(['magnet' => $magnet]);
    }

    function addMagnet($magnet) {
        $t = new Transmission();
        $json = $t->addMagnet($magnet);

        if(empty($json)) {
            $json = json_encode(['result' => 'fail']);
        }

        return $json;
    }

    function etaHuman($eta) {
        $etaHuman  = '';
        
        if($eta < 0) {
            $etaHuman = 'onbekend';
        }

        if($eta > 0) {
            $dtF = new \DateTime('@0');
            $dtT = new \DateTime("@$eta");
            $days = $dtF->diff($dtT)->format('%a');
            $hour = $dtF->diff($dtT)->format('%h');
            $min = $dtF->diff($dtT)->format('%i');
            $sec = $dtF->diff($dtT)->format('%s');

            if($days >= 1) {
                if($days == 1) {
                    $etaHuman .= '1 dag';
                } else {
                    $etaHuman .= $days.' dagen';
                }
            } else {
                if($hour > 0) {
                    $etaHuman .= $hour.' uur';
                }

                if($hour > 0 && $min > 0) {
                    $etaHuman .= ' en ';
                }

                if($min > 1) {
                    $etaHuman .= $min.' minuten';
                }
                
                if($min == 1) {
                    $etaHuman .= $sec.' seconden';
                }
            }
        }

        return $etaHuman;
    }
}

// torrent website 1337x.to
class Torrent1337 {
    private $domain = 'https://1337x.to';

    function search($searchPhrase = false, $url = false) {
        if($url == false) {
            if($searchPhrase == false) {
                return false;
            }
            
            $url = $this->domain.'/search/'.$searchPhrase.'/1/';
        }
        
        // echo $url;die();
        $html = file_get_contents($url);

        libxml_use_internal_errors(true);
        $dom = new DOMDocument();
        $dom->loadHTML($html);

        // torrents
        $xpath = new DOMXPath($dom);
        $trs = $xpath->query('//tbody/tr');
        
        $searchItems = array();
        foreach ($trs as $tr) {
            $title = $tr->getElementsByTagName('td')->item(0);
            $a = $title->getElementsByTagName('a')->item(1);
            $href = $this->domain.$a->getAttribute('href');
            
            $seeders = $tr->getElementsByTagName('td')->item(1);
            $leechers = $tr->getElementsByTagName('td')->item(2);
            $date = $tr->getElementsByTagName('td')->item(3);
            
            $size = $tr->getElementsByTagName('td')->item(4);
            $span = $size->getElementsByTagName('span')->item(0);
            $size->removeChild($span);
            
            $item = [
                'title' => $title->nodeValue,
                'seeders' => $seeders->nodeValue,
                'leechers' => $leechers->nodeValue,
                'date' => $date->nodeValue,
                'size' => $size->nodeValue,
                'href' => $href,
            ];
            
            $searchItems[] = $item;
        }
        
        // pagination
        $xpath = new DOMXPath($dom);
        $lis = $xpath->query("//div[@class='pagination']/ul/li");

        $pagination = [];
        foreach ($lis as $li) {
            $a = $li->getElementsByTagName('a')->item(0);
            $href = $this->domain.$a->getAttribute('href');

            $pagination[] = array(
                'url' => $href,
                'pageNumber' => $a->nodeValue,
            );
        }

        return array(
            'torrents' => $searchItems,
            'pagination' => $pagination,
        );
    }

    function getMagnetUrl($url = false) {
        if($url == false) {
            return false;
        }

        $html = file_get_contents($url);
        $pattern = '/href\s*\=\s*"(magnet[^"]+)"/';
        preg_match($pattern, $html, $matches);

        return $matches[1];
    }
}

