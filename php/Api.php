<?php

class Api {
    /**
     * page /api/1.0/cpu/info
     */
    function cpuInfo() {
        return json_encode(array(
            'success' => true,
            'info' => System::cpuInfo()
        ));
    }
}