<?php

class Transmission {
    function getSessionId() {
        $url = 'http://'.TRANSMISSION_HTTP['host'].'/transmission/rpc';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_PORT, TRANSMISSION_HTTP['port']);
        curl_setopt($ch, CURLOPT_USERPWD, TRANSMISSION_HTTP['user'].':'.TRANSMISSION_HTTP['pass']);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        $body = curl_exec($ch);
        curl_close($ch);

        // loop trough header for session id.
        $separator = "\r\n";
        $line = strtok($body, $separator);
        
        while ($line !== false) {
            $line = strtok($separator);

            if(substr($line, 0, 27) == 'X-Transmission-Session-Id: ') {
                // return session id
                return substr($line, 27);
            }
        }

        return false;
    }

    function getJSON($args) {
        $url = 'http://'.TRANSMISSION_HTTP['host'].'/transmission/rpc';
        $post = json_encode($args, JSON_NUMERIC_CHECK);

        // curl
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_PORT, TRANSMISSION_HTTP['port']);
        curl_setopt($ch, CURLOPT_USERPWD, TRANSMISSION_HTTP['user'].':'.TRANSMISSION_HTTP['pass']);

        $headers = array();
        $headers[] = 'X-Transmission-Session-Id: '.$this->getSessionId();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        $body = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);
        
        if($httpcode == 409) {
            echo "auth failed";
            die();
        }

        // is json
        return $body;
    }

    function addMagnet($magnet = false) {
        $args = array(
            'method' => 'torrent-add',
            'arguments' => [
                'filename' => $magnet
            ]
        );

        return $this->getJSON($args);
    }

    function addTorrent($base64_torrent_file = false) {
        $args = array(
            'method' => 'torrent-add',
            'arguments' => [
                'metainfo' => $base64_torrent_file
            ]
        );

        return $this->getJSON($args);
    }

    function getDownloads($decode_json = true) {
        $args = array(
            'method' => 'torrent-get',
            'arguments' => [
                'fields' => ['id', 'name', 'status', 'percentDone', 'rateDownload', 'rateUpload', 'leftUntilDone', 'sizeWhenDone', 'eta'],
            ]
        );

        $json = $this->getJSON($args);

        if($decode_json == false) {
            return $json;
        }

        return json_decode($json);
    }

    function getDetails($torrent_id = false) {
        $args = array(
            'method' => 'torrent-get',
            'arguments' => [
                'fields' => ['id', 'name', 'activityDate', 'corruptEver', 'desiredAvailable', 'downloadedEver', 'fileStats',
                'haveUnchecked', 'haveValid', 'peers', 'startDate', 'trackerStats', 'comment', 'creator',
                'dateCreated', 'files', 'hashString', 'isPrivate', 'pieceCount', 'pieceSize'],
                'ids' => [$torrent_id]
            ],
        );

        $json = $this->getJSON($args);
        return json_decode($json);
    }

    function remove($torrent_ids = [], $delete_data = false) {
        if(is_int($torrent_ids)) {
            // 1 numeric id was given instead of an array with ids
            $torrent_ids = array($torrent_ids);
        }

        $args = array(
            'method' => 'torrent-remove',
            'arguments' => [
                'ids' => $torrent_ids,
            ],
        );
        
        
        if($delete_data == true) {
            $args['arguments']['delete-local-data'] = true;
        }

        return $this->getJSON($args);
    }

    function stop($torrent_id = false) {
        $args = array(
            'method' => 'torrent-stop',
            'arguments' => [
                'ids' => [$torrent_id]
            ]
        );

        return $this->getJSON($args);
    }

    function start($torrent_id = false) {
        $args = array(
            'method' => 'torrent-start',
            'arguments' => [
                'ids' => [$torrent_id]
            ]
        );

        return $this->getJSON($args);
    }

    function setPriority($torrent_id = false, $file_id = false, $priority = 'high') {
        $args = array(
            'method' => 'torrent-set',
            'arguments' => [
                'ids' => [$torrent_id],
                'priority-'.$priority => [$file_id]
            ]
        );

        return $this->getJSON($args);
    }
}

