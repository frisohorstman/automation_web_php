<?php

class Infrared extends Boot {
    function pageRemoteTV() {
        $smarty = $this->smarty();
        $this->smartyDisplay($smarty, 'pages/infrared_remotes/tv_remote.tpl');
    }

    function pageRemoteSurroundSet() {
        $smarty = $this->smarty();
        $this->smartyDisplay($smarty, 'pages/infrared_remotes/surround_set_remote.tpl');
    }
    
    function pageRemotesOverview() {
        $smarty = $this->smarty();

        if(Net::isWebserverOn('voice_pi')) {
            $remotes = $this->getRemotes();
        } else {
            $remotes = Json::get('Infrared', 'getRemotes');
        }

        if(empty($remotes)) {
            // raspberry pi is offline
            echo "remotes is leeg";die();
        }

        // remotes page
        $smarty->assign('remotes', $remotes);
        $this->smartyDisplay($smarty, 'pages/infrared_remotes/page.tpl');
    }

    function pageMouse() {
        $smarty = $this->smarty();
        $this->smartyDisplay($smarty, 'pages/infrared_remotes/mouse_page.tpl');
    }

    /**
     * return array;
     *
     * parse all configs in lirc its config folder.
     */
    function getRemotes() {
        $lirc_conf_folder = '/etc/lirc/lircd.conf.d/';
        $files = scandir($lirc_conf_folder);
        $remotes = array();

        foreach($files as $file) {
            if(substr($file, -10) != 'lircd.conf') {
                continue;
            }

            $remote = $this->parseConfig($lirc_conf_folder.$file);
            list($name, $keys) = $remote;

            $remotes[] = array(
                'name' => $name,
                'keys' => $keys,
            );
        }

        return $remotes;
    }

    /**
     * input string x2; $remote, $key/btn
     *
     * call irsend directly instead of python script, since it is so much faster.
     * transmits infrared key.
     */
    function send($remote = false, $key = false) {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: *");
        
        if(isset($_GET['remote'])) {
            $remote = $_GET['remote'];
        }

        if(isset($_GET['key'])) {
            $key = $_GET['key'];
        }

        if(Net::isWebserverOn('voice_pi')) {
            // via python is traag..
            // $cmd = 'python3 /home/friso/frisos_automation/python/infrared.py -send '.$remote.' '.$key;
            $cmd = 'irsend -a localhost:2220 SEND_ONCE '.$remote.' '.$key;
            $output = shell_exec($cmd);

            $json = array(
                'cmd' => $cmd,
                'shell_output' => $output,
                'send_remote' => true,
            );
        } else {
            $json = Json::get('Infrared', 'send', [$remote, $key]);
        }

        if(isset($_GET['redirect'])) {
            header('Location: '.$_GET['redirect']);
        } else {
            return json_encode($json);
        }
    }

    /**
     * we recieve an infrared keypress, pipe trough to remote.py
     * that has actions bound to the $_GET['remote'] key.
     */
    function recieve() {
        if(!isset($_GET['key'])) {
            return json_encode(array(
                'success' => false
            ));
        }

        $cmd = 'python3 /home/friso/frisos_automation/remote.py -key '.$_GET['key'];
        $output = Net::sshExec('127.0.0.1', $cmd);

        return json_encode(array(
            'success' => true,
            'info' => 'Executed remote command...',
            'cmd' => $cmd,
            'output' => $output,
        ));
    }

    function parseConfig($filepath = false) {
        $conf = file_get_contents($filepath);

        // iterate over each line
        $separator = "\r\n";
        $line = strtok($conf, $separator);
        $reached_codes = false;
        $remote_name = false;
        $remote_keys = array();

        while ($line !== false) {
            $line = strtok($separator);

            // get the remote name from the config file
            if(substr(trim($line), 0, 4) == 'name') {
                $remote_name = substr(trim($line), 6);
            }

            if(trim($line) == 'begin codes') {
                $reached_codes = true;
                continue;
            }

            if(trim($line) == 'end codes') {
                $reached_codes = false;
                continue;
            }

            if($reached_codes == false) {
                continue;
            }

            // these are the remote keys, put them in an array
            list($key, $code) = preg_split('/\s+/', trim($line));
            $remote_keys[$key] = $code;
        }

        return array($remote_name, $remote_keys);
    }
}
