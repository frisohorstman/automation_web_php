<?php

// when youre not connecting via the network but via the internet
// a login is required.

class Auth extends Boot {
    /* login page */
    function loginPage() {
        if($this->isLoginValid() === false) {
            Js::addMsg('De login gegevens zijn incorrect.', 'danger');
        }
        
        $redirect_url = Auth::getLoggedInUserUri();
        
        if(Net::isOtherWebServerAwake()) {
            // login onto other webserver aswell
            $_REQUEST['redirect_to_other_webserver'] = true;
            $query = http_build_query($_REQUEST);
            $redirect_url = Net::otherWebServer().'/login?'.$query;
        }
        
        if(isset($_GET['redirect_to_other_webserver'])) {
            $redirect_url = Net::otherWebServer();
        }

        if(!$this->needsLogin()) {
            // no login needed (anymore) or submitted login is valid
            header('Location: '.$redirect_url);
        }

        
        // assign smarty & variables
        $smarty = $this->smarty();

        $smarty->assign('same_webserver_on_network', Net::sameWebServerOnNetwork());
        $smarty->assign('other_webserver', Net::otherWebServer());
        $smarty->assign('js_settings', Js::getSettings());

        $this->smartyDisplay($smarty, false, 'pages/login_page.tpl');
    }

    /* logout page */
    function logoutPage() {
        $this->logout();

        // goto login page after logout
        $redirect_url = '/login';

        // goto other webserver to logout there aswell
        if(Net::isOtherWebServerAwake() && !isset($_GET['redirect_to_other_webserver'])) {
            $redirect_url = Net::otherWebServer().'/logout?redirect_to_other_webserver';
        }

        // redirected to here by another webserver to logout, now go back.
        if(isset($_GET['redirect_to_other_webserver'])) {
            $redirect_url = Net::otherWebServer();
        }

        // echo "redirect url: ".$redirect_url;
        header('Location: '.$redirect_url);
    }

    /**
     * json page
     * 
     * this website hosted on the server and raspberry pi, so when you login either on of the hosting
     * parties will try to login via json on the other server at the same time.
     */
    function jsonPage() {        
        if($_REQUEST['action'] == 'login') {
            return json_encode(array(
                // 'is_other_webserver_awake' => Net::isOtherWebServerAwake(),
                'success' => $this->isLoginValid()
            ));
        }

        return json_encode(array(
            'success' => false
        ));
    }

    function logout() {
        $this->startSession();

        unset($_SESSION["logged_in"]);
        unset($_SESSION["timeout"]);
        unset($_SESSION["username"]);

        return true;
    }

    function startSession() {
        if(!isset($_SESSION)) {
            session_start();
        }
    }

    /**
     * checks if the _POST username and password are rightfull credentials
     */
    function isLoginValid() {
        if(!isset($_REQUEST['username']) || !isset($_REQUEST['password'])) {
            return null;
        }

        if($this->verifyUserLogin() == false) {
            return false;
        }


        $this->startSession();
        $_SESSION['logged_in'] = true;
        $_SESSION['timeout'] = time();
        $_SESSION['username'] = $_REQUEST['username'];

        return true;
    }

    /**
     * loop trough users and test if the login details match.
     */
    function verifyUserLogin() {
        foreach(AUTH as $user) {
            if(($_REQUEST['username'] == $user['user']) && ($_REQUEST['password'] == $user['pass'])) {
                return true;
            }
        }

        return false;
    }

    /**
     * return boolean
     * 
     * checks whether connection comes via internet or network
     */
    function needsLogin() {
        // connections via network dont have to login
        if(Net::isConnectedViaNetwork()) {
            return false;
        }

        // were connected via internet, is the user logged in?
        $auth = new Auth();

        if($auth->isLoggedIn() && $auth->routeIsAccessAllowed()) {
            // echo "hier geeft ie false..\n";
            // debug_print_backtrace();
            // echo "\n\n";
            return false;
        }
        // if($auth->isLoggedIn()) {
        //     if($auth->routeIsAccessAllowed() == false) {
        //         echo "route: ".uri()." is not allowed";die();
        //     }
        //     return false;
        // }
        
        // login is needed
        return true;
    }

    function routeIsAccessAllowed() {
        // $route = $this->getRoute();
        $user = Auth::getLoggedInUser();

        // access always has to be set
        if(!isset($user['access'])) {
            return false;
        }

        if($user['access'] == 'root') {
            return true;
        }

        if(is_array($user['access']) && in_array(uri(), Auth::getLoggedInUserAllUri())) {
            return true;
        }

        return false;
    }

    /**
     * return boolean;
     */
    static function isLoggedIn() {
        $auth = new Auth();
        $auth->startSession();

        if(isset($_SESSION['logged_in']) && $_SESSION['logged_in'] == true) {
            return true;
        }

        return false;
    }

    static function getLoggedInUser() {
        $auth = new Auth();
        $auth->startSession();

        if(!isset($_SESSION['username'])) {
            return false;
        }

        foreach(AUTH as $user) {
            if($user['user'] == $_SESSION['username']) {
                return $user;
            }
        }

        return false;
    }

    static function getLoggedInUserAllUri() {
        $user = Auth::getLoggedInUser();

        if($user == false) {
            return false;
        }

        if(is_array($user['access'])) {
            $routes = $user['access'];
            $routes[] = '/login';
            $routes[] = '/logout';
            $routes[] = '/merged.js';

            return $routes;
        }

        return false;
    }

    static function getLoggedInUserUri() {
        $uris = Auth::getLoggedInUserAllUri();

        if(!is_array($uris) || count($uris) == 0) {
            return '/';
        }

        return $uris[0];
    }
}