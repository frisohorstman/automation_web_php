<?php

$mysql_conn = false;

class Mysql extends Boot{
     public function __construct() {
          global $mysql_conn;
          
          if($mysql_conn != false) {
               // var_dump($mysql_conn);die();
               return $mysql_conn;
          }
          
          $mysql_conn = $this->connect();
          return $mysql_conn;
     }

     public function connect() {          
          $options = [
               PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
               PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
               PDO::ATTR_EMULATE_PREPARES   => false,
          ];

          try {
               // when database is hosted locally on voice pi
               // $db_host = Net::deviceGetHost('zwave_pi');
               // if(Net::isWebserverOn('zwave_pi')) {
                    $db_host = 'localhost';
               // }

               // database is hosted on external mysql server
               if(isset(MYSQL['host'])) {
                    $db_host = MYSQL['host'];
               }

               return new PDO("mysql:host=".$db_host.";dbname=".MYSQL['database'].";charset=".MYSQL['charset'], MYSQL['user'], MYSQL['pass'], $options);
          } catch (\PDOException $e) {
               // return $e->getCode();
               // no route to host (voice pi is offline)
               // if($e->getCode() == 2002) {
               //      echo 'whos da boss?';
               // }
               // die();
               // throw new \PDOException($e->getMessage(), (int)$e->getCode());

               $smarty = $this->smarty();
               $smarty->assign('title', 'Database unreachable');
               $smarty->assign('error_message', $e->getMessage());
               $this->smartyDisplay($smarty, 'pages/error_page.tpl');
               exit;
          }

          // return false;
     }

     public function query($q) {
          global $mysql_conn;
          return $mysql_conn->query($q);
     }

     public function prepare($a) {
          global $mysql_conn;
          return $mysql_conn->prepare($a);
     }

     public function lastInsertId() {
          global $mysql_conn;
          return $mysql_conn->lastInsertId();
     }
     
     // deze oude constructie neemt alle pdo classes en functies over
     // met je eigen inlog credentials. perfect! ...er was alleen een probleem
     // met deze constructie toen ... iets wou doen met met ...
     public function oude_construct_() {          
          $options = [
               PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
               PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
               PDO::ATTR_EMULATE_PREPARES   => false,
          ];

          try {
               $db_host = VOICE_IP;
               if(Net::isWebserverOn('voice_pi')) {
                    $db_host = 'localhost';
               }

               echo "mysqlCONNECTweeee";

               // parent::__construct("mysql:host=".$db_host.";dbname=".DB_NAME.";charset=".DB_CHARSET, DB_USER, DB_PASS, $options);
          } catch (\PDOException $e) {
               throw new \PDOException($e->getMessage(), (int)$e->getCode());
          }
     }
}
