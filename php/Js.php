<?php

$js_settings = [];

class Js {
    function __construct() {
        Js::addSetting('OTHER_WEBSERVER', Net::otherWebServer());
        Js::addSetting('VOICE_HTTP', Net::voiceHTTP());
        Js::addSetting('WEBSERVER_ON_VOICE', Net::isWebserverOn('voice_pi'));
    }

    /**
     * scan js/ directory for .js files and include these files via given method
     * in config.php setting variables.
     */
    function assignSmarty($smarty) {
        $assign = $this->getFileUriTags();
        if(JS_EMBED == true) {
            $assign = '<script>'.$this->getMerged().'</script>';
        }

        if(JS_EMBED == false && JS_MERGE == true) {
            $assign = '<script src="/merged.js"></script>';
        }

        // add js settings
        $assign .= '<script>const settings = '.Js::getSettings().';</script>';

        $smarty->assign('js', $assign);
    }

    static function getSettings() {
        global $js_settings;
        return json_encode($js_settings);
    }

    function getSetting($key) {
        global $js_settings;

        if(!isset($js_settings[$key])) {
            return [];
        }

        return $js_settings[$key];
    }

    static function addSetting($key, $val) {
        global $js_settings;
        $js_settings[$key] = $val;
    }

    static function addMsg($msg, $type = 'success') {
        $js = new Js();
        $msgs = $js->getSetting('messages');

        $msgs[] = [
            'msg' => $msg,
            'type' => $type,
        ];

        Js::addSetting('messages', $msgs);
    }

    /**
     * return array;
     * 
     * give back files from js/ folder
     */
    function getFilenames() {
        $files = array_merge(
            glob('js/*/*.js'),
            glob('js/*.js')
        );

        return $files;
    }

    /**
     * return string;
     */
    function getFileUriTags() {
        $js = '';
        foreach($this->getFilenames() as $filename) {
            $uri = '/'.$filename;

            $js .= '<script src="'.$uri.'"></script>'."\n";
        }

        return $js;
    }

    /**
     * return string;
     * 
     * concats all files together as one long file
     */
    function getMerged() {
        $js = '';
        foreach($this->getFilenames() as $filename) {
            $uri = '/'.$filename;
            $filepath = getcwd().$uri;

            $js .= file_get_contents($filepath)."\n\n";
        }

        if(JS_MINIFY) {
            echo "js minifier not supported";die();
        }

        header('Content-Type: application/javascript');
        return $js;
    }
}
