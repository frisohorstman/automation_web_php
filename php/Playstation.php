<?php

class Playstation {
    /**
     * asked to boot or shutdown ps4 via _GET parameter
     */
    function jsonPage() {
        if(!isset($_GET['ps4Power'])) {
            return false;
        }

        $output = $this->power($_GET['ps4Power']);
        return json_encode($output);
    }

    /**
     * power playstation on/off.
     */
    function power($action = 'on') {
        if(!Net::isWebserverOn('voice_pi')) {
            return Json::get('Playstation', 'power', array($action));
        }

        $output = shell_exec('sudo /home/friso/frisos_automation/start.sh playstation '.$action.' 2>&1');

        $success = true;
        if(strpos($output, 'Could not detect any matching PS4 device') !== false) {
            $success = false;
        }

        return array(
            'success' => $success,
            'output' => $output,
        );
    }

    /**
     * return boolean;
     * 
     * check if playstation is on, via database query.
     * 
     * !note
     * this on/off state is updated via a cronjob.
     * 
     * fetching the state directly costs alot of time thats
     * why this is handled via a cronjob...
     */
    function isPoweredOn() {
        $db = new Mysql();

        $sql = "SELECT
                    *
                FROM
                    playstation";
    
        $query = $db->prepare($sql);
        $query->execute();
        $result = $query->fetch();

        $power = false;
        if($result['power'] == 'on') {
            $power = true;
        }

        return $power;
    }
}