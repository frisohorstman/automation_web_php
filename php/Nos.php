<?php

class Nos {
    /**
     * return array;
     *
     * gets rss feed from nos website, this fetch sometimes fails,
     * so this fetch should goto database if it succeeds
     */
    function getExternal() {
        $url = 'http://feeds.nos.nl/nosnieuwsalgemeen';
        $feed = file_get_contents($url);

        return $feed;
    }

    /**
     * return string/false; $feed
     *
     * saves feed to database
     */
    function saveExternal($feed = false) {
        if($feed == false) {
  	      $feed = $this->getExternal();
	    }

        if($feed === false) {
            return false;
        }
        $this->deleteFromDatabase();
        $this->insertToDatabase($feed);

        return $feed;
    }

    function deleteFromDatabase() {
        $db = new Mysql();

        $sql = 'DELETE FROM home_automation.nos_rss';
    	return $db->query($sql);
    }

    /**
     * return boolean; $success
     *
     * fetch rss feed from nos.nl and save to database
     */
    function insertToDatabase($feed) {
        $db = new Mysql();

        if($feed == false) {
            return false;
        }

        // insert function
        $sql = 'INSERT INTO
                    nos_rss (id,rss,created)
                VALUES
                    (NULL, ?, ?)';

        $query = $db->prepare($sql);
        $success = $query->execute([$feed, date('Y-m-d H:i:s')]);

        return $success;
    }

    /**
     * return string/false;
     *
     * tries to fetch a file from the database, if this file is younger than 30 seconds then the
     * fetch from the nos website wont be done at all. the rss file weve got in the database
     * will be supplied. if its older, there will be tried to fetch rss feed from nos site.
     * if this isnt successfull the database file is used after all..
     */
    function getFromDatabase() {
        $db = new Mysql();

        $sql = "SELECT
                    *
                FROM
                    nos_rss
                ORDER BY
                    created DESC
               LIMIT 0, 1";

        $query = $db->query($sql);
        $result = $query->fetch();

        return $result;
    }

    /**
     * gets feed either from nos website or from database
     */
    function getRss() {
        $db_feed = $this->getFromDatabase();

        if($db_feed == false) {
            $feed = $this->getExternal();
            $this->saveExternal($feed);
        } else {
            $now = new DateTime();
            $created = new DateTime($db_feed['created']);
            $diff = $now->getTimestamp() - $created->getTimestamp();

            if($diff > 30) {
                // too old, fetch new rss feed from website
                $feed = $this->saveExternal();

                if($feed == false) {
                    $feed = $db_feed['rss'];
                }
            } else {
                // use cache from database
                $feed = $db_feed['rss'];
            }
        }

        return $this->parseNewsFeed($feed);
    }

    /**
     * return array;
     *
     * gets rss file from database or nos website then
     * rearrange rss feed and add time ago to feed.
     */
    function parseNewsFeed($rss) {
        $feed = simplexml_load_string($rss, 'SimpleXMLElement', LIBXML_NOCDATA);

        $news_feed = array();
        for($i = 0; $i < 3; $i++) {
            $item = $feed->channel->item[$i];
            $item->pubDate = $this->timeElapsed($item->pubDate);
            $news_feed[] = $item;
        }

        return $news_feed;
    }

    /**
     * input various;
     * return string;
     *
     * gives back time ago string. example: 30 seconds ago
     * @todo; replace with javascript time ago
     */
    function timeElapsed($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'jaren',
            'm' => 'maanden',
            'w' => 'weken',
            'd' => 'dagen',
            'h' => 'uur',
            'i' => 'minuten',
            's' => 'seconden',
        );

        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v;
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' geleden' : 'zojuist';
    }
}
