<?php

class Zway {
    /**
     * page /zway/json
     */
    function jsonPage() {
        if(isset($_GET['url'])) {
            return $this->apiGet();
        }

        return json_encode(array(
            'success' => false,
        ));
    }


    /**
     * return json
     * 
     * gets zway api json directly from pi, z-way-server needs to be running (zwave.me)
     */
    function apiGet($url = false) {        
        if($url == false) {
            $url = 'http://'.Net::deviceGetHost('zwave_pi').'/'.$_GET['url'];
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_PORT, ZWAY_API['port']);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_USERPWD, ZWAY_API['user'].':'.ZWAY_API['pass']);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }

    function getDevices() {
        $url = 'http://'.Net::deviceGetHost('zwave_pi').'/ZAutomation/api/v1/devices';
        $json = $this->apiGet($url);

        return json_decode($json);
    }

    function findDevice($findDeviceId = false) {
        $devices = $this->getDevices();

        foreach($devices->data->devices as $device) {
            if ($device->id == $findDeviceId) {
                return $device;
            }
        }

        return false;
    }
}