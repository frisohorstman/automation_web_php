<?php

class Json {
    /**
     * page /json
     */
    function jsonPage() {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: *");

        // $auth = new Auth();
        // if(!$auth->isLoginValid()) {
        //     return json_encode(array(
        //         'success' => false,
        //         'error' => 'auth failed',
        //     ));
        // }

        if(isset($_GET['class'])) {
            $class = $_GET['class'];
            $method = $_GET['method'];

            return $this->execMethod($class, $method);
        }

        return json_encode(array(
            'success' => false,
            'error' => 'unhandled json request',
        ));
    }
    

    /**
     * communicating with the voice raspberry pi is done via JSON.
     */
    static function get($class, $method, $args = []) {
        $device = Net::getDevice('voice_pi');
        $domain = 'https://'.$device['host'].'/json';

        $query = [
            'class' => $class,
            'method' => $method,
            'args' => $args,
        ];

        $url = $domain.'?'.http_build_query($query);

        // @todo, should be fixed by downloading ca of voicepi to this webserver
        $ssl_options = array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        // echo $url;die();
        
        $txt = file_get_contents($url, false, stream_context_create($ssl_options));
        
        
        if(empty($txt)) {
            Js::addMsg('Kon voice JSON niet ophalen', 'danger');
            return false;
        }

        $json = json_decode($txt, true);

        if(empty($json)) {
            // echo "json is empty: ".$txt;
            return false;
        }


        return $json[$method];
    }

    /**
     * functie in class uitvoeren en in json plaatsen
     */
    function execMethod($class, $method) {
        $c = new $class;
        if(!method_exists($c, $method)) {
            echo 'ERROR: Method does not exist.';
            exit;
        }

        // check if there are arguments
        $args = [];
        if(isset($_GET['args'])) {
            $args = $_GET['args'];
        }
        
        // call method in class
        $data = $c->$method(...$args);
        
        return json_encode(array(
            $method => $data
        ));
    }
}
