<?php

class OtherPages extends Boot {
    function homePage() {
        $smarty = $this->smarty();

        $tv = new Tv();
        $nos = new Nos();
        $voice = new Voice();
        $playstation = new Playstation();

        $smarty->assign('news_feed', $nos->getRss());
        $smarty->assign('playstation_running', $playstation->isPoweredOn());
        $smarty->assign('is_tv_powered_on', $tv->isPoweredOn());
        $smarty->assign('db_voice_notes', $voice->getNotes(array('hidden_in_dashboard' => 0, 'limit' => 3)));

        $this->smartyDisplay($smarty, 'pages/home.tpl');
    }

    function statusPage() {
        // catch (\PDOException $e) {
        // if()
        $smarty = $this->smarty();
        $statuses = array();

        // html dots
        $dot_green = '<div class="dot"></div>';
        $dot_red = '<div class="dot danger"></div>';
        
        // zway
        $buttons = '';
        $zwave_ip = Net::deviceGetIp('zwave_pi');
        $is_zwave_pi_on = Net::ping($zwave_ip);
        if($is_zwave_pi_on) {
            $zway = new Zway();
            $device = $zway->findDevice('ZWayVDev_zway_3-0-37');
            $ganglamp = $dot_red.'<span>De ganglamp staat uit</span>';

            $buttons = $this->getButtonsHTML('ganglamp-on', 'Aan', 'success', 'data-zway-device="ZWayVDev_zway_3-0-37"');
            if($device->metrics->level == 'on') {
                $ganglamp = $dot_green.'<span>De ganglamp staat aan</span>';
                $buttons = $this->getButtonsHTML('ganglamp-off', 'Uit', 'danger', 'data-zway-device="ZWayVDev_zway_3-0-37"');
            }
        } else {
            $ganglamp = $dot_red.'<span>De zwave pi is onbereikbaar</span>';
        }

        $statuses[] = array(
            'title' => 'Ganglamp',
            'buttons' => $buttons,
            'text' => $ganglamp,
            'td_tags' => 'data-zway-device="ZWayVDev_zway_3-0-37"',
        );

        // playstation
        $ps4 = new Playstation();
        $ps4_power = $dot_red.'<span>De PS4 staat uit</span>';
        $is_ps4_powered_on = $ps4->isPoweredOn();
        if($is_ps4_powered_on) {
            $ps4_power = $dot_green.'<span>De PS4 staat aan</span>';
        }

        if($is_ps4_powered_on) {
            $buttons = $this->getButtonsHTML('ps4-power-off', 'Uit', 'danger', 'style="display: block;"');
            $buttons .= $this->getButtonsHTML('ps4-power-on', 'Aan', 'success', 'style="display: none;"');
        } else {
            $buttons = $this->getButtonsHTML('ps4-power-off', 'Uit', 'danger', 'style="display: none;"');
            $buttons .= $this->getButtonsHTML('ps4-power-on', 'Aan', 'success', 'style="display: block;"');
        }

        $statuses[] = array(
            'title' => 'Playstation 4',
            'buttons' => $buttons,
            'text' => $ps4_power,
        );

        // tv
        $tv = new Tv();
        $tv_huiskamer = $dot_red.'<span>De TV staat uit</span>';
        $tv_is_powered_on = $tv->isPoweredOn();
        if($tv_is_powered_on) {
            $tv_huiskamer = $dot_green.'<span>De TV staat aan</span>';
        }

        if($tv_is_powered_on) {
            $buttons = $this->getButtonsHTML('tv-power-off', 'Uit', 'danger reload', 'data-remote="Samsung_AA59-00786A" data-key="KEY_POWER"');
        } else {
            $buttons = $this->getButtonsHTML('tv-power-on', 'Aan', 'success reload', 'data-remote="Samsung_AA59-00786A" data-key="KEY_POWER"');
        }

        $statuses[] = array(
            'title' => 'TV Huiskamer',
            'buttons' => $buttons,
            'text' => $tv_huiskamer,
        );

        // zwave pi
        if($is_zwave_pi_on) {
            $buttons = $this->getButtonsHTML('reboot-zwave-pi', 'Reboot', 'danger');
        }

        $statuses[] = array(
            'title' => 'Zwave Pi',
            'buttons' => $buttons,
            'text' => $this->statusDeviceHTML($is_zwave_pi_on, 'De zwave pi'),
        );

        // voice pi
        $voice_ip = Net::deviceGetIp('voice_pi');
        $is_voice_pi_on = Net::ping($voice_ip);
        if($is_voice_pi_on) {
            $device = Net::getDevice('voice_pi');
            $voice_url = Net::getHTTP($device);

            // $buttons = $this->getButtonsHTML('reboot-voice-pi', 'Reboot', 'danger');
            $buttons  = '<div class="btn-group" role="group">';
            $buttons .= '  <button type="button" id="reboot-voice-pi" class="btn btn-sm btn-outline-danger">Reboot</button>';
            $buttons .= '  <a href="'.$voice_url.'?no_redirect" class="btn btn-sm btn-outline-secondary">Webserver</a>';
            $buttons .= '</div>';
        }
        
        $statuses[] = array(
            'title' => 'Voice Pi',
            'buttons' => $buttons,
            'text' => $this->statusDeviceHTML($is_voice_pi_on, 'De voice pi'),
        );

        // server
        $server_ip = Net::deviceGetIp('server');
        $is_server_on = Net::ping($server_ip);

        $buttons = '';
        if($is_server_on) {
            // $buttons = $this->getButtonsHTML('reboot-server', 'Reboot', 'danger');
            $buttons  = '<div class="btn-group" role="group">';
            $buttons .= '  <button type="button" id="reboot-server" class="btn btn-sm btn-outline-danger">Reboot</button>';
            $buttons .= '  <button type="button" id="shutdown-server" class="btn btn-sm btn-outline-danger">Shutdown</button>';
            $buttons .= '</div>';
        } else {
            $buttons  = '<button type="button" id="wake-server" class="btn btn-sm btn-outline-success">Opstarten</button>';
        }

        $statuses[] = array(
            'title' => 'Server',
            'buttons' => $buttons,
            'text' => $this->statusDeviceHTML($is_server_on, 'De server'),
        );

        // laptop
        $laptop_ip = Net::deviceGetIp('laptop');
        $is_laptop_on = Net::ping($laptop_ip);
        $buttons = '';
        if($is_laptop_on) {
            // $buttons = $this->getButtonsHTML('reboot-laptop', 'Reboot', 'danger');
            $buttons  = '<div class="btn-group" role="group">';
            $buttons .= '  <button type="button" id="reboot-laptop" class="btn btn-sm btn-outline-danger">Reboot</button>';
            $buttons .= '  <button type="button" id="shutdown-laptop" class="btn btn-sm btn-outline-danger">Shutdown</button>';
            $buttons .= '</div>';
        }

        $statuses[] = array(
            'title' => 'Laptop',
            'buttons' => $buttons,
            'text' => $this->statusDeviceHTML($is_laptop_on, 'De laptop'),
        );

        $smarty->assign('statuses', $statuses);
        $smarty->assign('vcr_status', Vcr::status());

        $only_dev = true;
        if(Net::isWebserverOn('voice_pi')) {
            $only_dev = false;
        }

        $df =  $this->diskSpaceFree(false, $only_dev);
        $smarty->assign('disk_space', $df);

        $smarty->assign('is_filepath_vlc_running', Vlc::isRunning());
        $smarty->assign('is_filepath_stream_running', Media::isFilepathVideoStreamRunning());
        // $smarty->assign('is_voice_control_listening', Voice::isListening());
        $smarty->assign('webserver_on_voice', Net::isWebserverOn('voice_pi'));

        $this->smartyDisplay($smarty, 'pages/status/page.tpl');
    }

    function getButtonsHTML($id = '', $text = 'Stop', $type = 'danger', $html_tags = '') {
        return '<button class="btn btn-outline-'.$type.' btn-sm" id="'.$id.'" '.$html_tags.'>'.$text.'</button>';
    }

    function statusDeviceHTML($is_on = false, $device_name = false) {
        // html dots
        $dot_green = '<div class="dot"></div>';
        $dot_red = '<div class="dot danger"></div>';

        $html = $dot_red.'<span>'.$device_name.' staat uit</span>';
        if($is_on) {
            $html = $dot_green.'<span>'.$device_name.' staat aan</span>';
        }

        return $html;
    }
}