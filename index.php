<?php

require('config.php');
require('routes.php');
require('boot.php');

if(DEBUG) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

$boot = new Boot();
$boot->run();