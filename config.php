<?php

// bla
define('DEBUG', true);

/* MySQL settings */
define('MYSQL', [
    'host' => 'server',
    'database' => 'home_automation',
    'charset' => 'utf8mb4',
    'user' => 'friso',
    'pass' => 'bierentieten',
]);

/* Login for this website (When connecting via internet) */
define('AUTH', [
    [
        'user' => 'Dr-007',
        'pass' => 'bierentieten',
        'access' => 'root',
    ], [
        'user' => 'Visitor',
        'pass' => 'anderww',
        'access' => [
            '/media/folder',
            '/media/file',
        ],
    ]
]);

/* Transmission torrent client htpp settings */
define('TRANSMISSION_HTTP', [
    'host' => '127.0.0.1',
    'port' => '8080',
    'user' => 'friso',
    'pass' => 'bierentieten'
]);

define('ZWAY_API', [
    'port' => '8083',
    'user' => 'admin',
    'pass' => 'S0n1cb00m',
]);

define('NETWORK_DEVICES', [
    'router' => array(
        'ip' => '10.0.0.1',
        'port' => '8080',
    ),
    'tv' => array(
        'ip' => '10.0.0.116',
    ),
    'laptop' => array(
        'ip' => '10.0.0.105',
    ),
    'server' => array(
        'ip' => '10.0.0.108',
        'host' => 'server.home',
        'mac' => '18:C0:4D:DE:5E:13',
        'webserver' => array(
            'host' => 'server.frisohorstman.nl',
            // 'port' => '8080',
            // 'https_port' => '8443',
            'primary' => true,
        )
    ),
    'voice_pi' => array(
        'ip' => '10.0.0.120',
        'host' => 'automation.home',
        'webserver' => array(
            'host' => 'voice.frisohorstman.nl',
        )
    ),
    'zwave_pi' => array(
        'ip' => '10.0.0.111',
        'host' => 'zwavepi.home',
        'webserver' => array(
            'host' => 'zwave.frisohorstman.nl',
        )
    ),
]);

/* dev, merge js/css settings */
define('JS_EMBED', false);
define('JS_MERGE', true);
define('JS_MINIFY', false);

define('CSS_EMBED', false);
define('CSS_MERGE', false);
define('CSS_MINIFY', false);

