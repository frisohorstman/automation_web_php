<?php

# this works, but python websocket server is better.

include_once(__DIR__.'/../config.php');
include_once(__DIR__.'/../boot.php');

$boot = new Boot();
$boot->includePhp();

$websocket = new Websocket();
$websocket->listen();

class Websocket {
    private $server = false;
    private $address = '0.0.0.0';
    private $port = 12345;

    // vars
    private $clients = [];
    private $identified = [];

    private $voice_status = [
        'is_listening' => false
    ];

    private $infrared_status = [
        'is_listening' => false
    ];

    private $time = false;

    function listen() {
        $this->server = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        $this->clients[] = $this->server;

        socket_set_option($this->server, SOL_SOCKET, SO_REUSEADDR, 1);
        socket_bind($this->server, $this->address, $this->port);
        socket_listen($this->server);
        $client = socket_accept($this->server);
        $this->clients[] = $client;

        // send websocket http headers
        $this->sendHeaders($client);

        while (true) {
            $this->mainloop();
        }

        // close the listening socket
        socket_close($this->server);
    }

    function mainloop() {
        // create a copy, so $clients doesn't get modified by socket_select()
        $read = $this->clients;
        $null = NULL;

        if(empty($read)) {
            return false;
        }

        if (false === socket_select($read, $null, $null, NULL)) {
            echo "socket_select() failed, reason: ".socket_strerror(socket_last_error()) . "\n";
            die();
        }

        // check if there is a client trying to connect
        if (in_array($this->server, $read)) {
            $read = $this->accept($this->server, $read);
        }

        // recieve data
        foreach ($read as $read_sock) {
            $r = @socket_recv($read_sock, $buf, 1024, 0);

            if ($r === FALSE) {
                echo "socket_recv() failed, reason: ".socket_strerror(socket_last_error()) . "\n";
                $this->disconnect($read_sock);
                continue;
            } elseif ($r === 0) {
                $this->disconnect($read_sock);
                continue;
            } else {
                $txt = $this->unmask($buf);

                // echo "buf: ".$buf."\n";
                echo "websocket message: ".$txt."\n";

                // act on recieved data
                // $this->onData($this->server, $txt);
                $this->onData($read_sock, $txt);
            }
        }

        // send data
        $filename = 'websocket_send';
        if(file_exists($filename)) {
            $msg = file_get_contents($filename);

            if(!empty($msg)) {
                file_put_contents($filename, '');
                $this->writeToAll($msg);
            }
        }
    }

    // text we recieved from websocket sent by browser
    function onData($sock, $data) {
        if(empty($data)) {
            return false;
        }

        $json = json_decode($data);
        print_r($json);

        // write text to all sockets
        $this->writeToAll($data);

        // something identified itself
        if(isset($json->identify)) {
            $sock_index = array_search($sock, $this->clients);

            $this->identified[$sock_index] = array(
                'identifier' => $json->identify,
                'resource' => $this->clients[$sock_index],
            );

            if($json->identify == 'voice') {
                $this->voice_status['is_listening'] = true;
            }

            if($json->identify == 'infrared') {
                $this->infrared_status['is_listening'] = true;
            }

            echo "someone identified as ".$json->identify."\n";
            $this->statusWriteTo('all', $sock);
        }

        // call php class and method
        if(isset($json->php)) {
            if(isset($json->php->class) && isset($json->php->method)) {
                $json = new Json();
                $result = $json->execMethod($json->php->class, $json->php->method);
                print_r($result);
                echo "\n\n";
            }
        }
    }

    function writeToAll($msg, $except_sock = false) {
        foreach ($this->clients as $send_sock) {
            // if its the listening sock or the client that we got the message from, go to the next one in the list
            // if ($send_sock == $this->server) {
            if($send_sock == $except_sock || $send_sock == $this->server) {
                continue;
            }

            $this->write($send_sock, $msg);
        }
    }

    /**
     * input false/socket_resource
     * 
     * when false status will be written to all sockets
     */
    function statusWriteTo($sock = false, $except_sock = false) {
        $data = json_encode([
                             'infrared' => [ 'status' => $this->infrared_status ],
                             'voice' => [ 'status' => $this->voice_status ]
                            ]);

        if($sock == 'all') {
            $this->writeToAll($data, $except_sock);
        } else {
            $this->write($sock, $data);
        }
    }

    function write(&$sock, $msg) {
        $send = chr(129) . chr(strlen($msg)) . $msg;
        $success = socket_write($sock, $send);

        if ($success === false) {
            echo "write fail\n";
            socket_close($sock);
            $this->disconnect($sock);
            return false;
        }

        return true;
    }

    function disconnect($sock) {
        // remove client for $clients array
        $sock_index = array_search($sock, $this->clients);
        unset($this->clients[$sock_index]);
        
        if(isset($this->identified[$sock_index]) && $this->identified[$sock_index]['resource'] == $sock) {
            if($this->identified[$sock_index]['identifier'] == 'voice') {
                $this->voice_status['is_listening'] = false;
            }
            
            if($this->identified[$sock_index]['identifier'] == 'infrared') {
                $this->infrared_status['is_listening'] = false;
            }
            
            echo "removing identifier\n";
	        unset($this->identified[$sock_index]);
            $this->statusWriteTo('all');
        }
        
        // notify php server process
        socket_getpeername($sock, $ip);
        echo "client [$ip] disconnected.\n";
    }

    function accept($sock, $read) {
        // accept the client, and add him to the $clients array
        $newsock = socket_accept($sock);
        $this->clients[] = $newsock;

        // send websock headers
        $this->sendHeaders($newsock);

        socket_getpeername($newsock, $ip);
        echo "New client connected: {$ip}\n";
        
        // remove the listening socket from the clients-with-data array
        $key = array_search($sock, $read);
        unset($read[$key]);

        // give connecting client listening status of infrared/voice
        $this->statusWriteTo($newsock);

        return $read;
    }

    function sendHeaders($sock) {
        echo "sending headers!\n";
        $request = socket_read($sock, 5000);
        preg_match('#Sec-WebSocket-Key: (.*)\r\n#', $request, $matches);
        $key = base64_encode(pack(
            'H*',
            sha1($matches[1] . '258EAFA5-E914-47DA-95CA-C5AB0DC85B11')
        ));

        $headers = "HTTP/1.1 101 Switching Protocols\r\n";
        $headers .= "Upgrade: websocket\r\n";
        $headers .= "Connection: Upgrade\r\n";
        $headers .= "Sec-WebSocket-Version: 13\r\n";
        $headers .= "Sec-WebSocket-Accept: $key\r\n\r\n";
        socket_write($sock, $headers, strlen($headers));
    }

    function unmask($payload) {
        if(empty($payload)) {
            return false;
        }

        $decMessages = Array();

        do {
            $length = ord($payload[1]) & 127;
                  
            $masks = substr($payload, 2, 4);
            $data = substr($payload, 6);
            
            // concat string
            $text = '';
            for ($i = 0; $i < $length; ++$i) {   
                $text .= $data[$i] ^ $masks[$i%4];
            }
            
            $decMessages[] = trim($text);
            
            $payload = substr($payload, $length, strlen($payload));
        } while ($length < strlen($data));
        
        return $decMessages[0];
    }
}
