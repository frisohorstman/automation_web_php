<?php

$address = '0.0.0.0';
$port = 12345;

// Create WebSocket.
$server = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
socket_set_option($server, SOL_SOCKET, SO_REUSEADDR, 1);
socket_bind($server, $address, $port);
socket_listen($server);
$client = socket_accept($server);

// Send WebSocket handshake headers.
$request = socket_read($client, 5000);
preg_match('#Sec-WebSocket-Key: (.*)\r\n#', $request, $matches);
$key = base64_encode(pack(
    'H*',
    sha1($matches[1] . '258EAFA5-E914-47DA-95CA-C5AB0DC85B11')
));

$headers = "HTTP/1.1 101 Switching Protocols\r\n";
$headers .= "Upgrade: websocket\r\n";
$headers .= "Connection: Upgrade\r\n";
$headers .= "Sec-WebSocket-Version: 13\r\n";
$headers .= "Sec-WebSocket-Accept: $key\r\n\r\n";
socket_write($client, $headers, strlen($headers));

recieve_from_browser();

// Send messages into WebSocket in a loop.
// sends to browser
function send_to_browser() {
    while (true) {
        sleep(1);
        $content = 'Now: ' . time();
        $response = chr(129) . chr(strlen($content)) . $content;
        socket_write($client, $response);
    }
}

function recieve_from_browser() {
    global $client;
    global $server;

    $clients = [$client];
    $socket = $server;
    $null = null;

    while (true) {     
        //manage multipal connections     
        $changed = $clients;
        //returns the socket resources in $changed array
        if(empty($changed)) {
            continue;
        }
        
        socket_select($changed, $null, $null, 0, 10);
        
        //check for new socket
        if (in_array($socket, $changed)) {                      
            $socket_new = socket_accept($socket); //accpet new socket
            $clients[] = $socket_new; //add socket to client array
        
            $header = socket_read($socket_new, 10240); //read data sent by the socket
            perform_handshaking($header, $socket_new, $host, $port); //perform websocket handshake
        
            socket_getpeername($socket_new, $ip); //get ip address of connected socket
            $response = mask(json_encode(array('type' => 'system', 'status' => true, "id" => "SRV_CONNECTED", 'message' => $ip . ' connected'))); //prepare json data
            send_message($response); //notify all users about new connection
            //make room for new socket
            $found_socket = array_search($socket, $changed);
            unset($changed[$found_socket]);
        
        }
        foreach ($changed as $changed_socket) {                   
            while (@socket_recv($changed_socket, $buf, 1024, 0) >= 1) {                              
                $received_text = unmask($buf); //unmask data                
                print_r( $received_text );
                // $tst_msg = json_decode($received_text, true); //json decode                
                // $response_text = parse_msg($tst_msg, $changed_socket);                                    
                break 2; //exit this loop
            }    
        
        
            $buf = @socket_read($changed_socket, 10240, PHP_NORMAL_READ);           
            if ($buf === false) { // check disconnected client
                // remove client for $clients array
                $found_socket = array_search($changed_socket, $clients);
                socket_getpeername($changed_socket, $ip);
                unset($clients[$found_socket]);
        
                //notify all users about disconnected connection
                // $response = mask(json_encode(array('type' => 'system', 'message' => $ip . ' disconnected')));
                // send_message($response);
                echo "socket read stuff";
            }
        }
    }
}

function unmask($payload){  
    $decMessages = Array();  
    do { // This should be running until all frames are unmasked and added to $decMessages Array 
      $length = ord($payload[1]) & 127;
    
      if($length == 126) {
          $masks = substr($payload, 4, 4);
          $data = substr($payload, 8);
          $len = (ord($payload[2]) << 8) + ord($payload[3]);
      }elseif($length == 127) {
          $masks = substr($payload, 10, 4);
          $data = substr($payload, 14);
          $len = (ord($payload[2]) << 56) + (ord($payload[3]) << 48) +
              (ord($payload[4]) << 40) + (ord($payload[5]) << 32) +
              (ord($payload[6]) << 24) +(ord($payload[7]) << 16) +
              (ord($payload[8]) << 8) + ord($payload[9]);
      }else {        
          $masks = substr($payload, 2, 4);
          $data = substr($payload, 6);
          $len = $length;
      }
    
      $text = '';
      for ($i = 0; $i < $len; ++$i) {    
          $text .= $data[$i] ^ $masks[$i%4];
      }
    
      $decMessages[] = $text;
    
      // Here is problem. It doesn't put correct substr to $payload, so it could run again on stream shorted by last message     
    //   $payload = substr($payload, $length, strlen($payload));     
    $payload = substr($payload, $length, $len + 8); 
    }while (($len < strlen($data)) and $countert < 10);
    
    return $decMessages;
}

