<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Home automation">
    <meta name="author" content="Friso Horstman">
    <title>Home automation login</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/non_auto_include/login.css" rel="stylesheet">

    <!-- Javascript -->
    <script type="text/javascript" src="/js/lib/1jquery-slim.min.js"></script>
    <script type="text/javascript" src="/js/1msg.js"></script>
    <script>const settings = '{$js_settings}';</script>

    <script>
      $(document).ready(function () {
        const same_webserver_on_network = '{$same_webserver_on_network}';
        const other_webserver = '{$other_webserver}';

        // check if network is available via json call
        $.getJSON(same_webserver_on_network + '/net/json', { is_awake: 'get' }, function() {
            document.location = same_webserver_on_network;
        });
      });
    </script>

    <!-- Favicons -->
    <meta name="theme-color" content="#7952b3">
  </head>
  
  <body class="text-center">
    <main class="form-signin">
      <form method="POST" id="login_form">
        <h1 class="h3 mb-3 fw-normal">Please sign in</h1>

        <div class="form-floating">
          <input type="text" name="username" id="username" class="form-control" id="floatingInput" placeholder="Useranme">
          <label for="floatingInput">Username</label>
        </div>

        <div class="form-floating">
          <input type="password" name="password" id="password" class="form-control" id="floatingPassword" autocomplete="current-password" placeholder="Password" required>
          <label for="floatingPassword">Password</label>
        </div>

        <button class="w-100 btn btn-lg btn-primary" id="login_submit" type="submit">Sign in</button>
        <p class="mt-5 mb-3 text-muted">Home automation</p>
      </form>
    </main>
  </body>
</html>
