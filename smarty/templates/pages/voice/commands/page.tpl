{include file="./modal.inc.tpl"}

<div class="container py-5" id="voice-commands-page">
    <h1 class="cover-heading">Trefwoorden</h1>

    <p class="lead">
        Dit zijn de trefwoorden welke zijn verbonden aan functies.
        Als een functie niet bestaat word deze uiteraard niet aangeroepen.<br>
        Bekijk de <a href="/voice/log">logs</a> als je niet begrijpt wat er mis gaat.

        <div>
            <form method="GET" style="width: calc(100% - 130px); margin-left: 10px; float: left;">
                <div class="form-group">
                    <input type="text" name="search" class="form-control" placeholder="Zoek zoals stemherkenning doet. Oftewel gebruik zinnen." value="{if isset($_GET['search'])}{$_GET['search']}{/if}">
                </div>
            </form>
            <button type="button" class="btn btn-primary" style="float: right;" id="voice_command_create">Aanmaken</button>
        </div>

        {include file="./table.inc.tpl"}
    </p>
</div>