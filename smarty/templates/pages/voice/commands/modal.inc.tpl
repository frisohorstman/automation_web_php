<link href="/css/bootstrap-tagsinput.css" rel="stylesheet">
<script src="/js/lib/bootstrap-tagsinput.js"></script>

<!-- Modal -->
<div class="modal fade" id="modal_voice_commands" tabindex="-1" aria-labelledby="modal_voice_commands_title" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="POST" onkeydown="return event.key != 'Enter';">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_voice_commands_title">Edit Voice Command</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>

        <div class="modal-body">
          <input type="hidden" name="form_name" value="voice_command">
          <input type="hidden" name="function_id" id="function_id">

          <div class="input-group mb-3">
            <input type="text" class="form-control" name="comment" id="comment" placeholder="Description">
          </div>

          <strong>Function</strong>
          <div class="input-group mb-3">
            <input type="text" class="form-control" style="width: 110px; flex: none;" name="module" id="module" placeholder="module">
            <span class="input-group-text">.</span>
            <input type="text" class="form-control" style="width: 140px; flex: none;" name="class" id="class" placeholder="class">
            <span class="input-group-text">().</span>
            <input type="text" class="form-control" style="width: 151px; flex: none;" name="function" id="function" placeholder="function">
            <span class="input-group-text" style="margin-top: 5px;">(</span>
            <input type="text" class="form-control" style="margin-top: 5px;" name="arg1" id="arg1" placeholder="arg1">
            <span class="input-group-text" style="margin-top: 5px;">,</span>
            <input type="text" class="form-control" style="margin-top: 5px;" name="arg2" id="arg2" placeholder="arg2">
            <span class="input-group-text" style="margin-top: 5px;">)</span>
          </div>

          <strong>Keywords</strong>
          <input type="text" value="" name="keywords" id="keywords" data-role="tagsinput" />
        </div>

        <div class="custom-modal-footer">
          <button type="submit" name="delete" class="btn btn-danger btn-delete" style="float: left;">Delete</button>

          <button type="submit" class="btn btn-primary">Save</button>
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
</div>

