
<table class="table table-hover table-automation">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Python functie</th>
            <th scope="col">Trefwoorden</th>
            {if isset($db_voice_commands[0]['occurrences'])}<th scope="col">occurrences</th>{/if}
            <th scope="col">Wanneer</th>
        </tr>
    </thead>
    <tbody>
        {foreach $db_voice_commands as $voice}
            <tr class="voice_command_table_click {if isset($voice['occurrences']) && $voice['occurrences'] < 2}table-danger{/if}">
                <th scope="row">{$voice['id']}</th>
                <td class="function" data-id="{$voice['id']}" data-module="{$voice['module']}" data-class="{$voice['class']}" data-function="{$voice['function']}" data-arg1="{$voice['arg1']}" data-arg2="{$voice['arg2']}" data-comment="{$voice['comment']}">
                    <h3>{$voice['comment']}</h3>
                    <p>{$voice['module']}.{if $voice['class']}{$voice['class']}().{/if}{$voice['function']}({$voice['arg1']}{if $voice['arg2']}, {$voice['arg2']}{/if})</p>
                </td>
                <td class="keywords" data-keywords="{$voice['keywords']}">
                    {$voice['keywords']}
                </td>
                {if isset($voice['occurrences'])}
                    <td>
                        {$voice['occurrences']}
                    </td>
                {/if}
                <td>
                    {$voice['created']|date_format:"%e %B %Y %H:%M"}
                </td>
            </tr>
        {/foreach}
    </tbody>
</table>