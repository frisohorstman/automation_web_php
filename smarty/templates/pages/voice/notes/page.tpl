<div class="container py-5" id="voice-notes-page">
    <h1 class="cover-heading">Stem notities</h1>

    <p class="lead">
        Opgenomen notities

        <div>
            <button type="button" class="btn btn-primary" style="float: right;" id="voice_note_create">Create</button>
        </div>

        {include file="./table.inc.tpl"}
    </p>
</div>