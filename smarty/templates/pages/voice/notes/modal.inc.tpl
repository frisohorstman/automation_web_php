<!-- Modal -->
<div class="modal fade" id="modal_voice_notes" tabindex="-1" aria-labelledby="modal_voice_notes" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="POST" action="/voice/notes">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_voice_commands">Bewerk notitie</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>

        <div class="modal-body">
          <input type="hidden" name="form_name" value="voice_note">
          <input type="hidden" name="note_id" id="note_id">

          <div class="input-group mb-3">
            <textarea class="form-control" name="note" id="note" placeholder="Notitie"></textarea>
          </div>

          <div class="form-check hidden-in-dashboard-checkbox">
            <input class="form-check-input" name="hidden_in_dashboard" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">
              Hidden in dashboard
            </label>
          </div>
        </div>

        <div class="custom-modal-footer">
          <button type="submit" name="delete" class="btn btn-danger btn-delete" style="float: left;">Delete</button>
          <button type="button" name="hide" class="btn btn-warning btn-hide" style="float: left;">Hide in dashboard</button>

          <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>

