{include file="./modal.inc.tpl"}

<table class="table table-hover table-automation">
    {if !isset($shown_in_dashboard)}
        <thead>
            <tr>
                <th scope="col">Opgenomen tekst</th>
                <th scope="col">Verstopt in dashboard</th>
                <th scope="col">Aangemaakt</th>
            </tr>
        </thead>
    {/if}
    <tbody>
        {foreach $db_voice_notes as $note}
            <tr class="voice_notes_table_click" data-note="{$note['note']}" data-id="{$note['id']}" data-hidden-in-dashboard="{$note['hidden_in_dashboard']}">
                <td>
                    {$note['note']}
                </td>
                {if !isset($shown_in_dashboard)}
                <td>
                    {if $note['hidden_in_dashboard']}
                        ja
                    {else}
                        nee
                    {/if}
                </td>
                {/if}
                <td>
                    {$note['created']|date_format:"%e %B %Y %H:%M"}
                </td>
            </tr>
        {/foreach}
    </tbody>
</table>