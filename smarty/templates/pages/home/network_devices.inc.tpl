<div class="container py-5" id="network-devices">
    <h2 class="pb-2 border-bottom">Netwerk apparaten</h2>

    <div class="device-container">
        <div class="loading">
            Loading...<br>
            <small>(Contacting router)</small>
        </div>
        
        <div>
            <div class="large-left-side">
                <div id="internet_iface">
                    <div class="block hidden">
                        <a href="#">
                            <svg class="bi" width="100%"><use xlink:href="#globe"/></svg>
                            <h3>Internet</h3>
                            <p>MM:AA:CC:AA:DD:RR</p>
                            <div>10.0.0.1</div>
                        </a>
                    </div>
                </div>
                 
                <svg class="left-right-arrows" width="20px"><use xlink:href="#up-down-arrows"/></svg>
                <svg class="up-down-arrows" width="20px"><use xlink:href="#up-down-arrows"/></svg>
            </div>
            
            <div class="devices">
                <div id="ethernet"></div>
                <div id="wireless"></div>
            </div>
        </div>
    </div>
</div>