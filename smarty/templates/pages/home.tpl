
{if count($db_voice_notes) == 1}
    <div class="bg-light p-5 container">
        <h1 class="display-6">
            {ucfirst($db_voice_notes[0]['note'])}
        </h1>
        <p class="lead">
            Achtergelaten notitie op {$db_voice_notes[0]['created']|date_format:"%e %B"} om {$db_voice_notes[0]['created']|date_format:"%H:%M"}
        </p>
        <a class="btn btn-primary btn-lg" href="?hide_in_dashboard&note_id={$db_voice_notes[0]['id']}" role="button">
            Verstop notitie
        </a>
    </div>
{elseif count($db_voice_notes) > 1}
    <div class="container py-5" id="voice-notes-page">
        <h2 class="pb-2 border-bottom">Opgenomen notities</h2>

        <p class="lead">
            {assign var="shown_in_dashboard" value="true"}
            {include file="./voice/notes/table.inc.tpl"}
        </p>
    </div>
    <div class="b-example-divider"></div>
{/if}


{include file="./home/top_info.inc.tpl"}
<div class="b-example-divider"></div>

{include file="./home/single_actions.inc.tpl"}
<div class="b-example-divider"></div>

{include file="./home/network_devices.inc.tpl"}
<div class="b-example-divider"></div>

{include file="./home/mass_actions.inc.tpl"}
<div class="b-example-divider"></div>

{include file="./home/latest_news.inc.tpl"}

