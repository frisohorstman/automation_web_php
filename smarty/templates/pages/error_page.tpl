<div class="container py-5" id="error-page">
    <h2 class="pb-2 border-bottom">{$title}</h2>
    
    <p class="lead">
        {$error_message}
    </p>
</div>