<div class="container py-5 infrared-remotes-container">
    <h2 class="pb-2 border-bottom">Infrared remotes</h2>
    
    {foreach $remotes as $remote}
        <table class="table table-hover infrared-remote-table">
            <thead>
                <tr>
                    <th scope="col">{$remote['name']}</th>
                </tr>
            </thead>
            <tbody>
                {foreach $remote['keys'] as $key => $code}
                    <tr>
                        <td scope="row" data-remote="{$remote['name']}" data-key="{$key}">
                            {$key}
                        </td>
                    </tr>
                {/foreach}
            </tbody>
        </table>
    {/foreach}
</div>