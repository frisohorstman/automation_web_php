{include file="./svg.inc.tpl"}

<div class="container py-5 infrared-remotes-container">
  <h2 class="pb-2 border-bottom" style="text-align: center;">
    Afstandsbediening<br>Surround Set
  </h2>

  <div class="ir-remote surround-remote">
    <div class="ir-line">
      <div style="width: 92.35px;">
        <div class="title" style="text-align: center; height: 30px;">
          <svg class="bi" style="width: 25px; height: 25px;"><use xlink:href="#eject"/></svg>
        </div>
        <button class="ir-btn w100 ir-btn-flat" data-remote="" data-key=""></button>
      </div>
      <button class="ir-btn ir-btn-power-green" data-remote="Samsung_AA59-00786A" data-key="KEY_POWER">
        <svg class="bi" width="1em" height="1em"><use xlink:href="#power"/></svg>
      </button>
    </div>

    <div class="ir-line">
      <button class="ir-btn w31 m20 small-txt ir-btn-flat ir-btn-white" data-remote="Samsung_AA59-00786A" data-key="KEY_1">BD</button>
      <button class="ir-btn-circle w23" data-remote="Samsung_AA59-00786A" data-key="KEY_1">1</button>
      <button class="ir-btn-circle w23" data-remote="Samsung_AA59-00786A" data-key="KEY_2">2</button>
      <button class="ir-btn-circle w23" data-remote="Samsung_AA59-00786A" data-key="KEY_3">3</button>
    </div>
    <div class="ir-line">
      <button class="ir-btn w31 m20 small-txt ir-btn-flat ir-btn-white" data-remote="Samsung_AA59-00786A" data-key="KEY_4">TV</button>
      <button class="ir-btn-circle w23" data-remote="Samsung_AA59-00786A" data-key="KEY_5">4</button>
      <button class="ir-btn-circle w23" data-remote="Samsung_AA59-00786A" data-key="KEY_5">5</button>
      <button class="ir-btn-circle w23" data-remote="Samsung_AA59-00786A" data-key="KEY_6">6</button>
    </div>
    <div class="ir-line">
      <button class="ir-btn w31 m20 small-txt ir-btn-flat ir-btn-white" data-remote="Samsung_AA59-00786A" data-key="KEY_7">STB</button>
      <button class="ir-btn-circle w23" data-remote="Samsung_AA59-00786A" data-key="KEY_8">7</button>
      <button class="ir-btn-circle w23" data-remote="Samsung_AA59-00786A" data-key="KEY_8">8</button>
      <button class="ir-btn-circle w23" data-remote="Samsung_AA59-00786A" data-key="KEY_9">9</button>
    </div>


    <div class="ir-line">
      <button class="ir-btn w31 m20 small-txt ir-btn-flat" data-remote="Samsung_AA59-00786A" data-key="KEY_7"></button>
      <button class="ir-btn w23 m20 small-txt ir-btn-flat" data-remote="Samsung_AA59-00786A" data-key="KEY_8"></button>
      <button class="ir-btn-circle w23" data-remote="Samsung_AA59-00786A" data-key="KEY_8">0</button>
      <button class="ir-btn w23 m20 ir-btn-flat" data-remote="Samsung_AA59-00786A" data-key="KEY_9"></button>
    </div>


    <div class="ir-line">
      <button class="ir-btn small-txt red-btn" data-remote="Samsung_AA59-00786A" data-key="KEY_A_RED"></button>
      <button class="ir-btn small-txt green-btn" data-remote="Samsung_AA59-00786A" data-key="KEY_B_GREEN"></button>
      <button class="ir-btn small-txt yellow-btn" data-remote="Samsung_AA59-00786A" data-key="KEY_C_YELLOW"></button>
      <button class="ir-btn small-txt blue-btn" data-remote="Samsung_AA59-00786A" data-key="KEY_D_BLUE"></button>
    </div>

    <div class="ir-line">
      <div style="width: 33%;">
        <div class="title">
          TOP MENU
        </div>
        <button class="ir-btn-circle ir-btn-circle-small" style="margin-left: 10px;" data-remote="Samsung_AA59-00786A" data-key="ROUND"></button>
      </div>

      <button class="ir-btn btn-up" style="height: 45px; margin-top: 22px;" data-remote="Samsung_AA59-00786A" data-key="KEY_ARROW_UP"></button>

      <div style="width: 33%;">
        <div class="title" style="text-align: right;">
          POP UP/MENU
        </div>
        <button class="ir-btn-circle ir-btn-circle-small" style="margin-right: 10px; float: right;" data-remote="Samsung_AA59-00786A" data-key="ROUND"></button>
      </div>
    </div>

    <div class="ir-line">
      <button class="ir-btn btn-left" style="height: 45px;" data-remote="Samsung_AA59-00786A" data-key="KEY_ARROW_LEFT"></button>
      <button class="ir-btn btn-ok" style="height: 45px;" data-remote="Samsung_AA59-00786A" data-key="KEY_OK"></button>
      <button class="ir-btn btn-right" style="height: 45px;" data-remote="Samsung_AA59-00786A" data-key="KEY_ARROW_RIGHT"></button>
    </div>

    <div class="ir-line">
      <div style="width: 33%;">
        <div class="title">
          RETURN
        </div>
        <button class="ir-btn-circle ir-btn-circle-small" style="margin-left: 10px;" data-remote="Samsung_AA59-00786A" data-key="ROUND"></button>
      </div>

      <button class="ir-btn btn-down" style="height: 45px;" data-remote="Samsung_AA59-00786A" data-key="KEY_ARROW_UP"></button>

      <div style="width: 33%;">
        <div class="title" style="text-align: right;">
          OPTIONS
        </div>
        <button class="ir-btn-circle ir-btn-circle-small" style="margin-right: 10px; float: right;" data-remote="Samsung_AA59-00786A" data-key="ROUND"></button>
      </div>
    </div>

    <div class="ir-line">
      <div style="width: 33%;">
        <div class="title">
          FUNCTION
        </div>
        <button class="ir-btn-circle ir-btn-circle-small" style="margin-left: 10px;" data-remote="Samsung_AA59-00786A" data-key="ROUND"></button>
      </div>

      <div style="width: 33%;">
        <div class="title" style="text-align: center;">
          MENU
        </div>
        <button class="ir-btn small-txt blue-btn" style="width: 100%; border-radius: 15px;" data-remote="Samsung_AA59-00786A" data-key="KEY_D_BLUE">HOME</button>
      </div>


      <div style="width: 33%;">
        <div class="title" style="text-align: right;">
          SOUND MODE
        </div>
        <button class="ir-btn-circle ir-btn-circle-small" style="margin-right: 10px; float: right;" data-remote="Samsung_AA59-00786A" data-key="ROUND"></button>
      </div>
    </div>

    <div class="ir-line">
      <button class="ir-btn small-txt" data-remote="Samsung_AA59-00786A" data-key="KEY_REWIND">
        <svg class="bi" width="1em" height="1em" style="margin-right: 0px;"><use xlink:href="#skip-start"/></svg>
      </button>
      <button class="ir-btn small-txt" data-remote="Samsung_AA59-00786A" data-key="KEY_PLAY">
        <svg class="bi" width="1em" height="1em" style="margin-right: 0px; transform: rotate(180deg);"><use xlink:href="#forward"/></svg>
      </button>
      <button class="ir-btn small-txt" data-remote="Samsung_AA59-00786A" data-key="KEY_PAUSE">
        <svg class="bi" width="1em" height="1em" style="margin-right: 0px;"><use xlink:href="#forward"/></svg>
      </button>
      <button class="ir-btn small-txt" data-remote="Samsung_AA59-00786A" data-key="KEY_FAST_FWD">
        <svg class="bi" width="1em" height="1em" style="margin-right: 0px;"><use xlink:href="#skip-end"/></svg>
      </button>
    </div>

    <div class="ir-line">
      <button class="ir-btn small-txt" style="width: 67.5px;" data-remote="Samsung_AA59-00786A" data-key="KEY_REWIND">
        <svg class="bi" width="1em" height="1em" style="margin-right: 0px;"><use xlink:href="#skip-backward"/></svg>
      </button>
      <button class="ir-btn small-txt" style="width: 47%;" data-remote="Samsung_AA59-00786A" data-key="KEY_PLAY">
        <svg class="bi" width="1em" height="1em" style="margin-right: 0px;"><use xlink:href="#play"/></svg>
      </button>
      <button class="ir-btn small-txt" style="width: 67.5px;" data-remote="Samsung_AA59-00786A" data-key="KEY_FAST_FWD">
        <svg class="bi" width="1em" height="1em" style="margin-right: 0px;"><use xlink:href="#skip-forward"/></svg>
      </button>
    </div>

    <div class="ir-line">
      <button class="ir-btn small-txt" style="width: 67.5px;" data-remote="Samsung_AA59-00786A" data-key="KEY_REWIND">
        SUBT
      </button>
      <div style="width: 47%; text-align: center;">
        <button class="ir-btn small-txt" style="width: 67.5px;" data-remote="Samsung_AA59-00786A" data-key="KEY_PLAY">
          <svg class="bi" width="1em" height="1em" style="margin-right: 0px;"><use xlink:href="#pause2"/></svg>
        </button>
      </div>
      <button class="ir-btn small-txt" style="width: 67.5px;" data-remote="Samsung_AA59-00786A" data-key="KEY_FAST_FWD">
        <svg class="bi" width="1em" height="1em" style="margin-right: 0px;"><use xlink:href="#stop"/></svg>
      </button>
    </div>


    <div class="ir-line">
      <div class="w25">
        <div style="height: 78px;">
          <div class="title" style="padding-left: 8px; height: 25px;">
            <svg class="bi" style="width: 23px; height: 23px;"><use xlink:href="#mute"/></svg>
          </div>
          <button class="ir-btn-circle ir-btn-circle-small" data-remote="Samsung_AA59-00786A" data-key="ROUND"></button>
        </div>
        <div>
          <div class="title" style="padding-left: 8px; height: 25px;">
            <svg class="bi" style="width: 23px; height: 23px;"><use xlink:href="#mute"/></svg>
          </div>
          <button class="ir-btn-circle ir-btn-circle-small" data-remote="Samsung_AA59-00786A" data-key="ROUND"></button>
        </div>
      </div>
      <div class="ir-col">
        <button class="ir-btn" data-remote="Samsung_AA59-00786A" data-key="KEY_VOL_UP">
          <div class="large-txt" style="left: 23px;">+</div>
        </button>
        <button class="ir-btn" data-remote="Samsung_AA59-00786A" data-key="KEY_VOL_DOWN">
          <div class="large-txt" style="left: 38px; font-size: 60px; left: 23px; top: -17px;">-</div>
        </button>
      </div>
      <div class="ir-col">
        <button class="ir-btn btn-up" style="font-size: 33px;" data-remote="Samsung_AA59-00786A" data-key="KEY_CHAN_UP"></button>
        <button class="ir-btn btn-down" style="font-size: 33px;"  data-remote="Samsung_AA59-00786A" data-key="KEY_CHAN_DOWN"></button>
      </div>
      <div class="w25">
        <div style="height: 78px;">
          <div class="title" style="text-align: right; height: 25px;">
            DISPLAY
          </div>
          <button class="ir-btn-circle ir-btn-circle-small" style="float: right;" data-remote="Samsung_AA59-00786A" data-key="ROUND"></button>
        </div>
        <div>
          <div class="title" style="text-align: right; height: 25px; padding-right: 5px;">
            SLEEP
          </div>
          <button class="ir-btn-circle ir-btn-circle-small" style="float: right;" data-remote="Samsung_AA59-00786A" data-key="ROUND"></button>
        </div>
      </div>
    </div>
  </div>
</div>