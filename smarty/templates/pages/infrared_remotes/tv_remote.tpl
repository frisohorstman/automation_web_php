{include file="./svg.inc.tpl"}

<div class="container py-5 infrared-remotes-container">
  <h2 class="pb-2 border-bottom" style="text-align: center;">Afstandsbediening TV</h2>

  <div class="ir-remote">
    <div class="ir-line">
      <button class="ir-btn ir-btn-power" data-remote="Samsung_AA59-00786A" data-key="KEY_POWER">
        <svg class="bi" width="1em" height="1em"><use xlink:href="#power"/></svg>
      </button>
      <button class="ir-btn ir-hidden"></button>
      <button class="ir-btn ir-hidden"></button>
      <button class="ir-btn small-txt" data-remote="Samsung_AA59-00786A" data-key="KEY_SOURCE">
        <svg class="bi" width="1em" height="1em"><use xlink:href="#box-arrow-in-right"/></svg>
        SOURCE
      </button>
    </div>

    <div class="ir-line">
      <button class="ir-btn" data-remote="Samsung_AA59-00786A" data-key="KEY_1">1</button>
      <button class="ir-btn" data-remote="Samsung_AA59-00786A" data-key="KEY_2">2</button>
      <button class="ir-btn" data-remote="Samsung_AA59-00786A" data-key="KEY_3">3</button>
    </div>
    <div class="ir-line">
      <button class="ir-btn" data-remote="Samsung_AA59-00786A" data-key="KEY_4">4</button>
      <button class="ir-btn" data-remote="Samsung_AA59-00786A" data-key="KEY_5">5</button>
      <button class="ir-btn" data-remote="Samsung_AA59-00786A" data-key="KEY_6">6</button>
    </div>
    <div class="ir-line">
      <button class="ir-btn" data-remote="Samsung_AA59-00786A" data-key="KEY_7">7</button>
      <button class="ir-btn" data-remote="Samsung_AA59-00786A" data-key="KEY_8">8</button>
      <button class="ir-btn" data-remote="Samsung_AA59-00786A" data-key="KEY_9">9</button>
    </div>
    <div class="ir-line">
      <button class="ir-btn small-txt" data-remote="Samsung_AA59-00786A" data-key="KEY_TTX">
        <svg class="bi" width="1em" height="1em"><use xlink:href="#teletext"/></svg>
        TTX
      </button>
      <button class="ir-btn" data-remote="Samsung_AA59-00786A" data-key="KEY_0">0</button>
      <button class="ir-btn small-txt" data-remote="Samsung_AA59-00786A" data-key="KEY_PREV_CHAN">PRE-CH</button>
    </div>

    <div class="ir-line">
      <div class="ir-col">
        <button class="ir-btn" data-remote="Samsung_AA59-00786A" data-key="KEY_VOL_UP">
          <div class="large-txt">+</div>
        </button>
        <button class="ir-btn" data-remote="Samsung_AA59-00786A" data-key="KEY_VOL_DOWN">
          <div class="large-txt" style="left: 38px; font-size: 60px; top: -17px;">-</div>
        </button>
      </div>

      <div class="ir-col">
        <button class="ir-btn small-txt" style="height: 65px; margin-bottom: 5px;" data-remote="Samsung_AA59-00786A" data-key="KEY_MUTE">
          MUTE<br>
          <svg class="bi" style="width: 27px; height: 27px; margin-right: -7px;"><use xlink:href="#mute"/></svg>
        </button>
        <button class="ir-btn small-txt" style="height: 65px; margin-top: 5px;" onclick="alert('missing...');">
          CH LIST<br>
          <svg class="bi" style="margin: 0px; margin-top: 4px;"><use xlink:href="#channel-list"/></svg>
        </button>
      </div>

      <div class="ir-col">
        <button class="ir-btn btn-up" style="font-size: 33px;" data-remote="Samsung_AA59-00786A" data-key="KEY_CHAN_UP"></button>
        <button class="ir-btn btn-down" style="font-size: 33px;"  data-remote="Samsung_AA59-00786A" data-key="KEY_CHAN_DOWN"></button>
      </div>
    </div>

    <div class="ir-line">
      <button class="ir-btn small-txt" data-remote="Samsung_AA59-00786A" data-key="KEY_MENU">
        <svg class="bi" width="1em" height="1em"><use xlink:href="#menu"/></svg>
        MENU
      </button>
      <button class="ir-btn small-txt" data-remote="Samsung_AA59-00786A" data-key="KEY_SMART_HUB">SMART HUB</button>
      <button class="ir-btn small-txt" data-remote="Samsung_AA59-00786A" data-key="KEY_GUIDE">GUIDE</button>
    </div>

    <div class="ir-line">
      <button class="ir-btn small-txt" data-remote="Samsung_AA59-00786A" data-key="KEY_TOOLS">
        <svg class="bi" width="1em" height="1em"><use xlink:href="#remote-list"/></svg>
        TOOLS
      </button>
      <button class="ir-btn btn-up" data-remote="Samsung_AA59-00786A" data-key="KEY_ARROW_UP"></button>
      <button class="ir-btn small-txt" data-remote="Samsung_AA59-00786A" data-key="KEY_INFO">
        <svg class="bi" width="1em" height="1em"><use xlink:href="#remote-info"/></svg>
        INFO
      </button>
    </div>

    <div class="ir-line">
      <button class="ir-btn btn-left" data-remote="Samsung_AA59-00786A" data-key="KEY_ARROW_LEFT"></button>
      <button class="ir-btn btn-ok" data-remote="Samsung_AA59-00786A" data-key="KEY_OK"></button>
      <button class="ir-btn btn-right" data-remote="Samsung_AA59-00786A" data-key="KEY_ARROW_RIGHT"></button>
    </div>

    <div class="ir-line">
      <button class="ir-btn" data-remote="Samsung_AA59-00786A" data-key="KEY_RETURN">
        <div style="float: left; margin-left: 3px; margin-right: 5px;">&#8634;</div>
        <div class="small-txt" style="float: left; margin-top: 9px;">RETURN</div>
      </button>
      <button class="ir-btn btn-down" data-remote="Samsung_AA59-00786A" data-key="KEY_ARROW_DOWN"></button>
      <button class="ir-btn small-txt" data-remote="Samsung_AA59-00786A" data-key="KEY_EXIT">
        <svg class="bi" width="1em" height="1em"><use xlink:href="#remote-close"/></svg>
        EXIT
      </button>
    </div>

    <div class="ir-line">
      <button class="ir-btn small-txt red-btn" data-remote="Samsung_AA59-00786A" data-key="KEY_A_RED">A</button>
      <button class="ir-btn small-txt green-btn" data-remote="Samsung_AA59-00786A" data-key="KEY_B_GREEN">B</button>
      <button class="ir-btn small-txt yellow-btn" data-remote="Samsung_AA59-00786A" data-key="KEY_C_YELLOW">C</button>
      <button class="ir-btn small-txt blue-btn" data-remote="Samsung_AA59-00786A" data-key="KEY_D_BLUE">D</button>
    </div>

    <div class="ir-line">
      <button class="ir-btn small-txt" data-remote="Samsung_AA59-00786A" data-key="KEY_EMANUAL">MANUAL</button>
      <button class="ir-btn small-txt" data-remote="Samsung_AA59-00786A" data-key="KEY_3D">3D</button>
      <button class="ir-btn small-txt" data-remote="Samsung_AA59-00786A" data-key="KEY_SUBTITLE">SUBT</button>
      <button class="ir-btn small-txt" data-remote="Samsung_AA59-00786A" data-key="KEY_STOP">
        <svg class="bi" width="1em" height="1em" style="margin-right: 0px;"><use xlink:href="#stop"/></svg>
      </button>
    </div>

    <div class="ir-line">
      <button class="ir-btn small-txt" data-remote="Samsung_AA59-00786A" data-key="KEY_REWIND">
        <svg class="bi" width="1em" height="1em" style="margin-right: 0px;"><use xlink:href="#skip-backward"/></svg>
      </button>
      <button class="ir-btn small-txt" data-remote="Samsung_AA59-00786A" data-key="KEY_PLAY">
        <svg class="bi" width="1em" height="1em" style="margin-right: 0px;"><use xlink:href="#play"/></svg>
      </button>
      <button class="ir-btn small-txt" data-remote="Samsung_AA59-00786A" data-key="KEY_PAUSE">
        <svg class="bi" width="1em" height="1em" style="margin-right: 0px;"><use xlink:href="#pause2"/></svg>
      </button>
      <button class="ir-btn small-txt" data-remote="Samsung_AA59-00786A" data-key="KEY_FAST_FWD">
        <svg class="bi" width="1em" height="1em" style="margin-right: 0px;"><use xlink:href="#skip-forward"/></svg>
      </button>
    </div>

    <div class="ir-line" style="margin-bottom: 30px;">
      AA59-00786A
    </div>
    <div class="ir-line" style="margin-bottom: 30px;">
        <svg class="bi" style="margin-top: -70px; margin-left: 15%; color: black; width: 70%;"><use xlink:href="#samsung-logo"/></svg>
    </div>
  </div>
</div>