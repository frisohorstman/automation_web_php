
    <nav aria-label="Navigatie" style="float: right;">
        <ul class="pagination">
            {if (isset($_GET['search_url']) && $pagination[0] != $_GET['search_url'])}
                {foreach $pagination as $pageNo => $url}
                    {if $url == $_GET['search_url']}
                        {math equation="pageNo - 1" pageNo=$pageNo assign="back_no"}
                        {assign var="back_url" value=$pagination[$back_no]}
                    {/if}
                {/foreach}

                <li class="page-item">
                    <a class="page-link" href="{$back_url}">Previous</a>
                </li>
            {/if}

            {foreach $pagination as $i => $uri}
                {assign var="url" value=$uri['url']}
                {assign var="pageNo" value=$uri['pageNumber']}

                {if (isset($_GET['search_url']) && $url == $_GET['search_url']) || (!isset($_GET['search_url']) && $i == 0)}
                    {math equation="pageNo + 1" pageNo=$i assign="next_no"}
                    {assign var="next_url" value=false}
                    {if isset($pagination[$next_no]['url'])}
                        {assign var="next_url" value=$pagination[$next_no]['url']}
                    {/if}

                    <li class="page-item active">
                        <a class="page-link" href="{$url}">
                            {$pageNo} <span class="sr-only hidden">(current)</span>
                        </a>
                    </li>
                {else}
                    <li class="page-item">
                        <a class="page-link" href="{$url}">
                            {$pageNo}
                        </a>
                    </li>
                {/if}
            {/foreach}

            {if $next_url != false}
                <li class="page-item"><a class="page-link" href="{$next_url}">Next</a></li>
            {/if}
        </ul>
    </nav>