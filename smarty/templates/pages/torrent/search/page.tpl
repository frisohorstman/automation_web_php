<div class="container py-5" id="torrent-search-page">
    <h1 class="cover-heading">Torrents zoeken</h1>

    <p class="lead">
        <form method="GET">
            <div class="form-group">
                <input type="text" class="form-control" name="search" value="{$torrent_search}" placeholder="Zoekterm">
                <input type="submit" class="btn btn-primary mt-3" value="Zoeken">
            </div>
        </form>

        {if empty($torrents) && !empty($torrent_search)}
            <br><br>
            Niets gevonden...
        {/if}

        {include file="./table.inc.tpl"}
    </p>
</div>