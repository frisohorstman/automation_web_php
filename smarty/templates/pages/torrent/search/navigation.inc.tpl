
    <nav aria-label="Navigatie" style="float: right;">
        <ul class="pagination">
            {foreach $pagination as $i => $uri}
                {assign var="url" value=$uri['url']}
                {assign var="pageNo" value=$uri['pageNumber']}

                {if (isset($_GET['search_url']) && $url == $_GET['search_url']) || (!isset($_GET['search_url']) && $i == 0)}
                    <li class="page-item active">
                        <a class="page-link" href="{$url}">
                            {$pageNo} <span class="sr-only hidden">(current)</span>
                        </a>
                    </li>
                {else}
                    <li class="page-item">
                        <a class="page-link" href="{$url}">
                            {$pageNo}
                        </a>
                    </li>
                {/if}
            {/foreach}
        </ul>
    </nav>