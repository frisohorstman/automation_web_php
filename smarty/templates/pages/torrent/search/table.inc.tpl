{if $torrents}
<div class="torrent-search-table-container">
    <table class="table table-hover torrent-search-table">
        <thead>
            <tr>
                <th scope="col">Titel</th>
                <th scope="col">Grootte</th>
                <th scope="col">Seeders</th>
                <th scope="col">Leechers</th>
                <th scope="col">Datum</th>
            </tr>
        </thead>
        <tbody>
            {foreach $torrents as $torrent}
                <tr>
                    <td>
                        <a href="{$torrent['href']}" class="torrent-magnet">
                            {$torrent['title']}
                        </a>
                    </td>
                    <td>{$torrent['size']}</td>
                    <td>{$torrent['seeders']}</td>
                    <td>{$torrent['leechers']}</td>
                    <td>{$torrent['date']}</td>
                </tr>
            {/foreach}
        </tbody>
    </table>

    {include file="./navigation.inc.tpl"}
</div>
{/if}