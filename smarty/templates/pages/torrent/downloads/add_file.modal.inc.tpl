<!-- Modal -->
<div class="modal fade" id="modal_add_torrent" role="dialog" tabindex="-1" aria-labelledby="add_torrent_title" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="POST" enctype="multipart/form-data">
        <div class="modal-header">
          <h5 class="modal-title" id="add_torrent_title">Torrent toevoegen</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>

        <div class="modal-body">
          <input type="hidden" name="form_name" value="add_torrent">

          <div class="custom-file">
            <input type="file" class="custom-file-input" id="torrent" name="torrent">
            <label class="custom-file-label" for="torrent">Selecteer .torrent</label>
          </div>

          <div class="input-group mb-3">
            <input type="text" class="form-control" id="magnet" placeholder="Of plak een .magnet link">
          </div>
        </div>

        <div class="custom-modal-footer">
          <button type="submit" class="btn btn-primary btn-save">Save</button>
          <button type="button" class="btn btn-secondary" style="float: left;" data-bs-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
</div>

