<div class="title-wrapper">
    <h1 class="cover-heading">Torrent downloads</h1>

    <div class="button-container">
        <button type="button" class="btn btn-primary" id="torrent-add-file-btn">
            <svg class="bi" width="20px" height="20px"><use xlink:href="#file-plus"/></svg>
            Torrent uploaden
        </button>
    </div>
</div>