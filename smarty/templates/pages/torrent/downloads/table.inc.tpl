{if isset($torrent_error)}
    <h1 class="cover-heading">Torrent downloads</h1>
    {$torrent_error}
{else}
    {if empty($downloads)}
        {include file="./title.inc.tpl"}
        Er staat niets in je transmission download lijst.<br>
        Wil je naar torrents <a href="/torrent/search">zoeken?</a>
    {else}
        <form class="torrent-downloads-table" id="torrent-downloads-form" method="POST">
            {include file="./title.inc.tpl"}

            <div class="check-all mobile-devices">
                <div class="form-check">
                    <input class="form-check-input check-all" type="checkbox" value="">
                    <label>Check all</label>
                </div>
            </div>

            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">
                            <div class="form-check">
                                <input class="form-check-input check-all" type="checkbox" value="">
                            </div>
                        </th>
                        <th scope="col">Naam</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col">Down</th>
                        <th scope="col">Up</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    {foreach $downloads as $download}
                        <tr data-torrent-id="{$download->id}">
                            <td>
                                <div class="form-check">
                                    <input class="form-check-input mass-checkbox" type="checkbox" name="torrent_ids[]" value="{$download->id}">
                                </div>
                            </td>
                            <td class="title">
                                <a href="/torrent/details?id={$download->id}">
                                    {$download->name}
                                </a>
                            </td>
                            <td>
                                <div class="progress">
                                    <div class="progress-bar {if $download->percentDone == 100}bg-success{/if}" style="width: 0%" role="progressbar" aria-valuenow="{$download->percentDone}" aria-valuemin="0" aria-valuemax="100"></div>
                                    <div class="progress-text {if $download->percentDone == 100}text-white{/if}">
                                        {$download->percentDone}%  ({$download->sizeDownloadedHuman}/{$download->sizeWhenDoneHuman})
                                    </div>
                                </div>
                            </td>
                            <td class="eta">
                                {if $download->status == 4}
                                    {$download->etaHuman}
                                {else}
                                    -
                                {/if}
                            </td>
                            <td class="downspeed">
                                {$download->rateDownloadHuman}/s
                            </td>
                            <td class="upspeed">
                                {$download->rateUploadHuman}/s
                            </td>
                            <td>
                                <a href="/torrent/downloads?remove={$download->id}" class="torrent-remove" data-percent-done="{$download->percentDone}">
                                    <svg class="bi" width="20px" height="20px" style="margin-right: 5px;"><use xlink:href="#remove"/></svg>
                                </a>

                                
                                <a href="/torrent/downloads?start={$download->id}" class="torrent-start" {if $download->status == 4 || $download->status == 6}style="display: none;"{/if}>
                                    <svg class="bi"><use xlink:href="#play2"/></svg>
                                </a>

                                <a href="/torrent/downloads?stop={$download->id}" class="torrent-stop" {if $download->status != 4}style="display: none;"{/if}>
                                    <svg class="bi"><use xlink:href="#pause"/></svg>
                                </a>
                            </td>
                        </tr>
                    {/foreach}
                </tbody>
            </table>


            <div class="dropdown mass-action-container" id="mass-action-container">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="mass-actions" data-bs-toggle="dropdown" aria-expanded="false">
                    Met geselecteerde
                </button>

                <input type="hidden" id="mass_action" name="mass_action" value="delete_finished" />
                <ul class="dropdown-menu mass-actions-list" aria-labelledby="mass-actions">
                    <li>
                        <a class="dropdown-item" href="javascript://" data-action="delete_finished">
                            Verwijder uit de download lijst
                        </a>
                    </li>
                    <li>
                        <a class="dropdown-item" href="javascript://" data-action="delete_remove_data">
                            Verwijder uit delijst en verwijder gedownloade data
                        </a>
                    </li>
                </ul>
            </div>
        </form>
    {/if}
{/if}