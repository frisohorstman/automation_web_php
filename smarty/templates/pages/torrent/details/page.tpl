<div class="container py-5" id="torrent-details-page">
    <h1 class="cover-heading">Torrent details</h1>

    <p class="lead">
        {$details->name}<br><br>
        <a href="/torrent/downloads">Ga terug naar de downloads</a>

        {include file="./table.inc.tpl"}
    </p>
</div>