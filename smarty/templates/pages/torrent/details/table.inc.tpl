<table class="table torrent-details-table">
    <thead>
        <tr>
            <th scope="col">Files</th>
            <th scope="col">Completed</th>
            <th scope="col">Priority</th>
        </tr>
    </thead>
    <tbody>
        {foreach $details->files as $file_id => $file}
            <tr>
                <td>
                    {$file->name}
                </td>
                <td>
                    <div class="progress">
                        <div class="progress-bar {if $file->percentDone == 100}bg-success{/if}" style="width: {$file->percentDone}%" role="progressbar" aria-valuenow="{$file->percentDone}" aria-valuemin="0" aria-valuemax="100"></div>
                        <div class="progress-text {if $file->percentDone == 100}text-white{/if}">
                            {$file->percentDone}% ({$file->sizeCompletedHuman}/{$file->sizeWhenDoneHuman})
                        </div>
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <select class="form-control torrent-priority" data-torrent-id="{$_GET['id']}" data-file-id="{$file_id}">
                            <option value="high" {if $file->priority == 1}selected{/if}>High</option>
                            <option value="normal" {if $file->priority == 0}selected{/if}>Normal</option>
                            <option value="low" {if $file->priority == -1}selected{/if}>Low</option>
                        </select>
                    </div>
                </td>
            </tr>
        {/foreach}
    </tbody>
</table>