<table class="table status-table devices-table">
    <tbody>
        {foreach $statuses as $status}
            <tr>
                <td>
                    {$status['buttons']}
                </td>
                <td {if isset($status['td_tags'])}{$status['td_tags']}{/if}>
                    {$status['text']}
                </td>
            </tr>
        {/foreach}
    </tbody>
</table>