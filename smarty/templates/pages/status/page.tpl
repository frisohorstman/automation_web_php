<div class="container py-5" id="status-page">
    <p class="lead">
        <h2>Schijfruimte op server</h2>
        {include file="./diskspace_table.inc.tpl"}

        <h2>Activiteiten</h2>
        {include file="./activities_table.inc.tpl"}

	    {if !$webserver_on_voice}
          <h2>Videorecorder</h2>
          {include file="../media/vcr/status_table.inc.tpl"}
        {/if}

        <h2>Apparaten</h2>
        {include file="./devices_table.inc.tpl"}
    </p>
</div>
