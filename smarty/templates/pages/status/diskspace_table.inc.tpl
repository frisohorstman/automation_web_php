<table class="table diskspace-table">
    <thead>
        <tr>
            <th scope="col">Schijfnaam</th>
            <th scope="col">Partitie</th>
            <th scope="col">Mountpunt</th>
            <th scope="col">Vrij</th>
            <th scope="col"></th>
        </tr>
    </thead>
    <tbody>
        {foreach $disk_space as $i => $disk}
            {if $webserver_on_voice || (!$webserver_on_voice && $disk['label'])}
                <tr>
                    <td>
                        {$disk['label']|truncate:30}
                    </td>
                    <td>
                        {$disk['filesystem']}
                    </td>
                    <td>
                        <a href="/media/folder?filepath={$disk['mountpoint']}&find_video=0">
                            {$disk['mountpoint']}
                        </a>
                    </td>
                    <td>
                        {$disk['available']}
                    </td>
                    <td>
                        <div class="progress">
                            <div class="progress-bar {if $disk['used_percentage'] > 70}bg-danger{/if}" style="width: {$disk['used_percentage']}%" role="progressbar" aria-valuenow="{$disk['used_percentage']}" aria-valuemin="0" aria-valuemax="100"></div>
                            <div class="progress-text">
                                {$disk['used_percentage']}% used {$disk['used']} / {$disk['size']}
                            </div>
                        </div>
                    </td>
                </tr>
            {/if}
        {/foreach}
    </tbody>
</table>
