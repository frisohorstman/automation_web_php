<table class="table status-table activity-table">
    <tbody>
        <tr class="table-listening-status hidden">
            <td>
                <div class="btn-group" role="group">
                    <button class="btn btn-outline-danger btn-sm voice-recognition-stop">Stop</button>
                    <a class="btn btn-sm btn-outline-secondary" href="/voice/log">Log</a>
                </div>
            </td>
            <td>
                <div class="dot"></div>
                <span>Stembesturing luistert naar <a href="/voice/commands">trefwoorden</a>.</span>
            </td>
        </tr>
        <tr class="table-not-listening-status hidden">
            <td>
                <div class="btn-group" role="group">
                    <button class="btn btn-outline-success btn-sm voice-recognition-start">Start</button>
                    <a class="btn btn-sm btn-outline-secondary" href="/voice/log">Log</a>
                </div>
            </td>
            <td>
                <div class="dot danger"></div>
                <span>Stembesturing staat niet aan.</span>
            </td>
        </tr>


        {if !$webserver_on_voice && isset($is_filepath_vlc_running)}
            <tr>
                <td>
                    {if $is_filepath_vlc_running}
                        <button class="btn btn-outline-danger btn-sm stop-vlc-filepath reload">Stop</button>
                    {/if}
                </td>
                <td>
                    {if $is_filepath_vlc_running}
                        <div class="dot"></div>
                        <span>VLC speelt een videobestand af op de server.</span>
                    {else}
                        <div class="dot danger"></div>
                        <span>VLC speelt momenteel geen videobestand af.</span>
                    {/if}
                </td>
            </tr>
        {/if}
        {if !$webserver_on_voice && isset($is_filepath_stream_running)}
            <tr>
                <td>
                    {if $is_filepath_stream_running}
                        <button class="btn btn-outline-danger btn-sm stop-filepath-stream reload">Stop</button>
                    {/if}
                </td>
                <td>
                    {if $is_filepath_stream_running}
                        <div class="dot"></div>
                        <span>Er draait een HLS live stream voor een videobestand.</span>
                    {else}
                        <div class="dot danger"></div>
                        <span>Er draait geen webstream voor een videobestand.</span>
                    {/if}
                </td>
            </tr>
        {/if}
    </tbody>
</table>
