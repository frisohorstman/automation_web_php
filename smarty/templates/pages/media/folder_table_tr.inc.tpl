{if $m['is_file']}
    {assign var='url' value="/media/file?filepath="|cat:$m['filepath_url']}
{else}
    {assign var='url' value="/media/folder?filepath="|cat:$m['filepath_url']}
{/if}

{if $m['filename'] == 'screens'}
    {assign var='url' value="/media/thumbnails?folder="|cat:$m['filepath_url']}
{/if}

<tr>
    <td onclick="document.location = '{$url}'">
        <a href="{$url}">
            {if $m['is_file']}
                <svg class="bi" width="18px" height="15px"><use xlink:href="#file-play"/></svg>
            {else}
                <svg class="bi" width="18px" height="15px"><use xlink:href="#folder"/></svg>
            {/if}
            {$m['filename']}
        </a>
    </td>
    <td>
        {$m['filesize']}
    </td>
    <td onclick="document.location = '{$url}'">
        {$m['created']|date_format:"%e %B %Y %H:%M"}
    </td>
    <td>
        {if $m['video_filepath_url']}
            <a href="javascript://" class="play-in-vlc" data-filepath="{$m['video_filepath_url']}">
                <svg class="bi" width="22px" height="22px"><use xlink:href="#play2"/></svg>
            </a>
        {/if}
    </td>
</tr>