{include file="./media_controls_vlc.inc.tpl"}

<div class="container py-5" id="media-folder-page">
    <h1 class="cover-heading" id="media-folder-title">
        Media folder
    </h1>
    <div>
        {$media['folder']}
        
        <small>
            ({$disk_space['used_percentage']}% used, {$disk_space['available']} free)
        </small>
    </div>

    <p class="lead">
        <div style="width: 100%;">
            <a href="/media/folder?filepath={$media['parent_folder']}" class="btn btn-primary" style="float: left; width: 88px;" id="back-page">
                <svg class="bi" width="17px" height="17px"><use xlink:href="#backspace"/></svg>
                Terug
            </a>
            <form method="GET" style="width: calc(100% - 98px); margin-left: 10px; float: left;">
                <input type="hidden" name="folder" value="{$media['folder']}" />
                <div class="form-group">
                    <input type="text" name="search" class="form-control" placeholder="Zoek in folder..." value="{$filters['search']}" />
                </div>
            </form>
        </div>
        
        {if $media['files']|count == 0}
            <div class="mt-4 float-start">
                Er staan geen bestanden in deze folder.
            </div>
        {else}
            {include file="./folder_table.inc.tpl"}
        {/if}
    </p>
</div>