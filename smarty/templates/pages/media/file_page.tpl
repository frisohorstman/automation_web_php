{include file="./media_controls_vlc.inc.tpl"}

<div class="container py-5" id="media-file-page" style="margin-bottom: 100px;">
    <h1 class="cover-heading">Media file</h1>

    <div class="lead">
        <a href="/media/folder?filepath={$media['parent_folder']}" class="btn btn-primary" id="back-page">
            <svg class="bi" width="14px" height="14px"><use xlink:href="#backspace"/></svg>
            Terug
        </a>

        <table class="table media-file-detail-table">
            <tbody>
                <tr>
                    <td>
                        Bestandsnaam
                    </td>
                    <td>
                        {$media['filename']}<br>
                        {if $media['next_filepath']}
                            (<a href="/media/file?filepath={$media['next_filepath']}">volgende</a>)
                        {/if}
                    </td>
                </tr>
                <tr>
                    <td>
                        Type
                    </td>
                    <td>
                        {$media['content_type']}
                    </td>
                </tr>
                <tr>
                    <td>
                        Grootte
                    </td>
                    <td>
                        {$media['filesize']}
                    </td>
                </tr>
                {if $media['video_id3']}
                    <tr>
                        <td>
                            Lengte
                        </td>
                        <td>
                            {$media['video_id3']['playtime']}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Resolutie
                        </td>
                        <td>
                            {$media['video_id3']['video']['resolution_x']}x{$media['video_id3']['video']['resolution_y']}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Video formaat
                        </td>
                        <td>
                            {if isset($media['video_id3']['video']['dataformat'])}
                                {$media['video_id3']['video']['dataformat']}
                            {/if}

                            {if isset($media['video_id3']['video']['fourcc_lookup'])}
                                {$media['video_id3']['video']['fourcc_lookup']}
                            {/if}
                        </td>
                    </tr>
                {/if}
                <tr>
                    <td>
                        Aangemaakt
                    </td>
                    <td>
                        {$media['created']|date_format:"%e %B %Y %H:%M"}
                    </td>
                </tr>
            </tbody>
        </table>

        {if $media['is_video']}
            {if $media['thumbnails'] == false}
                <button type="button" class="btn btn-primary btn-lg mb-2 generate-thumbnails" data-filepath="{$media['filepath']}">
                    Genereer thumbnails
                </button>
            {else}
                <a type="button" class="btn btn-primary btn-lg mb-2 view-thumbnails" href="/media/thumbnails?folder={$media['parent_folder']}/screens">
                    Bekijk thumbnails
                </a>
            {/if}

            <button type="button" class="btn btn-primary btn-lg mb-2 play-in-vlc" data-filepath="{$media['filepath']}">
                Open in VLC
                <svg class="bi" style="width: 18px; height: 18px; margin-left: 5px;"><use xlink:href="#play2"/></svg>
            </button>

            {if $media['content_type'] == 'video/x-matroska'}
                <button type="button" class="btn btn-primary btn-lg mb-2 start-stream-in-browser" data-filepath="{$media['filepath']}">
                    Start stream in browser
                    <svg class="bi" style="width: 18px; height: 18px; margin-left: 5px;"><use xlink:href="#play2"/></svg>
                </button>

                <link href="//vjs.zencdn.net/5.8/video-js.min.css" rel="stylesheet" />
                <script src="/js/lib/videojs/videojs.min.js"></script>
                <script src="/js/lib/videojs/videojs-contrib-hls.min.js"></script>

                <div id="stream_wrapper"></div>
            {/if}

            {if $media['content_type'] != 'video/x-matroska'}
                <button type="button" class="btn btn-primary btn-lg mb-2 open-in-browser" data-filepath="{$media['filepath']}">
                    Open in browser
                    <svg class="bi" style="width: 18px; height: 18px; margin-left: 5px;"><use xlink:href="#play2"/></svg>
                </button>

                <video style="width: 100%; margin-bottom: 100px; display: none;" id="video_file" controls>
                    <source src="{$media['filepath']|urldecode}" type="{$media['content_type']}">
                    Your browser does not support the video tag.
                </video>
            {/if}
        {/if}


        {* show image / video *}
        {if $media['content_type']|substr:0:5 == 'image'}
            <img src="{$media['filepath']|urldecode}" style="width: 100%;" />
        {/if}
    </div>
</div>