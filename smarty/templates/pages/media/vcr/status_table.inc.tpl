<table class="table status-table activity-table">
    <tbody>
        <tr>
            <td>
                {if $vcr_status['is_vlc_running'] == true}
                    <button class="btn btn-outline-danger btn-sm stop-vlc-vcr-on-server reload">Stop</button>
                {else}
                    <button class="btn btn-outline-success btn-sm start-vlc-vcr-on-server reload">Start</button>
                {/if}
            </td>
            <td>
                {if $vcr_status['is_vlc_running'] == true}
                    <div class="dot"></div>
                    <span>VLC draait en gebruikt /dev/video0.</span>
                {else}
                    <div class="dot danger"></div>
                    <span>VLC voor videorecorder draait niet.</span>
                {/if}
            </td>
        </tr>
        <tr>
            <td>
                {if $vcr_status['is_streaming'] == true}
                    <button class="btn btn-outline-danger btn-sm stop-vcr-stream reload">Stop</button>
                {else}
                    <button class="btn btn-outline-success btn-sm start-vcr-ffmpeg-stream-on-server redirect">Start</button>
                {/if}
            </td>
            <td>
                {if $vcr_status['is_streaming'] == true}
                    <div class="dot"></div>
                    <span>FFMPEG videorecorder stream draait, <a href="/media/vcr/stream">bekijken</a>.</span>
                {else}
                    <div class="dot danger"></div>
                    <span>De videorecorder word niet gestreamd.</span>
                {/if}
            </td>
        </tr>
        <tr>
            <td>
                {if $vcr_status['is_recording'] == true}
                    <button class="btn btn-outline-danger btn-sm stop-vcr-record reload">Stop</button>
                {else}
                    <a href="/media/vcr/record" class="btn btn-outline-success btn-sm">Start</a>
                {/if}
            </td>
            <td>
                {if $vcr_status['is_recording'] == true}
                    <div class="dot"></div>
                    <span>Er word een video opgenomen naar de harde schijf. <a href="/media/vcr/record">Bekijk status</a>.</span>
                {else}
                    <div class="dot danger"></div>
                    <span>Er word momenteel niet opgenomen vanaf de VCR.
                {/if}
            </td>
        </tr>
        <tr>
            <td>
                <a href="/media/folder?filepath=/mnt/sda1/Capture" class="btn btn-outline-secondary btn-sm">
                    Opnames
                </a>
            </td>
            <td>
                <svg class="bi" style="width: 20px; height: 20px; float: left; margin-top: 6px; margin-right: 15px; color: #0d6efd;"><use xlink:href="#play2"/></svg>
                {if $vcr_status['last_recorded'] == false}
                    <span>Er zijn nog geen opnames.</span>
                {else}
                    <span>Ga naar de <a href="/media/folder?filepath={$vcr_status['last_recorded']['filepath_url']}">laatste opname</a>.</span>
                {/if}
            </td>
        </tr>
    </tbody>
</table>