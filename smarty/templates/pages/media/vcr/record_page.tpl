{include file="./navigation.inc.tpl"}

<div class="container py-5" id="vcr-record-page">
    <h1 class="cover-heading">Videorecorder opnemen</h1>

    <p class="lead">
        {if $vcr_status['is_streaming']}
            Het video apparaat /dev/video0, de videorecorder, word momenteel gebruikt door een stream.<br>
            Je moet eerst de stream <a href="javascript://" class="stop-vcr-stream">beëindigen</a> voordat je een opname kunt maken.
        {else if $vcr_status['is_vlc_running']}
            Het video apparaat /dev/video0, de videorecorder, word momenteel gebruikt door VLC.<br>
            Je moet eerst VLC <a href="javascript://" class="stop-vlc-vcr-on-server reload">stoppen</a> voordat je een opname kunt maken.
        {else}
            <form>
                <div class="form-check" style="margin-bottom: 10px;">
                    <input class="form-check-input" type="checkbox" name="playstop" id="playstop_checkbox" checked>
                    <label class="form-check-label" for="playstop_checkbox">
                        Play/stop cassette
                    </label>
                </div>

                <div class="form-check" style="margin-bottom: 10px;">
                    <input class="form-check-input" type="checkbox" name="convert" id="convert_checkbox" checked>
                    <label class="form-check-label" for="convert_checkbox">
                        Converteren na opname
                    </label>
                </div>

                <div class="input-group mb-3" style="max-width: 600px;">
                    <span class="input-group-text">Opname lengte</span>
                    <input type="text" class="form-control" id="record_hh" placeholder="HH">
                    <span class="input-group-text">:</span>
                    <input type="text" class="form-control" id="record_mm" placeholder="MM">
                    <span class="input-group-text">:</span>
                    <input type="text" class="form-control" id="record_ss" placeholder="SS">
                </div>

                <div class="buttons">
                    <button type="button" class="btn btn-primary start-vcr-record">
                        Start opname
                        <svg class="bi" style="width: 20px; height: 20px; margin-left: 5px; margin-top: 5px; margin-bottom: -3px;"><use xlink:href="#record"/></svg>
                    </button>

                    <button type="button" class="btn btn-danger stop-vcr-record">
                        <span>Stop opname</span>
                        <svg class="bi" style="width: 20px; height: 20px; margin-left: 5px; margin-top: 5px; margin-bottom: -3px;"><use xlink:href="#stop"/></svg>
                    </button>
                </div>

                <div class="convert_progress" style="margin-top: 25px; {if !$record_status}display: none{/if}">
                    <h2 class="cover-heading voortgang-title">
                        Laatst opgenomen bestand
                    </h2>

                    <div class="progress">
                        <div class="progress-bar {if $record_status && $record_status['progress']['percentage'] == 100}bg-success{/if}" style="{if $record_status}width: {$record_status['progress']['percentage']}%{/if}" role="progressbar" aria-valuenow="{if $record_status}{$record_status['progress']['percentage']}{/if}" aria-valuemin="0" aria-valuemax="100"></div>
                        <div class="progress-text">
                            {if $record_status}
                                {$record_status['progress']['percentage']}%
                            {/if}
                        </div>
                    </div>

                    <table class="table">
                        <tbody>
                                <tr>
                                    <td style="width: 200px;">
                                        Grootte
                                    </td>
                                    <td id="record_size">
                                        {if $record_status}
                                            {$record_status['progress']['details']['size']}
                                        {/if}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 200px;">
                                        Frames
                                    </td>
                                    <td id="record_frames">
                                        {if $record_status}
                                            {$record_status['progress']['details']['frames']}
                                        {/if}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 200px;" id="record_time_name">
                                        Opgenomen tijd
                                    </td>
                                    <td id="record_time">
                                        {if $record_status}
                                            {$record_status['progress']['details']['time']}
                                        {/if}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 200px;">
                                        Opname eindlengte
                                    </td>
                                    <td id="record_total_length">
                                        {if $record_status && $record_status['length'] == false}
                                            {$record_status['progress']['details']['time']}
                                        {/if}
                                        {if $record_status && $record_status['length'] != false}
                                            {$record_status['length']}
                                        {/if}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 200px;">
                                        Bestandsnaam
                                    </td>
                                    <td id="record_filename">
                                        <a href="/media/file?filepath={$record_status['filepath']}">
                                            {if $record_status}
                                                {$record_status['progress']['details']['filename']}
                                            {/if}
                                        </a>
                                    </td>
                                </tr>
                        </tbody>
                    </table>
                </div>
            </form>
        {/if}
    </p>
</div>