{include file="./navigation.inc.tpl"}

<div class="container py-5" id="vcr-status-page">
    <h1 class="cover-heading">Videorecorder status</h1>

    <p class="lead">
        {include file="./status_table.inc.tpl"}
    </p>
</div>