{include file="./navigation.inc.tpl"}

<script src="/js/lib/dash/dash.all.debug.js"></script>
{include file="./media_controls_vcr.inc.tpl"}

<div class="container py-5" id="vcr-stream-page">
    <h1 class="cover-heading">Videorecorder live stream</h1>

    <p class="lead">
        {if $vcr_status['is_recording']}
            Het video apparaat /dev/video0, de videorecorder, word momenteel gebruikt voor een opname.<br>
            Je moet eerst de opname <a href="javascript://" class="stop-vcr-record reload">beëindigen</a> voordat je een stream kunt starten.
        {else if $vcr_status['is_vlc_running']}
            Het video apparaat /dev/video0, de videorecorder, word momenteel gebruikt door VLC.<br>
            Je moet eerst VLC <a href="javascript://" class="stop-vlc-vcr-on-server reload">stoppen</a> voordat je een stream kunt starten.
        {else}
            <button type="button" class="btn btn-warning start-vcr-ffmpeg-stream-on-server">
                Start ffmpeg stream
                <svg class="bi" style="width: 18px; height: 18px; margin-left: 5px;"><use xlink:href="#play2"/></svg>
            </button>

            <button type="button" class="btn btn-danger stop-vcr-stream" style="display: none;">
                Stop stream
                <svg class="bi" style="width: 20px; height: 20px; margin-left: 5px; margin-top: 5px; margin-bottom: -3px;"><use xlink:href="#stop"/></svg>
            </button>

            <button type="button" class="btn btn-primary load-vcr-stream-in-browser" style="display: none;">
                (Re)load stream in browser
                <svg class="bi" style="width: 18px; height: 18px; margin-left: 5px;"><use xlink:href="#play2"/></svg>
            </button>

            <video id="vcr_stream" style="width: 100%; margin-top: 15px;" controls autoplay></video>
        {/if}
    </p>
</div>