<div class="media-controls vcr">
    <div class="bg"></div>

    <div class="buttons">
        <a href="javascript://" class="skip-backward" data-remote="vr6000" data-key="KEY_REWIND">
            <svg class="bi"><use xlink:href="#skip-backward"/></svg>
        </a>

        <a href="javascript://" class="play" data-remote="vr6000" data-key="KEY_PLAY">
            <svg class="bi"><use xlink:href="#play"/></svg>
        </a>

        <a href="javascript://" class="pause" data-remote="vr6000" data-key="Still">
            <svg class="bi"><use xlink:href="#pause2"/></svg>
        </a>

        <a href="javascript://" class="stop" data-remote="vr6000" data-key="KEY_STOP">
            <svg class="bi"><use xlink:href="#stop"/></svg>
        </a>

        <a href="javascript://" class="skip-forward" data-remote="vr6000" data-key="KEY_FORWARD">
            <svg class="bi"><use xlink:href="#skip-forward"/></svg>
        </a>
    </div>
</div>