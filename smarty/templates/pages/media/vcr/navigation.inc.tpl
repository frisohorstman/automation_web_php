<div class="container">
  <ul class="nav nav-pills nav-fill">
    <li class="nav-item">
      <a class="nav-link {if $uri == "/media/vcr"}active{/if}" href="/media/vcr">Status</a>
    </li>
    <li class="nav-item">
      <a class="nav-link {if $uri == "/media/vcr/stream"}active{/if}" href="/media/vcr/stream">Live stream</a>
    </li>
    <li class="nav-item">
      <a class="nav-link {if $uri == "/media/vcr/record"}active{/if}" href="/media/vcr/record">Opnemen</a>
    </li>
  </ul>
</div>