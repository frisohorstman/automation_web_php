<div class="media-controls vlc">
    <div class="bg"></div>

    <div class="buttons">
        <a href="javascript://" class="keystroke skip-backward" data-keystroke="Left">
            <svg class="bi"><use xlink:href="#skip-backward"/></svg>
        </a>

        <a href="javascript://" class="keystroke pause" data-keystroke="space">
            <svg class="bi"><use xlink:href="#pause2"/></svg>
        </a>

        <a href="javascript://" class="keystroke play hidden" data-keystroke="space">
            <svg class="bi"><use xlink:href="#play"/></svg>
        </a>

        <a href="javascript://" class="stop stop-vlc-filepath">
            <svg class="bi"><use xlink:href="#stop"/></svg>
        </a>

        <a href="javascript://" class="keystroke skip-forward" data-keystroke="Right">
            <svg class="bi"><use xlink:href="#skip-forward"/></svg>
        </a>

        <div class="time_wrapper">
            <div class="time"></div>
            <input type="range" class="form-range bar" min="0" max="10000">
            <div class="length"></div>
        </div>

        <a href="javascript://" class="goto-file">
            <svg class="bi"><use xlink:href="#box-arrow-in-right"/></svg>
        </a>

        <a href="javascript://" class="keystroke fullscreen" data-keystroke="f">
            <svg class="bi"><use xlink:href="#fullscreen"/></svg>
        </a>
    </div>
</div>