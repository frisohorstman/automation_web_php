
<table class="table table-hover media-table">
    <thead>
        <tr>
            <th scope="col">
                <a href="/media/folder?filepath={$media['folder']}&order=filename&by={$filters['rev_by']}">
                    Filename

                    {if $filters['order'] == 'filename'}
                        {if $filters['by'] == 'ASC'}
                            <svg class="bi" width="18px" height="18px"><use xlink:href="#chevron-bar-up"/></svg>
                        {else}
                            <svg class="bi" width="18px" height="18px"><use xlink:href="#chevron-bar-down"/></svg>
                        {/if}
                    {/if}
                </a>
            </th>
            <th scope="col">
                Grootte
            </th>
            <th scope="col">
                <a href="/media/folder?filepath={$media['folder']}&order=created&by={$filters['rev_by']}">
                    Aangemaakt

                    {if $filters['order'] == 'created'}
                        {if $filters['by'] == 'ASC'}
                            <svg class="bi" width="18px" height="18px"><use xlink:href="#chevron-bar-up"/></svg>
                        {else}
                            <svg class="bi" width="18px" height="18px"><use xlink:href="#chevron-bar-down"/></svg>
                        {/if}
                    {/if}
                </a>
            </th>
            <th scope="col"></th>
        </tr>
    </thead>
    <tbody>
        {foreach $media['files'] as $m}
            {if !$m['is_file']}
                {include file="./folder_table_tr.inc.tpl"}
            {/if}
        {/foreach}

        {foreach $media['files'] as $m}
            {if $m['is_file']}
                {include file="./folder_table_tr.inc.tpl"}
            {/if}
        {/foreach}
    </tbody>
</table>