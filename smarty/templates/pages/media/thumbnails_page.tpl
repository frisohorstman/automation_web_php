<link href="/js/lib/image_viewer/viewer.min.css" rel="stylesheet">
<script src="/js/lib/image_viewer/viewer.min.js"></script>



<div class="container py-5" id="media-file-page" style="margin-bottom: 100px;">
    <h1 class="cover-heading">Screenshots</h1>

    <div class="lead">
        <a href="/media/folder?filepath={$thumbnails['parent_folder']}" class="btn btn-primary" id="back-page">
            <svg class="bi" width="14px" height="14px"><use xlink:href="#backspace"/></svg>
            Terug
        </a>

        <div class="thumbnail_overview" id="gallery">
          {foreach $thumbnails['files'] as $thumb}
            <div class="thumbnail">
              <img src="{$thumb['filepath']}" />
            </div>
          {/foreach}
        </div>
    </div>
</div>

<script>
  const elm = document.getElementById('gallery');
  console.log('elm', elm)
  const gallery = new Viewer(elm);
</script>