

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark" aria-label="Ninth navbar example">
    <div class="container-xl">
      <a class="navbar-brand" href="/">
        <svg height="40px" width="180px"><use xlink:href="#awesome_media_logo_negative"/></svg>
      </a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample07XL" aria-controls="navbarsExample07XL" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExample07XL">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          {foreach $navigation as $nav}
            {if !isset($nav['subnav'])}
                <li><a class="nav-link {$nav['class']}" href="{$nav['uri']}">{$nav['name']}</a></li>
            {else}
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdown07XL" data-bs-toggle="dropdown" aria-expanded="false">
                  {$nav['name']}
                </a>
                <ul class="dropdown-menu" aria-labelledby="dropdown07XL">
                  {foreach $nav['subnav'] as $subnav}
                    <li><a class="dropdown-item {$subnav['class']}" href="{$subnav['uri']}">{$subnav['name']}</a></li>
                  {/foreach}
                </ul>
              </li>
            {/if}
          {/foreach}
        </ul>

        <div class="listen-status yellow transparant">Verbinden...</div>
      </div>
    </div>
  </nav>

{if isset($sub_navigation)}
  <div class="container">
    <header class="d-flex flex-wrap align-items-center justify-content-center justify-content-md-between py-3 mb-4 border-bottom">
      <ul class="nav col-12 col-md-auto mb-2 justify-content-center mb-md-0">
        {foreach $sub_navigation as $nav}
            <li><a class="nav-link px-2 {if $nav['class'] == 'active'}link-secondary{else}link-dark{/if}" href="{$nav['uri']}">{$nav['name']}</a></li>
        {/foreach}
      </ul>
    </header>
  </div>
{/if}