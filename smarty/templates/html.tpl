<!doctype html>
<html lang="nl">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <meta name="description" content="Network Home Automation Pages">
    <meta name="author" content="Friso Horstman">
    <meta name="generator" content="Hugo 0.82.0">
    <title>Home Automation</title>

    <!-- CSS -->
    {$css}

    <!-- JS -->
    {$js}
  </head>

  <body>
    {include file="svg.inc.tpl"}

    {include file="navigation.inc.tpl"}

    {include file=$page_template}
  </body>
</html>
