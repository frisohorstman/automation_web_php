{if isset($page_template)}
    {include file=$page_template}
{/if}

{if isset($html_content)}
    {$html_content}
{/if}
