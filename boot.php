<?php

// uri is passed on by nginx
function uri($return = 'uri') {
    $uri = '/';
    if(isset($_SERVER['REQUEST_URI'])) {
        $url = $_SERVER['REQUEST_URI'];
        $url = parse_url($url);

        $uri = $url['path'];
        
        if($return == 'parts') {
            return explode('/', $uri);
        }

        if($return == 'query') {
            if(!isset($url['query'])) {
                return false;
            } else {
                return $url['query'];
            }
        }
    }


    return $uri;
}

class Boot {
    function run() {
        $this->includePhp();

        // !alert, goto https is now done via JS.
        // only when the mic is accessed goto https
        // $net = new Net();
        // $net->upgradeToHttps();

        // goto url
        $followed = $this->followRoute();

        if($followed === false) {
            echo "The url (".uri().") you're visiting doesn't exist.";
            http_response_code(404);
            exit();
        }
    }

    function smarty() {
        require_once('smarty/smarty3/Smarty.class.php');

        $smarty = new Smarty();
        $smarty->setTemplateDir('./smarty/templates');
        $smarty->setCompileDir('./smarty/templates_c');
        $smarty->setCacheDir('./smarty/cache');
        $smarty->setConfigDir('./smarty/configs');

        $this->assignSmarty($smarty);
        $this->assignNavigation($smarty);
        $this->assignCSS($smarty);

        return $smarty;
    }

    function smartyDisplay($smarty, $page_tpl, $html_tpl = 'html.tpl') {
        $js = new Js();
        $js->assignSmarty($smarty);

        if($page_tpl != false) {
            $smarty->assign('page_template', $page_tpl);
        }

        $smarty->display($html_tpl);
    }

    /**
     * gives back free space on hard drives
     */
    function diskSpaceFree($find_dev = false, $only_dev = true) {
        $cmd = 'df -Th';
        $output = shell_exec($cmd);

        $lines = explode("\n", $output);
        $disk_space = [];
        foreach($lines as $i => $line) {
            $parts = preg_split('/\s+/', $line);

            if($i == 0 || !isset($parts[1])) {
                continue;
            }

            if($only_dev && substr($parts[0], 0, 4) != '/dev') {
                continue;
            }

            $label = false;
            $mountpoint = $parts[6];
            $cmd = 'lsblk -o LABEL,MOUNTPOINT | grep '.$mountpoint;
            $output = shell_exec($cmd);

            if(!empty($output)) {
                $label = str_replace($mountpoint, '', $output);
                $label = trim($label);
            }
            
            $df = array(
                'filesystem' => $parts[0],
                'mountpoint' => $mountpoint,
                'type' => $parts[1],
                'size' => $parts[2],
                'used' => $parts[3],
                'available' => $parts[4],
                'used_percentage' => str_replace('%', '', $parts[5]),
                'label' => $label,
            );

            if($find_dev !== false && $find_dev == substr($parts[0], 5)) {
                return $df;    
            }

            $disk_space[] = $df;
        }

        unset($disk_space[count($disk_space)]);

        if($find_dev !== false) {
            return false;
        }

        return $disk_space;
    }

    /**
     * includes php files from php directory
     */
    function includePhp() {
        $files = glob('php/*.php');

        foreach($files as $filepath) {
            include_once($filepath);
            $class = basename($filepath, '.php');
        }
    }


    function assignSmarty($smarty) {
        $smarty->assign('uri', uri());
        $smarty->assign('uri_parts', uri('parts'));
        $smarty->assign('uri_query', uri('query'));

        $smarty->assign('_GET', $_GET);
    }


    function assignCSS($smarty) {
        $assign = $this->getCSSFileUriTags();
        if(CSS_EMBED == true) {
            $assign = '<style>'.$this->getCSSMerged().'</style>';
        }

        if(CSS_EMBED == false && CSS_MERGE == true) {
            $assign = '<link href="/css" rel="stylesheet">';
        }

        $smarty->assign('css', $assign);
    }

    /**
     * return array;
     * 
     * give back css filenames from css/ folder
     */
    function getCSSFilenames() {
        return glob('css/*.css');
    }

    /**
     * return string;
     */
    function getCSSFileUriTags() {
        $css = '';
        foreach($this->getCSSFilenames() as $filename) {
            $uri = '/'.$filename;

            $css .= '<link href="'.$uri.'" rel="stylesheet">'."\n";
        }

        return $css;
    }

    /**
     * return string;
     * 
     * concats all files together as one long file
     */
    function getCSSMerged() {
        $css = '';
        foreach($this->getCSSFilenames() as $filename) {
            $uri = '/'.$filename;
            $filepath = getcwd().$uri;

            $css .= file_get_contents($filepath)."\n\n";
        }

        if(CSS_MINIFY) { // slow
            $css = str_replace("\n", "", $css);
            $css = str_replace("\r", "", $css);
            // $css = str_replace(" ", "", $css);
        }

        return $css;
    }


    /**
     * derivative from config.php's route's see it there are any urls
     * that also function as a navigation item.
     */
    function assignNavigation($smarty) {
        $smarty->assign('navigation', $this->buildNavigation(ROUTES));

        $subnav = $this->findSubNavigation(ROUTES);
        if(isset($subnav['subnav'])) {
            $smarty->assign('sub_navigation', $this->buildNavigation($subnav['subnav']));
        }
    }

    function findSubNavigation($routes) {
        foreach($routes as $uri => $route) {
            if($uri == uri()) {
                return $route;
            }

            if(isset($route['subnav'])) {
                $sub_route = $this->findSubNavigation($route['subnav']);

                if($sub_route) {
                    return $route;
                }
            }
        }

        return false;
    }

    function buildNavigation($routes) {
        $current_uri = uri();
        $current_query = uri('query');

#        $device = Net::findWebServer($_SERVER['HTTP_HOST']);

        if(!empty($current_query)) {
            $current_uri .= '?'.$current_query;
        }
        
        $navigation = array();
        foreach($routes as $uri => $route) {
            if(!isset($route['navigation'])) {
                continue;
            }


            if(isset($route['show_on']) && !Net::isWebserverOn($route['show_on'])) {
                continue;
            }

            if(isset($route['show_on_voice']) && $route['show_on_voice'] == false && Net::isWebserverOn('voice_pi')) {
                // dont show if webserver on voice pi and show_on_voice is true
                continue;
            }
            
            if(isset($route['show_when_loggedin']) && $route['show_when_loggedin'] == true && !Auth::isLoggedIn()) {
                // dont show if show_when_loggedin is true and youre not logged in
                continue;
            }

            // if its a user with an access array those urls may become navigation 
            $uris = Auth::getLoggedInUserAllUri();

            if(is_array($uris)) {
                if(!in_array($uri, $uris)) {
                    continue;
                }
            }

            $class = '';
            if($uri == $current_uri) {
                $class = 'active';
            }

            $nav = array(
                'name' => $route['navigation'],
                'uri' => $uri,
                'class' => $class
            );

            if(isset($route['subnav'])) {
                $nav['subnav'] = $this->buildNavigation($route['subnav']);
            }

            $navigation[] = $nav;
        }

        return $navigation;
    }

    // unused, but also not reimplented
    function oldmergefunction() {
        // if(JS_EMBED == false && uri() == '/javascript') {
            //     header('Content-Type: application/javascript');
            //     return $this->getJavascriptMerged();
            // }
            
            // if(CSS_EMBED == false && uri() == '/css') {
                //     header('Content-type: text/css');
                //     return $this->getCSSMerged();
                // }
    }

    function getRoute() {
        return $this->checkRoute(ROUTES);
    }

    /**
     * check if there is a route matching uri() if there is, execute this function.
     */
    function followRoute() {
        // @todo js:addmsg verplaatsen
        if(isset($_GET['error'])) {
            Js::addMsg($_GET['error'], 'danger');
        }

        $route = $this->getRoute();

        if($route == false) {
            return false;
        }

        // auth, defaults to:
        $auth = array(
            'class' => 'Auth',
            'method' => 'needsLogin',
        );

        // if auth has been set in config.php routes
        if(isset($route['auth'])) {
            $auth = $route['auth'];
        }

        // now check the auth class and function on return val false or true
        // !note, route_auth will only be false if explicitely set in config.php routes
        if($auth != false) {    
            $auth_class = $auth['class'];
            $auth_method = $auth['method'];

            $c = new $auth_class;
            $needs_login = $c->$auth_method();

            // echo "auth_class: ".$auth_class;
            // echo "auth_method: ".$auth_method;
            // var_dump($needs_login);
            // die();

            if($needs_login) {
                if(uri() != '/login') { // so you dont wind up in an endless redirect loop on /login page
                    header('Location: /login');
                }
            }
        }

        // authentication has been done or isnt required if we reach this point
        // so now call the route function that actually displays the page
        $route_class = $route['route']['class'];
        $route_method = $route['route']['method'];

        $c = new $route_class;
        $output = $c->$route_method();

        if($output) {
            // you may or may not return html/text in route method::function(), as you desire..
            echo $output;
        }

        return true;
    }

    // check if route is on current uri(), this thing runs twice once for main nav and once for subnav
    function checkRoute($routes) {
        foreach($routes as $uri => $route) {
            if(isset($route['subnav'])) {
                $sub_followed = $this->checkRoute($route['subnav']);

                if($sub_followed !== false) {
                    return $sub_followed;
                }
            }

            if(uri() != $uri) {
                continue;
            }

            if(!isset($route['route']['class']) || !isset($route['route']['method'])) {
                return false;
            }

            return $route;
        }
        
        return false;
    }
}
