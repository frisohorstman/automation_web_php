<?php

/**
 * !note
 * Auth defaults to Auth::needsLogin()
 */
define('ROUTES', array(
    '/merged.js' => array(
        'route' => array(
            'class' => 'Js',
            'method' => 'getMerged'
        )
    ),
    
    '/' => array(
        'navigation' => 'Home',
        'route' => array(
            'class' => 'OtherPages',
            'method' => 'homePage'
        )
    ),
    '/voice' => array(
        'navigation' => 'Spraakherkenning',
        'subnav' => array(
            '/voice/commands' => array(
                'navigation' => 'Trefwoorden',
                'route' => array(
                    'class' => 'Voice',
                    'method' => 'pageCommands'
                )
            ),
            '/voice/notes' => array(
                'navigation' => 'Notities',
                'route' => array(
                    'class' => 'Voice',
                    'method' => 'pageNotes'
                )
            ),
            '/voice/log' => array(
                'navigation' => 'Log',
                'route' => array(
                    'class' => 'Voice',
                    'method' => 'pageLog'
                )
            ),
            '/voice/json' => array(
                'route' => array(
                    'class' => 'Voice',
                    'method' => 'pageJSON'
                )
            ),
        ),
    ),
    '/infrared/remotes' => array(
        'navigation' => 'Afstandsbedieningen',
        'subnav' => array(
            '/infrared/remote/tv' => array(
                'navigation' => 'Televisie',
                'show_on' => 'voice_pi',
                'route' => array(
                    'class' => 'Infrared',
                    'method' => 'pageRemoteTV'
                )
            ),
            '/infrared/remote/surround' => array(
                'navigation' => 'Surround Set',
                'show_on' => 'voice_pi',
                'route' => array(
                    'class' => 'Infrared',
                    'method' => 'pageRemoteSurroundSet'
                )
            ),
            '/infrared/remote/muis' => array(
                'navigation' => 'Muis',
//                'show_on_voice' => false,
                'route' => array(
                    'class' => 'Infrared',
                    'method' => 'pageMouse'
                )
            ),
            '/infrared/remote/overview' => array(
                'navigation' => 'Overzicht',
                'show_on' => 'voice_pi',
                'route' => array(
                    'class' => 'Infrared',
                    'method' => 'pageRemotesOverview'
                )
            ),
        ),
    ),
    '/infrared/json/recieve' => array(
        'route' => array(
            'class' => 'Infrared',
            'method' => 'recieve'
        )
    ),
    '/infrared/json/send' => array(
        'route' => array(
            'class' => 'Infrared',
            'method' => 'send'
        )
    ),
    '/torrents' => array(
        'navigation' => 'Torrents',
        'show_on' => 'server',
        'subnav' => array(
            '/torrent/json' => array(
                'route' => array(
                    'class' => 'Torrents',
                    'method' => 'jsonPage'
                )
            ),
            '/torrent/downloads' => array(
                'navigation' => 'Downloads',
                'route' => array(
                    'class' => 'Torrents',
                    'method' => 'downloadsPage'
                )
            ),
            '/torrent/downloads/table' => array(
                'route' => array(
                    'class' => 'Torrents',
                    'method' => 'downloadsHtmlTable'
                )
            ),
            '/torrent/search' => array(
                'navigation' => 'Zoeken',
                'route' => array(
                    'class' => 'Torrents',
                    'method' => 'searchPage'
                )
            ),
            '/torrent/search/table' => array(
                'route' => array(
                    'class' => 'Torrents',
                    'method' => 'searchHtmlTable'
                )
            ),
            '/torrent/details' => array(
                'route' => array(
                    'class' => 'Torrents',
                    'method' => 'detailPage'
                )
            ),
            '/torrent/details/table' => array(
                'route' => array(
                    'class' => 'Torrents',
                    'method' => 'detailHtmlTable'
                )
            ),
        )
    ),
    '/media' => array(
        'navigation' => 'Media',
        'show_on' => 'server',
        'subnav' => array(
            '/media/file' => array(
                'route' => array(
                    'class' => 'Media',
                    'method' => 'filePage'
                )
            ),
            '/media/folder' => array(
                'navigation' => 'Downloads',
                'route' => array(
                    'class' => 'Media',
                    'method' => 'folderPage'
                )
            ),
            '/media/thumbnails' => array(
                'route' => array(
                    'class' => 'Media',
                    'method' => 'thumbnailsPage'
                )
            ),
            '/media/folder?filepath=/mnt/sda1/Movies' => array(
                'navigation' => 'Movies',
                'route' => array(
                    'class' => 'Media',
                    'method' => 'folderPage',
                )
            ),
            '/media/folder?filepath=/mnt/sda1/Series' => array(
                'navigation' => 'Series',
                'route' => array(
                    'class' => 'Media',
                    'method' => 'folderPage',
                )
            ),
            '/media/folder?filepath=/mnt/sda1/Capture' => array(
                'navigation' => 'Opnames',
                'route' => array(
                    'class' => 'Media',
                    'method' => 'folderPage',
                )
            ),
            '/media/vcr' => array(
                'navigation' => 'Videorecorder',
                'route' => array(
                    'class' => 'Vcr',
                    'method' => 'statusPage'
                )
            ),
            '/media/vcr/stream' => array(
                'route' => array(
                    'class' => 'Vcr',
                    'method' => 'streamPage'
                )
            ),
            '/media/vcr/record' => array(
                'route' => array(
                    'class' => 'Vcr',
                    'method' => 'recordPage'
                )
            ),
            '/media/json' => array(
                'route' => array(
                    'class' => 'Media',
                    'method' => 'jsonPage'
                )
            ),
        )
    ),
    '/vlc/json' => array(
        'route' => array(
            'class' => 'Vlc',
            'method' => 'jsonPage'
        )
    ),
    '/status' => array(
        'navigation' => 'Overzicht',
        'route' => array(
            'class' => 'OtherPages',
            'method' => 'statusPage'
        )
    ),
    '/playstation/json' => array(
        'auth' => false,
        'route' => array(
            'class' => 'Playstation',
            'method' => 'jsonPage'
        )
    ),
    '/json' => array(
        'auth' => false,
        'route' => array(
            'class' => 'Json',
            'method' => 'jsonPage'
        )
    ),
    '/net/json' => array(
        // auth is determined in route method
        'auth' => false,
        // this url is called from unprotected home.frisohorstman.nl since you  
        // cant call https from http, this way that simple page always loads..
        'httpsUpgrade' => false,
        'route' => array(
            'class' => 'Net',
            'method' => 'jsonPage'
        )
    ),
    '/login' => array(
        'httpsUpgrade' => false,
        'route' => array(
            'class' => 'Auth',
            'method' => 'loginPage'
        )
    ),
    '/logout' => array(
        'auth' => false,
        'navigation' => 'Logout',
        'show_when_loggedin' => true,
        'route' => array(
            'class' => 'Auth',
            'method' => 'logoutPage'
        )
    ),
    '/auth/json' => array(
        'auth' => false,
        'route' => array(
            'class' => 'Auth',
            'method' => 'jsonPage'
        )
    ),    
    '/zway/json' => array(
        'route' => array(
            'class' => 'Zway',
            'method' => 'jsonPage'
        )
    ),


    // api
    '/api/1.0/cpu/info' => array(
        'route' => array(
            'class' => 'Api',
            'method' => 'cpuInfo'
        )
    )
));
